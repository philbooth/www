## Gadget review: Polar Flow M430 vs Garmin Forerunner 645
<!-- gadgets reviews running -->
For Christmas I bought myself a Garmin Forerunner 645,
after becoming irritated to the point of distraction
by my Polar Flow M430.
Now that I've used the Garmin
for a few weeks,
this is my compare-and-contrast review
of the two devices.

![The Polar Flow M430 alongside the Garmin Forerunner 645](https://assets.philbooth.me/images/polar-flow-m430-garmin-forerunner-645-672.jpg)

### Polar Flow M430

These are the reasons
I got annoyed with the M430:

* It takes a long time to connect to GPS
  and occasionally drops out
  part-way through a run,
  causing obviously wrong routes and times
  to be recorded.
  Most of my running
  is along the seafront,
  so it's not like there are
  tall buildings getting in the way
  or whatever.

* The heart-rate monitor is inconsistent.
  Sometimes it appears to under-count by 50%,
  e.g. showing a rate in the 90s
  when I'm going flat out
  and know it should be in the 180s.
  After a minute or two
  it will suddenly jump up
  to the correct level.

* Except for showing the time,
  the information on the different watch faces
  isn't really useful or detailed.

* Syncing data is kind of clunky.
  For a long time,
  I didn't understand that
  you have to explicitly open
  the Polar Flow app on your phone first,
  before you hold the button to sync
  on the watch.
  Consequently,
  I kept re-pairing the two devices
  thinking there was a problem
  with the bluetooth connection
  when there wasn't.

* The strap is way too long
  for my wrist
  and the watch face itself
  is quite bulky.
  That makes it tricky
  to fit underneath tight-fitting
  thermal layers in the winter months.

* Other minor nits,
  like the lap summary screen
  that prevents you interacting
  with any of the other functions,
  or the charging indicator
  that nags you
  if you don't let it reach
  all the way to 100%,
  combine to give the overall sense
  of a product
  where not much thought
  has gone in to
  the user experience.

Lest I paint an entirely negative picture,
I should make clear that it's not all bad.
On the occasions when
the GPS and the heart-rate monitor
function correctly,
the watch is fine
and does its job.
But it's not a joy to use
and I wanted a change,
so enter the Garmin.

### Garmin Forerunner 645

I actually bought the Forerunner 645 Music,
which lets me stream music
directly to bluetooth-connected headphones
without needing to take my phone out running.
As that's not a feature
on the Polar Flow,
I'm not going to consider it for comparison here.
But it works well
and has enough storage
to comfortably fit
25 90-ish-minute techno mixes,
which is my motivational accompaniment of choice
when running.

The Garmin is both lighter and slimmer
than the Polar Flow,
but the first thing
you really notice about it
is that the screen is much better.
It's in colour,
is easier to read
and has a selction of widgets
to scroll through
that show in-depth, varied information
about your day or week.
These include the usual suspects
like step counter, calorie counter and heart monitor,
but also local weather, a compass, training status
and the ability to install
3rd-party widgets from the Connect IQ Store.
Immediately the Forerunner becomes
a more useful thing to wear on your wrist
all of the time,
whereas with the M430
I tended not to wear it
for general use
and just put it on
when going out for a run.

The mobile app is also much better
on the Garmin,
with bucketloads of features
and, best of all,
syncing is an activity
that takes place automatically
in the background
rather than being an explicit step
to be invoked by button-press.
There's also a separate app
for MacOS/Windows,
if you prefer to control your watch
from your computer.

A couple of times I've worn
both watches together on a run,
to see how they compare.
The Garmin definitely feels lighter
on my wrist
and always connects to GPS faster.
The two watches mostly agree
on all the major statistics,
except for the aforementioned flakiness
when measuring heart-rate on the Polar Flow.

### Conclusion

I definitely prefer the Garmin Forerunner 645
to the Polar Flow M430
and recommend it
if you happen to be weighing up
a decision between the two watches.
I haven't noticed any rough edges
in the Garmin user interface
like I did with the Polar Flow,
and it is better than the Polar Flow
in all the areas where I found
that device annoying.
