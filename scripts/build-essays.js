'use strict'

const fs = require('fs')
const path = require('path')

const md = require('markdown-it')({
  html: true,
  xhtmlOut: true
})

const essayPath = path.resolve('essays')
const timestampTemplate ='<p class="article-time"><em><time datetime="{{{time}}}">{{{formattedDate}}}</time></em></p>\n'
const blueskyParagraph = [
  '<aside>',
  '<p class="smallprint jobspam">',
  'You can find me',
  '<a href="https://bsky.app/profile/philbooth.me">on Bluesky <img alt="" class="icon" src="https://assets.philbooth.me/images/bluesky.png" /></a>',
  '</p>',
  '</aside>',
].join('\n')
const jobParagraph = [
  '<aside>',
  '<p class="smallprint jobspam">',
  'Are you hiring?',
  'Because I\'m looking for my next role!',
  'See <a href="https://files.philbooth.me/philbooth.cv.pdf" rel="nofollow">my CV</a>.',
  '</p>',
  '</aside>',
].join('\n')

const MONTHS = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
]

// https://regex101.com/r/huZmNz/1
const PERSONAL_PATTERN = /<!--.*personal.*-->/i

fs.readdirSync(essayPath)
  .forEach(essay => {
    if (essay && essay[0] !== '.') {
      console.log(essay)

      const time = parseInt(essay.substr(0, essay.indexOf('-')))

      const markup = md.render(fs.readFileSync(path.join(essayPath, essay), {
        encoding: 'utf8'
      }))

      const firstParagraph = markup.indexOf('<p>')
      const timestampParagraph = timestampTemplate
        .replace('{{{time}}}', new Date(time).toISOString())
        .replace('{{{formattedDate}}}', formatDate(time))

      const footer = PERSONAL_PATTERN.test(markup) ? '' : jobParagraph

      fs.writeFileSync(
        path.resolve(`views/essays/${essay.split('.')[0]}.html`),
        markup.slice(0, firstParagraph)
          .concat(timestampParagraph, markup.slice(firstParagraph), footer),
      )
    }
  })

function formatDate(timestamp) {
  const date = new Date(timestamp)

  if (isNaN(date.getTime())) {
    throw new Error(`Invalid timestamp "${timestamp}" in essay data`)
  }

  const day = formatDay(date.getDate())
  const month = MONTHS[date.getMonth()]
  const year = date.getFullYear()

  return `${day} ${month} ${year}`
}

function formatDay(date) {
  switch (date) {
    case 1:
    case 21:
    case 31:
      return `${date}<sup>st</sup>`

    case 2:
    case 22:
      return `${date}<sup>nd</sup>`

    case 3:
    case 23:
      return `${date}<sup>rd</sup>`

    default:
      return `${date}<sup>th</sup>`
  }
}
