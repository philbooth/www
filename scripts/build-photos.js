'use strict'

const fs = require('fs')
const path = require('path')
const photoPath = path.resolve('photos')

fs.readdirSync(photoPath)
  .forEach(photo => {
    if (photo && photo[0] !== '.' && photo.endsWith('.json')) {
      console.log(photo)

      const timestamp = parseInt(photo.substr(0, photo.indexOf('-')))
      const time = new Date(timestamp).toISOString()

      const {
        date,
        location,
        region,
        title,
        url,
        url_672,
      } = require(`../photos/${photo}`);

      fs.writeFileSync(
        path.resolve(`views/photos/${photo.split('.')[0]}.html`),
        `<h2>${title}</h2>
<p>
  <a href="${url}">
    <img alt="${title}" src="${url_672}" />
  </a>
</p>
<dl>
  <dt>Date:</dt><dd><time datetime="${time}">${date}</time></dd>
  <dt>Where:</dt><dd>${location}, ${region}</dd>
</dl>`,
      )
    }
  })
