'use strict'

const SECOND = 1000
const MINUTE = SECOND * 60
const HOUR = MINUTE * 60
const DAY = HOUR * 24
const WEEK = DAY * 7
const YEAR = DAY * 365

module.exports = { SECOND, MINUTE, HOUR, WEEK, YEAR }

