'use strict'

const bunyan = require('bunyan')
const methods = [ 'trace', 'debug', 'info', 'warn', 'error', 'fatal' ]

module.exports = options => {
  const log = {}
  const impl = bunyan.createLogger(options)

  methods.forEach(name => {
    log[name] = () => {
      const args = arguments
      setImmediate(() => {
        impl[name](...args)
      })
    }
  })

  return log
}

