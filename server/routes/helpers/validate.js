'use strict'

module.exports = {
  post: validatePost,
  delete: validateDelete
}

function validatePost (body, method, mandatory, patterns) {
  validatePassword(body)

  if (method === 'put') {
    mandatory.forEach(key => {
      if (! body[key]) {
        throw {
          code: 400,
          message: `Bad request. Missing property "${key}".`
        }
      }
    })
  }

  Object.keys(body).forEach(key => {
    if (key === 'password') {
      return
    }

    const pattern = patterns[key]

    if (! pattern) {
      throw {
        code: 400,
        message: `Bad request. Property "${key}" is not permitted.`
      }
    }

    const value = body[key]

    if (value && ! pattern.test(value)) {
      throw {
        code: 400,
        message: `Bad request. Value "${value}" for property "${key}" is invalid.`
      }
    }
  })
}

function validatePassword (body) {
  const password = process.env.MAGIC_WORD

  if (! password) {
    throw {
      code: 500,
      message: 'No password configured.'
    }
  }

  if (body.password !== password) {
    throw {
      code: 401,
      message: 'Unauthorized.'
    }
  }
}

function validateDelete (body, type, patterns) {
  validatePassword(body)

  if (! body[type]) {
    throw {
      code: 400,
      message: `Bad request. Missing property "${type}".`
    }
  }

  const value = body[type]
  if (! patterns[type].test(value)) {
    throw {
      code: 400,
      message: `Bad request. Value "${value}" for property "${type}" is invalid.`
    }
  }
}

