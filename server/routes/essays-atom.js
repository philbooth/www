'use strict'

const path = require('path')

const log = require('bunyan').createLogger({ name: 'server/routes/essays-atom' })
const handlebars = require('express-handlebars').create({
  partialsDir: 'views/partials'
})

const data = require('../data/essays')

const TEMPLATE_PATH = path.join(__dirname, '../../views/atom.xml')
const COMMON_PROPERTIES = {
  blogAuthor: 'Phil Booth',
  blogEmail: 'pmbooth@gmail.com',
  baseUri: 'https://philbooth.me',
}

// https://regex101.com/r/fE0kof/1
const SUBSCRIBER_COUNT_PATTERN = /(?<count>[0-9]+?)[\s]+subscriber/i

const { essays, tagMap } = data.getEssays()
const responseMap = new Map()
const awaitFeeds = Promise.all(
  [
    handlebars.render(TEMPLATE_PATH, {
      ...COMMON_PROPERTIES,
      blogTitle: 'Existing by coincidence, programming deliberately',
      blogPath: essays[0].link,
      atomPath: essays[0].atomLink,
      lastTimestamp: new Date(essays[0].essay.time).toISOString(),
      essays: essays.map(item => {
        const { essay } = item
        return {
          title: essay.title,
          path: essay.url,
          timestamp: new Date(essay.time).toISOString(),
          content: essay.content
        }
      })
    }, {
      cache: true
    })
      .then(feed => responseMap.set('all', feed))
  ]
    .concat(
      Array.from(tagMap.entries())
        .map(([ tag, tagEssays ]) => {
          const { link, atomLink } = tagEssays[0]
          return handlebars.render(TEMPLATE_PATH, {
            ...COMMON_PROPERTIES,
            blogTitle: `Phil Booth's blog: ${tag} posts`,
            blogPath: link,
            atomPath: atomLink,
            lastTimestamp: new Date(tagEssays[0].essay.time).toISOString(),
            essays: tagEssays.map(item => {
              const { essay } = item
              return {
                title: essay.title,
                path: essay.url,
                timestamp: new Date(essay.time).toISOString(),
                content: essay.content
              }
            })
          }, {
            cache: true
          })
            .then(feed => responseMap.set(tag, feed))
        })
    )
)

module.exports = {
  path: essays[0].atomLink,
  handler: (viewData, request, response) => {
    const filter = request.query.topic
    const userAgent = request.headers['user-agent']

    const match = SUBSCRIBER_COUNT_PATTERN.exec(userAgent)
    if (match) {
      const { count } = match.groups

      const slash = userAgent.indexOf('/')
      const space = userAgent.indexOf(' ')
      let service = userAgent.slice(0, slash >= 0 && slash < space ? slash : space).trim()
      if (service === 'Mozilla') {
        service = userAgent.split(';')[1].trim()
      }

      log.info({ count, filter, service }, 'blog feed subscription')
    }

    const feed = responseMap.get(filter) || responseMap.get('all')

    response.locals.etag = null

    awaitFeeds.then(() => {
      response.set('Content-Type', 'application/atom+xml')
      response.send(feed)
    })
  }
}

