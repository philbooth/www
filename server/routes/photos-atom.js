'use strict'

const path = require('path')

const log = require('bunyan').createLogger({ name: 'server/routes/essays-atom' })
const handlebars = require('express-handlebars').create({
  partialsDir: 'views/partials'
})

const data = require('../data/photos')

const TEMPLATE_PATH = path.join(__dirname, '../../views/atom.xml')
const COMMON_PROPERTIES = {
  blogAuthor: 'Phil Booth',
  blogEmail: 'pmbooth@gmail.com',
  baseUri: 'https://philbooth.me',
}

// https://regex101.com/r/fE0kof/1
const SUBSCRIBER_COUNT_PATTERN = /(?<count>[0-9]+?)[\s]+subscriber/i

const { photos, tagMap } = data.getPhotos()
const responseMap = new Map()
const awaitFeeds = Promise.all(
  [
    handlebars.render(TEMPLATE_PATH, {
      ...COMMON_PROPERTIES,
      blogTitle: 'Ham-fisted snapper',
      blogPath: photos[0].link,
      atomPath: photos[0].atomLink,
      lastTimestamp: new Date(photos[0].photo.time).toISOString(),
      essays: photos.map(item => {
        const { photo } = item
        return {
          title: photo.title,
          path: photo.url,
          timestamp: new Date(photo.time).toISOString(),
          content: photo.content
        }
      })
    }, {
      cache: true
    })
      .then(feed => responseMap.set('all', feed))
  ]
    .concat(
      Array.from(tagMap.entries())
        .map(([ tag, tagPhotos ]) => {
          const { link, atomLink } = tagPhotos[0]
          return handlebars.render(TEMPLATE_PATH, {
            ...COMMON_PROPERTIES,
            blogTitle: `Phil Booth's photostream: ${tag}`,
            blogPath: link,
            atomPath: atomLink,
            lastTimestamp: new Date(tagPhotos[0].photo.time).toISOString(),
            essays: tagPhotos.map(item => {
              const { photo } = item
              return {
                title: photo.title,
                path: photo.url,
                timestamp: new Date(photo.time).toISOString(),
                content: photo.content
              }
            })
          }, {
            cache: true
          })
            .then(feed => responseMap.set(tag, feed))
        })
    )
)

module.exports = {
  path: photos[0].atomLink,
  handler: (viewData, request, response) => {
    const filter = decodeURIComponent(request.query.tag || '')
    const userAgent = request.headers['user-agent']

    const match = SUBSCRIBER_COUNT_PATTERN.exec(userAgent)
    if (match) {
      const { count } = match.groups

      const slash = userAgent.indexOf('/')
      const space = userAgent.indexOf(' ')
      let service = userAgent.slice(0, slash >= 0 && slash < space ? slash : space).trim()
      if (service === 'Mozilla') {
        service = userAgent.split(';')[1].trim()
      }

      log.info({ count, filter, service }, 'blog feed subscription')
    }

    const feed = responseMap.get(filter) || responseMap.get('all')

    response.locals.etag = null

    awaitFeeds.then(() => {
      response.set('Content-Type', 'application/atom+xml')
      response.send(feed)
    })
  }
}

