'use strict'

const log = require('bunyan').createLogger({ name: 'server/routes/essay' })

const data = require('../data/essays')
const hash = require('../hash')

const essays = data.getEssays()
  .essays
  .reduce((map, item) => {
    const { essay } = item
    map.set(essay.url, {
      ...essay,
      viewHash: hash(JSON.stringify(essay)),
      tags: essay.tags.map(tag => ({
        ...tag,
        isLink: true
      }))
    })
    return map
  }, new Map())
const routes = require('.').view.map(route => ({
  path: route.path,
  navTitle: route.navTitle,
  isLink: true
}))

module.exports = {
  handler: (viewData, request, response, next) => {
    const { headers, path } = request

    const essay = essays.get(path)
    if (! essay) {
      const logData = { path, userAgent: headers['user-agent'] }

      if (path === '/blog/blog.atom') {
        log.warn(logData, 'redirecting bad atom link')
        return response.redirect(301, '/blog.atom')
      }

      log.error(logData, 'essay not found')
      return response.sendStatus(404)
    }

    response.locals = Object.assign({
      viewName: `essays/${essay.time}-${essay.url.substr(essay.url.lastIndexOf('/') + 1)}`,
      viewTitle: essay.title,
      description: essay.abstract,
      etag: essay.viewHash,
      tagline: 'Existing by coincidence, programming deliberately',
      isEssay: true,
      routes,
      tags: essay.tags.length > 0 ? essay.tags : null
    }, viewData)

    next()
  }
}

