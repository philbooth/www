'use strict'

const log = require('bunyan').createLogger({ name: 'server/routes/photos' })

const data = require('../data/photos')
const hash = require('../hash')
const vagueTime = require('vague-time')

const { photos, tags, tagMap } = data.getPhotos()
tags.unshift({ link: '/photos', atomLink: '/photos.atom', term: 'all' })

const PAGE_SIZE = 8

module.exports = {
  path: '/photos',
  navTitle: 'Photos',
  handler: (viewData, request, response, next) => {
    let filtered

    const filter = decodeURIComponent(request.query.tag || '')
    if (filter) {
      if (!tagMap.has(filter)) {
        const { headers, path } = request
        log.error({ path, tag: filter, userAgent: headers['user-agent'] }, 'tag not found')

        return response.sendStatus(404)
      }

      filtered = tagMap.get(filter)
    } else {
      filtered = photos
    }

    const page = parseInt(request.query.page || '1')
    const index = (page - 1) * PAGE_SIZE

    if (index >= filtered.length) {
      return response.sendStatus(404)
    }

    const hasBeforeLink = page > 1
    const hasAfterLink = index + PAGE_SIZE < filtered.length
    const basePaginationLink = `/photos${filter ? `?tag=${filter}&page=` : '?page='}`

    response.locals = Object.assign({
      viewName: 'photos',
      viewTitle: `Photos${filter ? ` tagged: ${filter}` : ''}`,
      appendSiteTitle: ! filter,
      description: 'Phil Booth\'s photostream',
      tagline: 'Ham-fisted snapper',
      atomPath: filtered[0].atomLink,
      photos: filtered.map(tag => {
        const { photo } = tag
        return {
          content: photo.content.slice(0, photo.content.indexOf('</p>') + 5),
          title: photo.title,
          url: photo.url,
          time: photo.timeString,
          vagueTime: vagueTime.get({ to: photo.time, units: 'ms' }),
          tags: filtered === photos && photo.tags.length > 0 ? photo.tags.map(marshallTag) : null
        }
      }).slice(index, index + PAGE_SIZE),
      tags: tags.map(marshallTag),
      hasBeforeLink,
      beforeLink: `${basePaginationLink}${page - 1}`,
      hasAfterLink,
      afterLink: `${basePaginationLink}${page + 1}`,
    }, viewData)
    response.locals.etag = hash(JSON.stringify(response.locals))
    next()

    function marshallTag (tag) {
      let isLink

      if (tag.term === 'all') {
        isLink = !! filter
      } else {
        isLink = tag.term !== filter
      }

      return { ...tag, isLink }
    }
  }
}
