'use strict'

const data = require('../data/cv')
const hash = require('../hash')

const MONTHS = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
]

data.jobs.forEach((job) => {
  job.timeFromFormatted = formatDate(job.timeFrom)

  if (job.timeUntil) {
    job.timeUntilFormatted = formatDate(job.timeUntil)
  }
})

const VIEW_HASH = hash(JSON.stringify(data))

module.exports = {
  path: '/cv',
  handler: (viewData, request, response, next) => {
    response.locals = {
      ...data,
      ...viewData,
      etag: VIEW_HASH,
      viewName: 'cv',
    }
    next()
  }
}

function formatDate (dateString) {
  const date = new Date(dateString)

  if (isNaN(date.getTime())) {
    throw new Error(`Invalid date "${dateString}" in CV data`)
  }

  const month = MONTHS[date.getMonth()]
  const year = date.getFullYear()

  return `${month} ${year}`
}
