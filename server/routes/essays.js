'use strict'

const log = require('bunyan').createLogger({ name: 'server/routes/essays' })

const data = require('../data/essays')
const hash = require('../hash')
const vagueTime = require('vague-time')

const { essays, tags, tagMap } = data.getEssays()
tags.unshift({ link: '/blog', atomLink: '/blog.atom', term: 'all' })

const PAGE_SIZE = 8

module.exports = {
  path: '/blog',
  navTitle: 'Blog',
  handler: (viewData, request, response, next) => {
    let filtered

    const filter = decodeURIComponent(request.query.topic || '')
    if (filter) {
      if (!tagMap.has(filter)) {
        const { headers, path } = request
        log.error({ path, tag: filter, userAgent: headers['user-agent'] }, 'tag not found')

        return response.sendStatus(404)
      }

      filtered = tagMap.get(filter)
    } else {
      filtered = essays
    }

    const page = parseInt(request.query.page || '1')
    const index = (page - 1) * PAGE_SIZE

    if (index >= filtered.length) {
      return response.sendStatus(404)
    }

    const hasBeforeLink = page > 1
    const hasAfterLink = index + PAGE_SIZE < filtered.length
    const basePaginationLink = `/blog${filter ? `?topic=${filter}&page=` : '?page='}`

    response.locals = Object.assign({
      viewName: 'essays',
      viewTitle: `Blog${filter ? ` topic: ${filter}` : ''}`,
      appendSiteTitle: ! filter,
      description: 'Phil Booth\'s blog',
      tagline: 'Existing by coincidence, programming deliberately',
      atomPath: filtered[0].atomLink,
      essays: filtered.map(tag => {
        const { essay } = tag
        return {
          title: essay.title,
          abstract: essay.abstract,
          url: essay.url,
          time: essay.timeString,
          vagueTime: vagueTime.get({ to: essay.time, units: 'ms' }),
          tags: filtered === essays && essay.tags.length > 0 ? essay.tags.map(marshallTag) : null
        }
      }).slice(index, index + PAGE_SIZE),
      tags: tags.map(marshallTag),
      hasBeforeLink,
      beforeLink: `${basePaginationLink}${page - 1}`,
      hasAfterLink,
      afterLink: `${basePaginationLink}${page + 1}`,
    }, viewData)
    response.locals.etag = hash(JSON.stringify(response.locals))
    next()

    function marshallTag (tag) {
      let isLink

      if (tag.term === 'all') {
        isLink = !! filter
      } else {
        isLink = tag.term !== filter
      }

      return { ...tag, isLink }
    }
  }
}
