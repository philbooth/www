'use strict'

const log = require('bunyan').createLogger({ name: 'routes/code' })
const data = require('../data/code')
const constants = require('../constants')
const hash = require('../hash')

const REFRESH_INTERVAL = constants.HOUR * 24 + constants.MINUTE
const TAGLINES = [
  'Organically cultivated software',
  'Complexophobe. Reductionist.',
  '1x engineer',
]

let repoData, viewHash

module.exports = {
  path: '/code',
  navTitle: 'Code',
  handler: (viewData, request, response, next) => {
    response.locals = Object.assign({
      viewName: 'code',
      viewTitle: 'Code',
      appendSiteTitle: true,
      description: 'Details of open-source projects that Phil has contributed to.',
      tagline: TAGLINES[Math.floor(Math.random() * TAGLINES.length)],
      etag: viewHash
    }, viewData, repoData)
    next()
  }
}

// Ensure there's always a fresh cached copy
// of the repo data to serve requests from.
refreshRepoData().then(() => setInterval(refreshRepoData, REFRESH_INTERVAL))

function refreshRepoData () {
  log.info('refreshing repo data')

  return data.getRepos()
    .then(repos => {
      log.debug(repos, 'fetched fresh repo data')

      repoData = repos

      log.info('refreshed repo data')
    })
    .then(() => viewHash = hash(JSON.stringify(repoData)))
    .catch(error => log.error(error))
}

