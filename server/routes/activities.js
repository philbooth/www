'use strict'

const log = require('bunyan').createLogger({ name: 'routes/activities' })
const data = require('../data/activities')
const constants = require('../constants')
const hash = require('../hash')

const REFRESH_INTERVAL = constants.HOUR * 12 + constants.MINUTE
const TAGLINES = [
  'Belly\'s gonna get ya'
]

let activitiesData, viewHash

module.exports = [
  {
    path: '/runs',
    navTitle: 'Runs',
    handler: (viewData, request, response, next) => {
      response.locals = Object.assign({
        viewName: 'activities',
        viewTitle: 'Runs',
        appendSiteTitle: true,
        description: 'Details of runs that Phil has done.',
        tagline: TAGLINES[Math.floor(Math.random() * TAGLINES.length)],
        etag: viewHash
      }, viewData, activitiesData.runs)
      next()
    }
  },
  {
    path: '/rides',
    navTitle: 'Rides',
    handler: (viewData, request, response, next) => {
      response.locals = Object.assign({
        viewName: 'activities',
        viewTitle: 'Rides',
        appendSiteTitle: true,
        description: 'Details of bike rides that Phil has done.',
        tagline: TAGLINES[Math.floor(Math.random() * TAGLINES.length)],
        etag: viewHash
      }, viewData, activitiesData.rides)
      next()
    }
  },
]

// Ensure there's always a fresh cached copy
// of the running data to serve requests from.
refreshActivitiesData().then(() => setInterval(refreshActivitiesData, REFRESH_INTERVAL))

function refreshActivitiesData () {
  log.info('refreshing activities data')

  return data.getActivities()
    .then(activities => {
      log.debug(activities, 'fetched fresh activities data')

      activitiesData = activities

      log.info('refreshed activities data')
    })
    .then(() => viewHash = hash(JSON.stringify(activitiesData)))
    .catch(error => log.error(error))
}

