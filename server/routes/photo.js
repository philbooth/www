'use strict'

const log = require('bunyan').createLogger({ name: 'server/routes/photo' })

const data = require('../data/photos')
const hash = require('../hash')

const photos = data.getPhotos()
  .photos
  .reduce((map, item) => {
    const { photo } = item
    map.set(photo.url, {
      ...photo,
      viewHash: hash(JSON.stringify(photo)),
      tags: photo.tags.map(tag => ({
        ...tag,
        isLink: true
      }))
    })
    return map
  }, new Map())
const routes = require('.').view.map(route => ({
  path: route.path,
  navTitle: route.navTitle,
  isLink: true
}))

module.exports = {
  handler: (viewData, request, response, next) => {
    const { headers, path } = request

    const photo = photos.get(path)
    if (! photo) {
      const logData = { path, userAgent: headers['user-agent'] }

      if (path === '/photos/photos.atom') {
        log.warn(logData, 'redirecting bad atom link')
        return response.redirect(301, '/photos.atom')
      }

      log.error(logData, 'photo not found')
      return response.sendStatus(404)
    }

    response.locals = Object.assign({
      viewName: `photos/${photo.url.substr(photo.url.lastIndexOf('/') + 1)}`,
      viewTitle: photo.title,
      etag: photo.viewHash,
      tagline: 'Ham-fisted snapper',
      isEssay: true,
      routes,
      tags: photo.tags.length > 0 ? photo.tags : null
    }, viewData)

    next()
  }
}
