'use strict'

const log = require('bunyan').createLogger({ name: 'routes/root' })
const code = require('../data/code')
const activities = require('../data/activities')
const bluesky = require('../data/bluesky')
const essays = require('../data/essays')
const photos = require('../data/photos')
const constants = require('../constants')
const hash = require('../hash')
const check = require('check-types')
const vagueTime = require('vague-time')

// Refresh intervals for different views are slightly offset to spread load
const REFRESH_INTERVAL = constants.HOUR * 6 + constants.MINUTE

let eventData

module.exports = {
  path: '/',
  navTitle: 'About',
  handler: (viewData, request, response, next) => {
    const now = Date.now()
    response.locals = Object.assign({
      viewName: 'root',
      description: 'Personal website for Phil Booth, a software engineer based in the UK.',
      tagline: 'Progressive working class software engineer',
      events: eventData && eventData.map(e => Object.assign({}, e))
    }, viewData)
    if (response.locals.events) {
      response.locals.events.forEach(event => setFriendlyDate(event, now))
    }
    response.locals.etag = hash(JSON.stringify(response.locals))

    next()
  }
}

function setFriendlyDate (object, now) {
  if (check.date(object.when)) {
    object.when = vagueTime.get({ to: object.when, from: now, units: 'ms' })
  }
}

// Ensure there's always a fresh cached copy
// of the event data to serve requests from.
refreshEventData().then(() => setInterval(refreshEventData, REFRESH_INTERVAL))

function refreshEventData () {
  log.info('refreshing event data')

  return Promise.all([
    bluesky.getLastPost(),
    code.getLastCommit(),
    activities.getLastRide(),
    activities.getLastRun(),
    essays.getLastEssay(),
    photos.getLastPhoto()
  ])
  .then(events => {
    log.debug({ eventData: events }, 'fetched fresh event data')

    if (events.length > 0) {
      eventData = events.filter(e => !!e).sort((lhs, rhs) => rhs.when - lhs.when)
    }

    log.info('refreshed event data')
  })
  .catch(error => log.error(error))
}

