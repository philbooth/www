'use strict'

const routes = [
  require('./root'),
  require('./code'),
  ...require('./activities'),
  require('./photos'),
  require('./essays'),
  require('./essays-atom'),
  require('./photos-atom'),
  require('./cv'),
]

module.exports = {
  all: routes,
  view: routes.slice(0, 6)
}

