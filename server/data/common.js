/* eslint-disable no-underscore-dangle */

'use strict'

const Redis = require('ioredis')

const {
  REDIS_HOST,
  REDIS_PASSWORD,
  REDIS_PORT,
} = process.env

const CHART_DATA = {
  chartWidth: 360,
  chartHeight: 180,
  padding: 2,
  axis: {
    width: 2,
    x: {},
    y: {}
  }
}
CHART_DATA.axis.x.length = CHART_DATA.chartWidth - CHART_DATA.axis.width
CHART_DATA.axis.x.offset = CHART_DATA.padding + CHART_DATA.axis.width / 2
CHART_DATA.axis.y.offset = CHART_DATA.padding / 2
CHART_DATA.maxHeight = CHART_DATA.chartHeight - CHART_DATA.padding - CHART_DATA.axis.width

module.exports = (log, cachedData) => ({
  getCachedData (key) {
    const cache = cachedData[key]

    if (cache.item) {
      return Promise.resolve(cache.item)
    }

    return new Promise(resolve => {
      cache.resolve = resolve
    })
  },

  setCachedData (key, data) {
    const cache = cachedData[key]

    cache.item = data

    if (cache.resolve) {
      cache.resolve(cache.item)
      delete cache.resolve
    }
  },

  connect () {
    return new Redis({
      host: REDIS_HOST,
      password: REDIS_PASSWORD,
      port: REDIS_PORT,
    })
  },

  async finish (redis, after) {
    await redis.quit()

    if (after) {
      after()
    }
  },

  async fail (error, redis) {
    log.error(error)

    if (redis) {
      await redis.quit()
    }
  },

  generateDistributionChartData (items, key, ranges, max, title) {
    const data = new Array(ranges.length).fill(0)

    items.forEach(item => setData(item[key]))

    let rangeShift
    const zeroRangeIndices = getZeroRangeIndices(ranges)
    const barWidth = getBarWidth(ranges.length - zeroRangeIndices.length)
    const unitsPerPixel = data.reduce(greater, 0) / CHART_DATA.maxHeight

    return Object.assign({
      chartTitle: title,
      type: key,
      metricType: 'Count',
      bars: ranges.map(mapRange).filter(filterZeroRange),
      barWidth: barWidth - CHART_DATA.padding
    }, CHART_DATA)

    function setData (datum) {
      const lastIndex = ranges.length - 1
      ranges.some((range, index) => {
        if (index === lastIndex || datum <= ranges[index + 1]) {
          data[index] += 1
          return true
        }

        return false
      })
    }

    function getZeroRangeIndices () {
      const indices = []

      data.some(setIndex)

      rangeShift = indices.length

      data.slice(0)
        .reverse()
        .some((v, i) => setIndex(v, ranges.length - 1 - i))

      return indices

      function setIndex (value, index) {
        if (value === 0) {
          indices.push(index)
          return false
        }

        return true
      }
    }

    function mapRange (value, index) {
      const range = mapBar(data[index], index - rangeShift, unitsPerPixel, barWidth)
      range.lowerBound = ranges[index]
      range.upperBound = ranges[index + 1] || max
      return range
    }

    function filterZeroRange (value, index) {
      return zeroRangeIndices.indexOf(index) === -1
    }
  }
})

function getBarWidth (rangeCount) {
  return CHART_DATA.axis.x.length / rangeCount
}

function greater (a, b) {
  if (a > b) {
    return a
  }

  return b
}

function mapBar (value, index, unitsPerPixel, barWidth) {
  const barHeight = value / unitsPerPixel
  let textClass = 'chart-label'
  let labelOffset

  if (barHeight > 20) {
    labelOffset = 16
    textClass += ' chart-bar-label'
  } else {
    labelOffset = 0 - CHART_DATA.padding
  }

  return {
    offset: {
      x: index * barWidth,
      y: CHART_DATA.maxHeight - barHeight
    },
    barHeight,
    labelOffset,
    textClass,
    value
  }
}

