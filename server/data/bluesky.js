'use strict'

const log = require('bunyan').createLogger({ name: 'data/bluesky' })
const request = require('request-promise-native')

const BASE_URI = 'https://public.api.bsky.app/xrpc/app.bsky.feed'
const FEED_ENDPOINT = `${BASE_URI}.getAuthorFeed`
const BLUESKY_USER = 'philbooth.me'
const PROFILE_URL = `https://bsky.app/profile/${BLUESKY_USER}`

module.exports = {
  async getLastPost () {
    const posts = await fetchPosts()
    const lastPost = posts.find((post) => !! post.post.record.text)
    if (lastPost) {
      return mapPost(lastPost.post)
    }
  },
}

async function fetchPosts () {
  log.trace('fetchPosts')

  const response = await request.get(FEED_ENDPOINT, {
    headers: { 'User-Agent': 'https://philbooth.me/' },
    json: true,
    qs: { actor: BLUESKY_USER },
  })

  return response.feed
}

function mapPost (post) {
  const id = post.uri.slice(post.uri.lastIndexOf('/') + 1)

  const { text } = post.record
  const snippet = text.length > 60 ? `${text.slice(0, 58)}…` : text

  return {
    type: 'skeet',
    url: `${PROFILE_URL}/post/${id}`,
    what: snippet,
    when: new Date(post.record.createdAt),
    //whom: `@${BLUESKY_USER}`,
    //whomUrl: PROFILE_URL,
  }
}
