'use strict'

/* eslint-disable camelcase, no-underscore-dangle */

const fs = require('fs')
const log = require('bunyan').createLogger({ name: 'data/activities' })
const path = require('path')
const request = require('request-promise-native')
const viewHelpers = require('../view-helpers')

const common = require('./common')(log, {})

const {
  STRAVA_REFRESH_TOKEN: REFRESH_TOKEN,
  STRAVA_CLIENT_ID: CLIENT_ID,
  STRAVA_CLIENT_SECRET: CLIENT_SECRET,
} = process.env

const BASE_URI = 'https://www.strava.com'
const TOKEN_ENDPOINT = `${BASE_URI}/oauth/token`
const ACTIVITIES_ENDPOINT = `${BASE_URI}/api/v3/activities`
const PAGE_LENGTH = 100
const HOUR = 1000 * 60 * 60

const MONTHS = [
  'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
  'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
]
const STATS = new Map([
  [ 'This week', activity => {
    const from = new Date()
    from.setUTCDate(from.getUTCDate() - from.getUTCDay())
    zeroTime(from)
    return activity.date >= from
  } ],
  [ 'Last week', activity => {
    const from = new Date()
    from.setUTCDate(from.getUTCDate() - from.getUTCDay() - 7)
    zeroTime(from)
    const until = new Date()
    until.setUTCDate(until.getUTCDate() - until.getUTCDay())
    zeroTime(until)
    return activity.date >= from && activity.date < until
  } ],
  [ 'This month', activity => {
    const from = new Date()
    from.setUTCDate(1)
    zeroTime(from)
    return activity.date >= from
  } ],
  [ 'Last month', activity => {
    const from = new Date()
    from.setUTCMonth(from.getUTCMonth() - 1, 1)
    zeroTime(from)
    const until = new Date()
    until.setUTCDate(1)
    zeroTime(until)
    return activity.date >= from && activity.date < until
  } ],
  [ 'This year', activity => {
    const from = new Date()
    from.setUTCMonth(0, 1)
    zeroTime(from)
    return activity.date >= from
  } ],
  [ 'Last year', activity => {
    const from = new Date()
    from.setUTCFullYear(from.getUTCFullYear() - 1)
    from.setUTCMonth(0, 1)
    zeroTime(from)
    const until = new Date()
    until.setUTCMonth(0, 1)
    zeroTime(until)
    return activity.date >= from && activity.date < until
  } ]
])

const LOCAL_DATA = '../../fixtures/activities.json'

const DISTRIBUTION_RANGES = {
  rides: {
    distance: [ 0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50 ],
    vertical: [ 0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000 ],
  },
  runs: {
    distance: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 ],
    vertical: [ 0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000 ],
  },
}
const DISTRIBUTION_MAX = {
  rides: {
    distance: 50,
    vertical: 8000,
  },
  runs: {
    distance: 28,
    vertical: 1000,
  },
}
const DISTRIBUTION_TITLES = {
  distance: 'Distribution of distances (miles)',
  vertical: 'Distribution of vertical gain (feet)'
}
const DISTRIBUTION_TYPES = {
  distance: 'distance (miles)',
  vertical: 'vertical gain (feet)'
}

let resolveLastRide
let lastRide = new Promise(resolve => resolveLastRide = resolve)

let resolveLastRun
let lastRun = new Promise(resolve => resolveLastRun = resolve)

module.exports = {
  getActivities () {
    if (process.env.NODE_ENV === 'development') {
      const activities = require(LOCAL_DATA)
      if (activities.rides) {
        setLastRide(activities.rides.activities[0])
      }
      if (activities.runs) {
        setLastRun(activities.runs.activities[0])
      }
      return Promise.resolve(activities)
    }

    return fetchActivities()
      .then(marshallActivities)
      .then(activities => {
        if (process.env.NODE_ENV === 'development-write') {
          fs.writeFileSync(
            path.join(__dirname, LOCAL_DATA),
            JSON.stringify(activities, null, '  ')
          )
        }

        return activities
      })
  },

  getLastRide () {
    return lastRide
  },

  getLastRun () {
    return lastRun
  }
}

function zeroTime (date) {
  date.setUTCHours(0)
  date.setUTCMinutes(0)
  date.setUTCSeconds(0)
  date.setUTCMilliseconds(0)
}

async function fetchActivities (tokens = null, index = 1, activities = []) {
  log.trace('fetchActivities')

  if (! tokens) {
    tokens = await getTokens()
  }

  try {
    const response = await request.get(ACTIVITIES_ENDPOINT, {
      auth: {
        bearer: tokens.access,
      },
      headers: {
        'User-Agent': 'https://philbooth.me/runs',
      },
      json: true,
      qs: {
        page: index,
        per_page: PAGE_LENGTH,
      },
    })

    activities = activities.concat(response)

    if (response.length < PAGE_LENGTH) {
      return activities
    }

    return fetchActivities(tokens, index + 1, activities)
  } catch (error) {
    log.error(error)

    if (error.response && error.response.statusCode === 401) {
      const refreshedTokens = await refreshTokens(tokens)
      await writeTokens(refreshedTokens)
      return fetchActivities(refreshedTokens, index, activities)
    }

    throw error
  }
}

async function getTokens () {
  log.trace('getTokens')

  let tokens = await readTokens()

  if (! tokens || expiresSoon(tokens)) {
    tokens = await refreshTokens(tokens)
    await writeTokens(tokens)
  }

  return tokens
}

function expiresSoon (tokens) {
  return tokens.expires < Date.now() + HOUR
}

async function readTokens () {
  log.trace('readTokens')

  const redis = await common.connect()

  try {
    const tokens = JSON.parse(await redis.get('tokens:strava'))

    await common.finish(redis)

    return tokens
  } catch (error) {
    await common.fail(error, redis)
  }
}

async function refreshTokens (tokens) {
  log.trace('refreshTokens')

  if (tokens) {
    log.info(tokens, 'refreshing tokens')
  } else {
    log.warn('no tokens to refresh, attempting to use $STRAVA_REFRESH_TOKEN')
    tokens = {
      refresh: REFRESH_TOKEN,
    }
  }

  const response = await request.post(TOKEN_ENDPOINT, {
    headers: {
      'User-Agent': 'https://philbooth.me/runs',
    },
    json: true,
    qs: {
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      grant_type: 'refresh_token',
      refresh_token: tokens.refresh,
    },
  })

  log.info(response, '/oauth/tokens response')

  return {
    access: response.access_token,
    refresh: response.refresh_token,
    expires: response.expires_at * 1000,
  }
}

async function writeTokens (tokens) {
  log.trace('writeTokens')

  const redis = await common.connect()

  try {
    log.info('updating tokens')
    await redis.set('tokens:strava', JSON.stringify(tokens))

    await common.finish(redis)
  } catch (error) {
    await common.fail(error, redis)
  }
}

function marshallActivities (activities) {
  log.trace('marshallActivities')

  const rides = activities
    .filter(activity => activity.type === 'Ride' || activity.type === 'VirtualRide')
    .map(mapActivity.bind(null, setLastRide))

  const runs = activities
    .filter(activity => activity.type === 'Run' || activity.type === 'VirtualRun')
    .map(mapActivity.bind(null, setLastRun))

  return {
    rides: {
      activities: rides,
      charts: marshallCharts('rides', rides),
      stats: marshallStats(rides),
    },
    runs: {
      activities: runs,
      charts: marshallCharts('runs', runs),
      stats: marshallStats(runs),
    },
  }
}

function mapActivity (setLast, activity, index) {
  const date = new Date(activity.start_date)
  const result = {
    distance: {
      miles: mapMiles(activity.distance),
      kilometres: mapKilometres(activity.distance)
    },
    vertical: {
      feet: mapFeet(activity.total_elevation_gain),
      metres: activity.total_elevation_gain
    },
    duration: mapDuration(activity.moving_time),
    time: mapTime(date),
    date,
    heart: {
      max: activity.max_heartrate,
      mean: activity.average_heartrate
    },
    pace: {
      max: activity.max_speed,
      mean: activity.average_speed
    },
    cadence: activity.average_cadence,
    url: `${BASE_URI}/activities/${activity.id}`,
  }

  if (index === 0) {
    setLast(result)
  }

  return result
}

function mapMiles (metres) {
  return metres / 1609.34
}

function mapKilometres (metres) {
  return metres / 1000
}

function mapFeet (metres) {
  return Math.round(metres / 0.3048)
}

function mapDuration (seconds) {
  return {
    hours: Math.floor(seconds / 3600),
    minutes: Math.floor(seconds % 3600 / 60),
    seconds: seconds % 60
  }
}

function mapTime (date) {
  return `${mapDayOfMonth(date)}-${mapMonth(date)}-${date.getUTCFullYear()} ${
    mapHour(date)}:${mapMinute(date)} UTC`
}

function mapDayOfMonth (date) {
  return padNumber(date.getUTCDate())
}

function padNumber (number) {
  if (number < 10) {
    return `0${number}`
  }

  return number
}

function mapMonth (date) {
  return MONTHS[date.getUTCMonth()]
}

function mapHour (date) {
  return padNumber(date.getUTCHours())
}

function mapMinute (date) {
  return padNumber(date.getUTCMinutes())
}

function marshallStats (activities) {
  const stats = []

  STATS.forEach((predicate, title) => {
    const stat = {
      title,
      activities: 0,
      distance: {
        miles: 0,
        kilometres: 0
      },
      vertical: {
        feet: 0,
        metres: 0
      },
      duration: {
        hours: 0,
        minutes: 0,
        seconds: 0
      },
      heart: {
        max: 0
      }
    }

    activities.forEach(activity => {
      if (predicate(activity)) {
        stat.activities += 1
        stat.distance.miles += activity.distance.miles
        stat.distance.kilometres += activity.distance.kilometres
        stat.vertical.feet += activity.vertical.feet
        stat.vertical.metres += activity.vertical.metres
        stat.duration.hours += activity.duration.hours

        if (stat.duration.minutes + activity.duration.minutes >= 60) {
          stat.duration.hours += 1
          stat.duration.minutes = (stat.duration.minutes + activity.duration.minutes) % 60
        } else {
          stat.duration.minutes += activity.duration.minutes
        }

        if (stat.duration.seconds + activity.duration.seconds >= 60) {
          stat.duration.minutes += 1
          stat.duration.seconds = (stat.duration.seconds + activity.duration.seconds) % 60
        } else {
          stat.duration.seconds += activity.duration.seconds
        }

        if (activity.heart.max > stat.heart.max) {
          stat.heart.max = activity.heart.max
        }
      }
    })

    if (stat.activities === 0) {
      // Ensure that `{{#if activities}}` in the template evaluates truthily
      stat.activities = '0'
    }

    stats.push(stat)
  })

  return stats
}

function setLastRide (activity) {
  let hours
  if (activity.duration.hours > 0) {
    hours = `${viewHelpers.formatInteger(activity.duration.hours)}h `
  } else {
    hours = ''
  }

  const marshalled = {
    type: 'ride',
    url: activity.url,
    what: `${hours}${
      viewHelpers.formatInteger(activity.duration.minutes)}' ${activity.duration.seconds}" / ${
      viewHelpers.formatFloat(activity.distance.miles)} miles ➡️ / ${
      viewHelpers.formatInteger(activity.vertical.feet)} feet ⬆️`,
    when: activity.date
  }

  if (resolveLastRide) {
    resolveLastRide(marshalled)
    resolveLastRide = null
  } else {
    lastRide = Promise.resolve(marshalled)
  }
}

function setLastRun (activity) {
  let hours
  if (activity.duration.hours > 0) {
    hours = `${viewHelpers.formatInteger(activity.duration.hours)}h `
  } else {
    hours = ''
  }

  const marshalled = {
    type: 'run',
    url: activity.url,
    what: `${hours}${
      viewHelpers.formatInteger(activity.duration.minutes)}' ${activity.duration.seconds}" / ${
      viewHelpers.formatFloat(activity.distance.miles)} miles ➡️ / ${
      viewHelpers.formatInteger(activity.vertical.feet)} feet ⬆️`,
    when: activity.date
  }

  if (resolveLastRun) {
    resolveLastRun(marshalled)
    resolveLastRun = null
  } else {
    lastRun = Promise.resolve(marshalled)
  }
}

function marshallCharts (type, activities) {
  return generateDistributionChartData(type, activities).concat(generateBarChartData(activities))
}

function generateDistributionChartData (type, activities) {
  return [ 'distance', 'vertical' ]
    .map(key => common.generateDistributionChartData(
      activities.map(activity => ({
        [DISTRIBUTION_TYPES.distance]: activity.distance.miles,
        [DISTRIBUTION_TYPES.vertical]: activity.vertical.feet
      })),
      DISTRIBUTION_TYPES[key],
      DISTRIBUTION_RANGES[type][key],
      DISTRIBUTION_MAX[type][key],
      DISTRIBUTION_TITLES[key]
    ))
}

function generateBarChartData () {
  // TODO
  return []
}
