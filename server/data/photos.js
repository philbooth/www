'use strict'

const fs = require('fs')
const path = require('path')

const dataPath = path.join(__dirname, '../../photos')
const viewPath = path.join(__dirname, '../../views/photos')

const VISIBLE_TAGS = new Set([
  'Flowers',
  'Garden',
  'Views',
  'Wildlife',
  'Wildflowers',
  'Italy',
  'Dorset',
  'Hampshire',
  'Burley',
  'Lymington',
  'New Forest',
  'Oxford',
  'Pennington',
  'Wales',
  'Wilverley',
  'Milo',
  'Ralph',
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
  '2020',
  '2021',
  '2022',
  '2023',
  '2024',
  '2025',
  '2026',
  '2027',
  '2028',
  '2029',
  '2030',
])

const photos = fs.readdirSync(dataPath)
  .reduce((p, photo) => {
    if (!photo || photo[0] === '.' || !photo.endsWith('.json')) {
      return p
    }

    const { location, region, tags, title } = require(`../../photos/${photo}`)

    const basename = photo.split('.')[0]
    const time = parseInt(photo.split('-')[0])

    const content = fs.readFileSync(
      path.join(viewPath, `${basename}.html`),
      { encoding: 'utf8' },
    )

    return [
      ...p,
      {
        link: '/photos',
        atomLink: '/photos.atom',
        term: 'all',
        photo: {
          location,
          region,
          url: `/photos/${basename}`,
          time,
          timeString: new Date(time).toISOString(),
          title,
          content: content.substring(content.indexOf('</h2>') + 5),
          tags: tags.map(tag => ({
            link: `/photos?tag=${tag}`,
            atomLink: `/photos.atom?tag=${tag}`,
            term: tag
          }))
          .sort(sortTags)
        }
      }
    ]
  }, [])
  .sort((lhs, rhs) => rhs.photo.time - lhs.photo.time)

const { map: tagMap, set: tagSet } = photos.reduce((t, item) => {
  const { photo } = item
  photo.tags.forEach(tag => {
    const array = t.map.get(tag.term)
    const innerItem = { ...tag, photo }
    if (array) {
      array.push(innerItem)
    } else {
      t.map.set(tag.term, [ innerItem ])
      t.set.add(tag)
    }
  })
  return t
}, { map: new Map(), set: new Set() })
const allTags = Array.from(tagSet)
  .filter(({ term }) => VISIBLE_TAGS.has(term))
  .sort(sortTags)

const lastPhoto = {
  type: 'photo',
  what: photos[0].photo.title,
  when: new Date(photos[0].photo.time),
  where: `${photos[0].photo.location}, ${photos[0].photo.region}`,
  url: photos[0].photo.url
}

module.exports = {
  getPhotos () {
    return { photos, tags: allTags, tagMap }
  },

  getLastPhoto () {
    return Promise.resolve(lastPhoto)
  }
}

function sortTags (lhs, rhs) {
  return lhs.term.localeCompare(rhs.term, 'en', { sensitivity: 'base' })
}
