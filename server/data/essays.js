'use strict'

const fs = require('fs')
const path = require('path')

const essayPath = path.join(__dirname, '../../views/essays')
const essays = fs.readdirSync(essayPath)
  .map(essay => {
    const splitIndex = essay.indexOf('-')
    const time = parseInt(essay.substr(0, splitIndex))
    const content = fs.readFileSync(path.join(essayPath, essay), { encoding: 'utf8' })
    const title = content.substring(content.indexOf('<h2>') + 4, content.indexOf('</h2>'))
    const body = content.substring(content.indexOf('<p>'))
    const abstract = body.substring(3, body.indexOf('</p>')).replace(/<.+?>/g, '')
    return {
      link: '/blog',
      atomLink: '/blog.atom',
      term: 'all',
      essay: {
        url: `/blog/${essay.substring(splitIndex + 1, essay.lastIndexOf('.'))}`,
        time,
        timeString: new Date(time).toISOString(),
        title,
        abstract,
        content: content.substring(content.indexOf('</h2>') + 5),
        tags: content.indexOf('<!--') === -1 ? [] : content.substring(
          content.indexOf('<!--') + 4,
          content.indexOf('-->')
        )
          .trim()
          .split(/\s+/)
          .filter(tag => !! tag)
          .map(tag => ({
            link: `/blog?topic=${tag}`,
            atomLink: `/blog.atom?topic=${tag}`,
            term: tag
          }))
          .sort(sortTags)
      }
    }
  })
  .sort((lhs, rhs) => rhs.essay.time - lhs.essay.time)

const { map: tagMap, set: tagSet } = essays.reduce((t, item) => {
  const { essay } = item
  essay.tags.forEach(tag => {
    const array = t.map.get(tag.term)
    const innerItem = { ...tag, essay }
    if (array) {
      array.push(innerItem)
    } else {
      t.map.set(tag.term, [ innerItem ])
      t.set.add(tag)
    }
  })
  return t
}, { map: new Map(), set: new Set() })
const tags = Array.from(tagSet)
  .sort(sortTags)

const lastEssay = {
  type: 'blog',
  what: essays[0].essay.title,
  when: new Date(essays[0].essay.time),
  url: essays[0].essay.url
}

module.exports = {
  getEssays () {
    return { essays, tags, tagMap }
  },

  getLastEssay () {
    return Promise.resolve(lastEssay)
  }
}

function sortTags (lhs, rhs) {
  return lhs.term.localeCompare(rhs.term, 'en', { sensitivity: 'base' })
}
