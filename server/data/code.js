'use strict'

const check = require('check-types')
const fs = require('fs')
const github = require('octokat')({ token: process.env.GITHUB_API_TOKEN })
const log = require('bunyan').createLogger({ name: 'data/code' })
const path = require('path')

const { Gitlab } = require('gitlab')
const gitlab = new Gitlab({ token: process.env.GITLAB_API_TOKEN })

const GITHUB_API_BASE = 'https://api.github.com'
const USER = 'philbooth'
const EMAIL = 'pmbooth@gmail.com'

const MONTHS = [
  'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
  'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
]

const LANGUAGES = new Set([
  'actionscript', 'ada', 'basic', 'c', 'c++', 'c#', 'clojure', 'clojurescript',
  'cobol', 'coffeescript', 'common lisp', 'd', 'dart', 'delphi', 'eiffel',
  'elixir', 'elm', 'erlang', 'euler', 'f#', 'forth', 'fortran', 'go', 'groovy',
  'haskell', 'haxe', 'io', 'java', 'javascript', 'julia', 'kotlin', 'lisp',
  'lua', 'matlab', 'miranda', 'nim', 'objective-c', 'ocaml', 'pascal', 'perl',
  'php', 'plpgsql', 'prolog', 'python', 'racket', 'ruby', 'rust', 'scala',
  'scheme', 'shell', 'smalltalk', 'sql', 'sqlpl', 'swift', 'tcl', 'typescript',
  'xbl', 'xslt'
])

const GITLAB_IGNORE = new Set([
  'complexity-report',
  'escomplex',
  'escomplex-ast-moz',
  'escomplex-coffee',
  'escomplex-js',
  'escomplex-traits',
  'get-off-my-log',
  'JSComplexity.org',
  'notes-mozlando-2015',
  'notes-renderconf-2016',
  'notes-velocityeu-2013',
  'notes-velocityus-2014',
  'SchemaBrute',
  'VisualSO'
])

const GITHUB_REPOS = [
  {
    repo: 'actix/actix-web',
    description: 'Web framework built on the actor model.'
  }, {
    repo: 'hapijs/catbox',
    description: 'Multi-strategy caching service for Hapi.js.'
  }, {
    repo: 'lognormal/boomerang',
    description: 'Client-side library for web performance measurement and reporting.'
  }, {
    repo: 'mehcode/config-rs',
    description: 'Layered configuration system for Rust applications.'
  }, {
    repo: 'mozilla/fxa-activity-metrics',
    description: 'Data pipeline scripts for importing Firefox Accounts event data to Redshift.'
  }, {
    repo: 'mozilla/fxa-amplitude-send'
  }, {
    repo: 'mozilla/fxa',
    description: 'Firefox Accounts microservices ecosystem.'
  }, {
    repo: 'mozilla/gecko-dev',
    description: 'Firefox web browser.'
  }, {
    repo: 'mozilla/node-convict',
    description: 'Configuration management for Node.js.'
  }, {
    repo: 'mozilla-services/syncstorage-rs',
    description: 'Firefox Sync storage server.'
  }, {
    repo: 'mozilla-services/tokenserver',
    description: 'Firefox Sync token server.'
  }, {
    description: 'GCP-secrets integrated config loader for Node.js.',
    repo: 'qatalog/gcp-config-node',
    user: 'phil-booth-qatalog',
  }, {
    repo: 'springernature/boomcatch'
  }, {
    repo: 'springernature/webpagetest-mapper',
    description: 'Maps JSON data from the WebPageTest API into human-readable document formats.'
  }
]

const ALTERNATIVE_URLS = {
  'philbooth/bfj': 'https://www.npmjs.com/package/bfj',
  'philbooth/check-types.js': 'https://www.npmjs.com/package/check-types',
  'philbooth/tryer': 'https://www.npmjs.com/package/tryer',
  'philbooth/uaparser-rs': 'https://crates.io/crates/fast-uaparser',
  'philbooth/unicode-bom': 'https://crates.io/crates/unicode-bom'
}

// `mozilla/gecko-dev` 500s, presumably because history is so big?
const STATIC_COMMITS = {
  'mozilla/gecko-dev': [
    {
      id: '6b63924827a58a443943d143c718b79c0e253975',
      shortId: '6b63924',
      comment: 'Bug 1300297 - Ensure FxA device id is cleared on password change. r=markh',
      time: '2016-09-03T04:17:00Z',
      date: '03-Sep-2016',
      url: 'https://github.com/mozilla/gecko-dev/commit/6b63924827a58a443943d143c718b79c0e253975'
    },
    {
      id: 'b1bab4c5a0ffbf617ffbff965c503694626b864c',
      shortId: 'b1bab4c',
      comment: 'Bug 1296328 - Update FxA device registration on password change. r=markh',
      time: '2016-08-19T08:18:00Z',
      date: '19-Aug-2016',
      url: 'https://github.com/mozilla/gecko-dev/commit/b1bab4c5a0ffbf617ffbff965c503694626b864c'
    },
    {
      id: '8a5646364ea53c37c96566faba7bd9e8d3735c53',
      shortId: '8a56463',
      comment: 'Bug 1227527 - Implement basic FxA device registration. r=markh',
      time: '2016-01-13T04:55:00Z',
      date: '13-Jan-2016',
      url: 'https://github.com/mozilla/gecko-dev/commit/8a5646364ea53c37c96566faba7bd9e8d3735c53'
    }
  ]
}

const LOCAL_DATA = '../../fixtures/code.json'

module.exports = {
  getRepos () {
    if (process.env.NODE_ENV === 'development') {
      return Promise.resolve(require(LOCAL_DATA))
    }

    return Promise.all([
      fetchGitlab().then(marshallGitlab),
      fetchGithub().then(marshallGithub)
    ])
    .then(([ gitlabRepos, githubRepos ]) => {
      if (process.env.NODE_ENV === 'development-write') {
        fs.writeFileSync(
          path.join(__dirname, LOCAL_DATA),
          JSON.stringify({
            gitlab: gitlabRepos,
            github: githubRepos
          }, null, '  ')
        )
      }

      return {
        gitlab: gitlabRepos,
        github: githubRepos
      }
    })
  },
  getLastCommit () {
    return fetchLastCommit()
  }
}

function fetchGitlab () {
  log.trace('fetchGitlab')

  return gitlab.Projects.all({
    // eslint-disable-next-line camelcase
    order_by: 'last_activity_at',
    owned: true,
    visibility: 'public'
  })
    .then(projects => Promise.all(
      projects.map(project => gitlab.Projects.languages(project.id)
        .then(languages => ({
          ...project,
          languages
        }))
      )
    ))
}

function marshallGitlab (repos) {
  log.trace('marshallGitlab')

  return repos
    .filter(repo => ! repo.fork && ! GITLAB_IGNORE.has(repo.name))
    //.sort((lhs, rhs) => Date.parse(rhs.last_activity_at) - Date.parse(lhs.last_activity_at))
    .map(repo => {
      log.debug({ repo }, 'marshallGitlab::map')

      return marshallRepo(repo, name => {
        if (name === 'FxHey') {
          return 'FxHey!'
        }

        return name
      }, data => {
        if (data.name === 'www') {
          return 'The source code for this website.'
        }

        return data.description
      })
    })
}

function marshallRepo (repo, name, describe) {
  const fullName = repo.fullName || repo.path_with_namespace

  return {
    repo: fullName,
    url: ALTERNATIVE_URLS[fullName] || repo.htmlUrl || repo.web_url,
    name: name(repo.name),
    description: describe(repo),
    stats: {
      stars: repo.stargazersCount || repo.star_count,
      forks: repo.forksCount || repo.forks_count,
      issues: repo.openIssuesCount || repo.open_issues_count
    },
    languages: Object.entries(repo.languages || {})
      .reduce((unsorted, [ language, lineCount ]) => {
        if (check.positive(lineCount) && LANGUAGES.has(language.toLowerCase())) {
          unsorted.push({ language, lineCount })
        }
        return unsorted
      }, [])
      .sort((lhs, rhs) => rhs.lineCount - lhs.lineCount)
      .slice(0, 3)
      .map(sorted => sorted.language)
      .join(', ')
  }
}

function fetchGithub () {
  log.trace('fetchGithub')

  return GITHUB_REPOS.reduce(async (promise, repo) => {
    const r = await promise

    try {
      const baseUrl = `${GITHUB_API_BASE}/repos/${repo.repo}`

      const author = repo.user || USER
      const data = await Promise.all([
        github.fromUrl(baseUrl)
          .fetch(),
        STATIC_COMMITS[repo.repo] ?
          [] :
          github.fromUrl(`${baseUrl}/commits`)
            .fetch({ author }),
        github.fromUrl(`${baseUrl}/languages`)
          .fetch(),
        author,
      ])

      return [ ...r, data ]
    } catch (error) {
      log.error(error)
      return r
    }
  }, [])
}

function marshallGithub (repos) {
  log.trace('marshallGithub')

  return repos
    .map(([ repo, commits, languages, author ], index) => {
      const duplicates = {}
      const githubRepo = marshallRepo(
        { ...repo, languages },
        () => repo.name,
        () => GITHUB_REPOS[index].description || repo.description
      )

      commits = commits || []

      githubRepo.commitsUrl = `${repo.htmlUrl}/commits?author=${author}`

      if (STATIC_COMMITS[repo.fullName]) {
        githubRepo.commits = STATIC_COMMITS[repo.fullName]
      } else {
        githubRepo.commits = commits
          .filter(commit => {
            const message = commit.commit.message
            if (! duplicates[message]) {
              duplicates[message] = message
              return message.indexOf('Merge pull request') === -1
            }

            return false
          })
          .map(commit => {
            log.debug({ commit }, 'marshallGithub::map')

            return {
              id: commit.sha,
              shortId: commit.sha.substr(0, 7),
              comment: `${marshallComment(commit.commit.message)}`,
              time: commit.commit.author.date,
              date: marshallDate(new Date(commit.commit.author.date)),
              url: commit.htmlUrl
            }
          })
          .slice(0, 3)
      }

      return githubRepo
    })
    .sort((lhs, rhs) => {
      const lhsCommit = lhs.commits[0]
      if (! lhsCommit) {
        return 1
      }

      const rhsCommit = rhs.commits[0]
      if (! rhsCommit) {
        return -1
      }

      return compareDates(lhsCommit, rhsCommit)
    })
}

function marshallComment (comment) {
  const index = comment.indexOf('\n')

  if (index === -1) {
    return comment
  }

  return comment.substr(0, index)
}

function marshallDate (date) {
  return `${marshallDayOfMonth(date)}-${marshallMonth(date)}-${date.getFullYear()}`
}

function marshallDayOfMonth (date) {
  const dayOfMonth = date.getDate()

  if (dayOfMonth < 10) {
    return `0${dayOfMonth}`
  }

  return dayOfMonth
}

function marshallMonth (date) {
  return MONTHS[date.getMonth()]
}

function compareDates (lhs, rhs) {
  const dates = {
    lhs: lhs.date.split('-'),
    rhs: rhs.date.split('-')
  }
  const [ lhd, lhm, lhy ] = dates.lhs
  const [ rhd, rhm, rhy ] = dates.rhs

  if (parseInt(lhy) < parseInt(rhy)) {
    return 1
  }

  if (parseInt(rhy) < parseInt(lhy)) {
    return -1
  }

  if (MONTHS.indexOf(lhm) < MONTHS.indexOf(rhm)) {
    return 1
  }

  if (MONTHS.indexOf(rhm) < MONTHS.indexOf(lhm)) {
    return -1
  }

  if (parseInt(lhd) < parseInt(rhd)) {
    return 1
  }

  if (parseInt(rhd) < parseInt(lhd)) {
    return -1
  }

  return 0
}

function fetchLastCommit () {
  log.trace('fetchLastCommit')

  let lastCommit

  return fetchUntil(
    github.fromUrl(`${GITHUB_API_BASE}/users/${USER}/events`).fetch, [], events => {
      return events.some(event => {
        return event.type === 'PushEvent' && event.payload.commits.reverse().some(commit => {
          if (commit.author.email === EMAIL) {
            lastCommit = {
              type: 'commit',
              url: commit.htmlUrl || commit.url
                .replace('//api.', '//')
                .replace('/repos/', '/')
                .replace('/commits/', '/commit/'),
              what: marshallComment(commit.message),
              where: event.repo.name,
              when: new Date(event.createdAt)
            }

            return true
          }

          return false
        })
      })
    }
  )
    .then(() => lastCommit)
    .catch(error => {
      log.debug({ error }, 'did not fetch last commit')
    })
}

function fetchUntil (fetch, results, done) {
  return new Promise((resolve, reject) => {
    fetch()
      .then(result => {
        results = results.concat(result)

        if (result.nextPage && ! done(result)) {
          return resolve(fetchUntil(result.nextPage, results, done))
        }

        resolve(results)
      })
      .catch(reject)
  })
}

