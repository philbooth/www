'use strict'

const log = require('bunyan').createLogger({ name: 'server/middleware' })
const useragent = require('useragent')

const routes = require('./routes').all.reduce((set, route) => set.add(route.path), new Set())
const essay = require('./routes/essay')
const photo = require('./routes/photo')

const ALLOWED_METHODS = new Set([ 'GET', 'HEAD' ])
const YEAR = require('./constants').YEAR / 1000
const CACHE_FOR_A_YEAR = [
  'public',
  'immutable',
  `max-age=${YEAR}`,
  `stale-while-revalidate=${YEAR}`,
  `stale-if-error=${YEAR}`
].join(', ')

const { NODE_ENV } = process.env

useragent(true)

module.exports = viewData => {
  return {
    redirectToCanonicalUrl,
    logRequest,
    rewriteCacheableUrl,
    handlePath,
    handleDynamicRequest
  }

  function handlePath (request, response, next) {
    const { headers, method, path } = request

    if (! ALLOWED_METHODS.has(method)) {
      response.sendStatus(404)
      return
    }

    switch (path) {
      case '/style/main.css':
        response.set('ETag', `"${viewData.hashes.style.main}"`)
      case '/cv.pdf':
      case '/philbooth.pdf':
      case '/resume.pdf':
      case '/favicon-32.png':
      case '/favicon-16.png':
      case '/favicon.ico':
      case '/robots.txt':
      case '/sitemap.xml':
      case '/google37c78afc60247a94.html':
        next()
        break
      case '/style/dashboard.css':
        response.set('ETag', `"${viewData.hashes.style.dashboard}"`)
        next()
        break
      case '/style/cv.css':
        response.set('ETag', `"${viewData.hashes.style.cv}"`)
        next()
        break
      case '/fonts/patua-one-v6-latin-regular.woff2':
        response.set('ETag', `"${viewData.hashes.fonts.patua.woff2}"`)
        next()
        break
      case '/fonts/patua-one-v6-latin-regular.woff':
        response.set('ETag', `"${viewData.hashes.fonts.patua.woff}"`)
        next()
        break
      case '/fonts/patua-one-v6-latin-regular.ttf':
        response.set('ETag', `"${viewData.hashes.fonts.patua.ttf}"`)
        next()
        break
      case '/fonts/roboto-v16-latin-regular.woff2':
        response.set('ETag', `"${viewData.hashes.fonts.robotoRegular.woff2}"`)
        next()
        break
      case '/fonts/roboto-v16-latin-regular.woff':
        response.set('ETag', `"${viewData.hashes.fonts.robotoRegular.woff}"`)
        next()
        break
      case '/fonts/roboto-v16-latin-regular.ttf':
        response.set('ETag', `"${viewData.hashes.fonts.robotoRegular.ttf}"`)
        next()
        break
      case '/fonts/roboto-v16-latin-700.woff2':
        response.set('ETag', `"${viewData.hashes.fonts.robotoHeavy.woff2}"`)
        next()
        break
      case '/fonts/roboto-v16-latin-700.woff':
        response.set('ETag', `"${viewData.hashes.fonts.robotoHeavy.woff}"`)
        next()
        break
      case '/fonts/roboto-v16-latin-700.ttf':
        response.set('ETag', `"${viewData.hashes.fonts.robotoHeavy.ttf}"`)
        next()
        break
      default:
        if (routes.has(path)) {
          next()
          break
        }

        if (/^\/blog\//.test(path)) {
          essay.handler(viewData, request, response, next)
          break
        }

        if (/^\/photos\//.test(path)) {
          photo.handler(viewData, request, response, next)
          break
        }

        if (/^\/images\//.test(path)) {
          const imageFileName = request.path.substr(path.lastIndexOf('/') + 1)
          const imageHash = viewData.hashes.images[imageFileName]
          if (imageHash) {
            response.set('ETag', `"${imageHash}"`)
            next()
            break
          }
        }

        log.error({ path, userAgent: headers['user-agent'] }, 'not found')
        response.sendStatus(404)
        return
    }
  }
}

function redirectToCanonicalUrl (request, response, next) {
  const host = request.hostname
  const canonicalHost = host.replace(/^www\.(.+)$/i, '$1')
  const protocol = request.headers['x-forwarded-proto']
  const isProduction = NODE_ENV === 'production'

  // eslint-disable-next-line no-extra-parens
  if (! isProduction || (canonicalHost.length === host.length && protocol === 'https')) {
    return next()
  }

  const canonicalUrl = joinUrl('https', canonicalHost, request.path, request.query)

  log.warn({ canonicalUrl }, '301 redirect')

  response.redirect(301, canonicalUrl)
}

function joinUrl (protocol, host, path, query) {
  let queryString = ''

  if (query) {
    const params = Object.entries(query)

    if (params.length > 0) {
      queryString = `?${
        params
          .map(([ key, value ]) => `${key}=${value}`)
          .join('&')
      }`
    }
  }

  return `${protocol}://${host}${path}${queryString}`
}

function logRequest (request, response, next) {
  log.trace({
    method: request.method,
    protocol: request.protocol,
    host: request.hostname,
    path: request.path,
    query: request.query,
  }, 'received')

  next()
}

function rewriteCacheableUrl (request, response, next) {
  const url = request.url.replace(/\/([^\/]+)\.[0-9a-f]{40}\.([0-9a-z]+)$/i, '/$1.$2')

  if (url.length !== request.url.length) {
    request.url = url
    response.set('Cache-Control', CACHE_FOR_A_YEAR)
  }

  next()
}

function handleDynamicRequest (request, response) {
  const etag = response.locals.etag
  if (etag) {
    if (request.headers['if-none-match'] === `W/${etag}`) {
      response.sendStatus(304)
    } else {
      response.set('ETag', `W/"${etag}"`)
    }
  }

  let options
  const { viewName } = response.locals
  if (viewName === 'cv') {
    options = {
      layout: 'cv'
    }
  }
  response.render(viewName, options)
}
