'use strict'

const fs = require('fs')
const path = require('path')
const express = require('express')
const handlebars = require('express-handlebars').create({
  defaultLayout: 'main',
  extname: 'html',
  helpers: require('./view-helpers')
})
const log = require('bunyan').createLogger({ name: 'server' })
const compression = require('compression')
const hash = require('./hash')
const bodyParser = require('body-parser')
const routes = require('./routes')
const { WEEK } = require('./constants')

const MAIN_STYLE_PATH = path.join(__dirname, '../public/style/main.css')
const DASHBOARD_STYLE_PATH = path.join(__dirname, '../public/style/dashboard.css')
const CV_STYLE_PATH = path.join(__dirname, '../public/style/cv.css')
const PATUA_WOFF2_PATH = path.join(__dirname, '../public/fonts/patua-one-v6-latin-regular.woff2')
const PATUA_WOFF_PATH = path.join(__dirname, '../public/fonts/patua-one-v6-latin-regular.woff')
const PATUA_TTF_PATH = path.join(__dirname, '../public/fonts/patua-one-v6-latin-regular.ttf')
const ROBOTO_REGULAR_WOFF2_PATH = path.join(__dirname, '../public/fonts/roboto-v16-latin-regular.woff2')
const ROBOTO_REGULAR_WOFF_PATH = path.join(__dirname, '../public/fonts/roboto-v16-latin-regular.woff')
const ROBOTO_REGULAR_TTF_PATH = path.join(__dirname, '../public/fonts/roboto-v16-latin-regular.ttf')
const ROBOTO_HEAVY_WOFF2_PATH = path.join(__dirname, '../public/fonts/roboto-v16-latin-700.woff2')
const ROBOTO_HEAVY_WOFF_PATH = path.join(__dirname, '../public/fonts/roboto-v16-latin-700.woff')
const ROBOTO_HEAVY_TTF_PATH = path.join(__dirname, '../public/fonts/roboto-v16-latin-700.ttf')

const IMAGES_PATH = path.join(__dirname, '../public/images')
const IMAGE_PATHS = fs.readdirSync(IMAGES_PATH).map(image => path.join(IMAGES_PATH, image))

const viewData = {
  title: 'Phil Booth',
  hashes: {
    style: {
      main: hash(fs.readFileSync(MAIN_STYLE_PATH)),
      dashboard: hash(fs.readFileSync(DASHBOARD_STYLE_PATH)),
      cv: hash(fs.readFileSync(CV_STYLE_PATH))
    },
    fonts: {
      patua: {
        woff2: hash(fs.readFileSync(PATUA_WOFF2_PATH)),
        woff: hash(fs.readFileSync(PATUA_WOFF_PATH)),
        ttf: hash(fs.readFileSync(PATUA_TTF_PATH))
      },
      robotoRegular: {
        woff2: hash(fs.readFileSync(ROBOTO_REGULAR_WOFF2_PATH)),
        woff: hash(fs.readFileSync(ROBOTO_REGULAR_WOFF_PATH)),
        ttf: hash(fs.readFileSync(ROBOTO_REGULAR_TTF_PATH))
      },
      robotoHeavy: {
        woff2: hash(fs.readFileSync(ROBOTO_HEAVY_WOFF2_PATH)),
        woff: hash(fs.readFileSync(ROBOTO_HEAVY_WOFF_PATH)),
        ttf: hash(fs.readFileSync(ROBOTO_HEAVY_TTF_PATH))
      }
    },
    images: IMAGE_PATHS.reduce((map, imagePath) => {
      map[imagePath.substr(imagePath.lastIndexOf('/') + 1)] = hash(fs.readFileSync(imagePath))
      return map
    }, {})
  },
  copyright: 'Phil Booth'
}
const middleware = require('./middleware')(viewData)

const app = express()

app.engine('html', handlebars.engine)
app.set('view engine', 'html')
app.set('view cache', true)
app.set('trust proxy', true)
app.set('x-powered-by', false)
app.set('etag', false)

app.use(middleware.redirectToCanonicalUrl)
app.use(middleware.logRequest)
app.use(compression())
app.use(middleware.rewriteCacheableUrl)
app.use(middleware.handlePath)
app.use(express.static('public', { maxAge: WEEK, etag: false, lastModified: false }))
app.use(bodyParser.urlencoded({ extended: false, limit: '10kb' }))

routes.all.forEach(route => {
  const routeViewData = Object.assign({
    routes: routes.view.map(viewDataRoute => ({
      path: viewDataRoute.path,
      isLink: viewDataRoute.path !== route.path,
      navTitle: viewDataRoute.navTitle
    }))
  }, viewData)
  app[route.method || 'get'](route.path, (request, response, next) => {
    route.handler(routeViewData, request, response, next)
  })
})

app.use(middleware.handleDynamicRequest)

const port = process.env.PORT || 8080
app.listen(port, () =>
  log.info({ port }, 'listening')
)

