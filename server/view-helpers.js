'use strict'

const check = require('check-types')

module.exports = {
  formatInteger,

  formatFloat (number) {
    if (
      check.not.number(number) ||
      number % 1 === 0 ||
      number.toString() === number.toFixed(1)
    ) {
      return formatInteger(number)
    }

    return formatInteger(Number(number.toFixed(2)))
  },

  add (lhs, rhs) {
    return lhs + rhs
  },

  minus (lhs, rhs) {
    return lhs - rhs
  },

  halve (number) {
    return number / 2
  },

  capitalise (string) {
    if (! string) {
      return
    }

    return string.substr(0, 1).toUpperCase() + string.substr(1)
  },

  plural (number, unit) {
    if (number === 1) {
      return `1 ${unit}`
    }

    return `${formatInteger(number)} ${unit}s`
  },

  debug (...args) {
    console.log('#####   D E B U G   #####')
    console.log('template context:')
    console.log(this)
    if (args.length === 2) {
      console.log('template value:')
      console.log(args[0])
    }
  }
}

function formatInteger (number) {
  if (! number) {
    return number
  }

  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

