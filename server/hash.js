'use strict'

const log = require('bunyan').createLogger({ name: 'server/hash' })
const crypto = require('crypto')

module.exports = string => {
  log.debug({ string }, 'hash')

  return crypto
    .createHash('sha1')
    .update(string)
    .digest('hex')
}

