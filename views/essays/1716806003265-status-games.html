<h2>Status games</h2>
<!-- status startups teams -->
<p class="article-time"><em><time datetime="2024-05-27T10:33:23.265Z">27<sup>th</sup> May 2024</time></em></p>
<p>There's a fascinating chapter in <a href="https://www.keithjohnstone.com/">Keith Johnstone</a>'s <a href="https://www.bloomsbury.com/uk/impro-9781350069053/"><em>Impro</em></a>,
called <em>Status</em>.
The context is improvisational theatre
but it contains some real wisdom about life in general.
I've thought about it a lot over the years
and it's gradually informed some opinions I hold
about healthy and unhealthy ways
that engineering teams work together.</p>
<p>The chapter opens:</p>
<blockquote>
<p>When I began teaching at the Royal Court Theatre Studio (1963),
I noticed that the actors couldn't reproduce &quot;ordinary&quot; conversation.
They said &quot;Talky scenes are dull&quot;,
but the conversations they acted out
were nothing like those I overheard in life.
For some weeks I experimented with scenes
in which two strangers met and interacted,
and I tried saying &quot;No jokes&quot;,
and &quot;Don't try to be clever&quot;,
but the work remained unconvincing.
They had no way to mark time
and allow situations to develop,
they were forever striving
to latch on to &quot;interesting&quot; ideas.
If casual conversations really were motiveless,
and operated by chance,
why was it impossible to reproduce them at the studio?</p>
<p>I was preoccupied with this problem
when I saw the Moscow Art's production
of <em>The Cherry Orchard</em>.
Everyone on stage seemed to have chosen
the <em>strongest</em> possible motives for each action -
no doubt the production had been &quot;improved&quot;
in the decades since Stanislavsky directed it.
The effect was &quot;theatrical&quot;
but not like life as I knew it.
I asked myself for the first time
what were the <em>weakest</em> possible motives,
the motives that the characters I was watching
might really have had.
When I returned to the studio
I set the first of my status exercises.</p>
<p>&quot;Try to get your status
just a little above or below your partner's&quot;,
I said,
and I insisted the gap should be minimal.
The actors seemed to know exactly what I meant
and the work was transformed.
The scenes became &quot;authentic&quot;
and actors seemed marvellously observant.
Suddenly we understood
that every inflection and movement implies a status,
and that no action is due to chance,
or really &quot;motiveless&quot;.
It was hysterically funny,
but at the same time very alarming.
All our secret manoeuverings were exposed.
If someone asked a question
we didn't bother to answer it,
we concentrated on why it had been asked.
No one could make an &quot;innocuous&quot; remark
without everyone instantly grasping what lay behind it.</p>
</blockquote>
<p>It continues brilliantly from there
and is definitely worth reading in full,
even if you're not interested in the performing arts.</p>
<h3>The origins of status</h3>
<p><a href="https://en.wikipedia.org/wiki/Sociality">Social hierarchies are a natural byproduct of evolution</a>
because animals that co-operate are more likely to survive
and replicate their genes into future generations.
There are examples of this throughout nature:
in insects, reptiles, birds, fish, mammals
and of course in funny bald apes
that walk around on their hind legs.
Within those social structures there's space
for all kinds of successful strategies.
Some animals will be dominant,
others will be submissive,
many will modify their behaviour depending on circumstance.
Fundamentally this is where status comes from.
It's had 800 million years or so to evolve
and is deeply woven into the fabric of our existence.</p>
<p>Because of all that evolutionary pressure,
there are many little cues and behaviours
embedded in our personalities,
which help to make society work.
The way we stand or sit,
the way we speak,
the direction we choose to look,
what we do with our hands;
these are all ways in which we project our status.
Every interaction between people
is filled with these little status transactions
and most of the time we don't even realise it.
Sometimes status conflicts occur,
where two people simultaneously try to assume higher (or lower) status.
Those conflicts can result in feelings of anger or disappointment,
which then go on to influence future interactions
between the people involved.</p>
<p>This stuff happens all the time
and you start seeing it everywhere
once you know to look out for it.
Because of <em>Impro</em>
I like to call them <em>status games</em>,
although they're not what that term means in the book.
In a work context,
you could also call them office politics
or team dynamics.</p>
<h3>Status in engineering organisations</h3>
<p>I really enjoy working in startups
but I've come to realise they're especially fragile
to the effects of status games between engineers.
The odds against a startup succeeding are huge.
For success to happen,
a lot of things have to go improbably well.
It really needs the whole team working in harmony,
towards a common objective.
If one person decides they want to pull in a different direction,
it can ruin everything.
This is where status games come in.
Large, established organisations can rumble on
without being derailed by them.
But in startups
there isn't the time or space
for that distraction.</p>
<p>Here are some concrete ways
that I've seen engineers pathologically try to elevate their own status:</p>
<ul>
<li>Build a new thing that doesn't need to exist.</li>
<li>Build a thing using different technology
that the rest of the team are unfamiliar with.</li>
<li>Rewrite from scratch something that was already working fine.</li>
<li>Work on easy changes that touch many lines of code but don't impact the business.</li>
<li>Claim to be working on big and/or complicated changes,
which nobody else has visibility of.</li>
<li>Claim sole or majority credit for something that was a joint effort.</li>
<li>Make noisy contributions to discussion without meaningfully progressing it.</li>
</ul>
<p>And here are some ways
that I've seen engineers pathologically try to lower other people's status:</p>
<ul>
<li>Shoot down an idea or proposal without an objective reason.</li>
<li>Leave <a href="/blog/the-art-of-good-code-review">code review</a> comments saying that something should be done differently,
without meaningfully improving it.</li>
<li>Identify another person as the reason something broke or went wrong in production.</li>
<li>Cc or @mention someone's manager when you think they're wrong about something.</li>
</ul>
<p>When challenged on these behaviours,
people will often try to post-rationalise them
with some reasonable sounding justification.
Especially when technical issues are concerned,
it's important to try and separate
status games from the subject matter.
Technical decisions should be based on merit,
not the relative status of the protagonists.</p>
<h3>Defusing status games</h3>
<p>Like many things,
status games have a tendency to escalate.
Left unchecked,
they get worse and worse
as people compete increasingly
to attain higher status.
The best way to de-escalate is to shine a light on it.
Call out what you see
and why you think it's happening,
without passing judgement on either side of the conflict.
Remember that we're hardwired for this behaviour,
so acknowledge it then forgive and move on.
It's important for the whole team to see
that status games achieve nothing for the protagonists,
although be careful that your own contribution
does not itself turn into a status game.</p>
<p>If you find yourself locked in a status conflict,
try not to aggravate it.
Discuss it rationally with the other person.
Sometimes it's prudent to publicly assume lower status
and quickly defuse the situation,
but you should still discuss it later
because the underlying conflict may fester
(and sometimes assuming low status
becomes a high-status tactic unintentionally).</p>
<p>None of this is to say that all disagreement is bad or wrong.
Reasoned, logical, evidence-based debate is good and healthy.
Just follow the evidence and be rational about outcomes.</p>
<aside>
<p class="smallprint jobspam">
Are you hiring?
Because I'm looking for my next role!
See <a href="https://files.philbooth.me/philbooth.cv.pdf" rel="nofollow">my CV</a>.
</p>
</aside>