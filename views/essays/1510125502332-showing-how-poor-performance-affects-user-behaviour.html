<h2>Showing how poor performance affects user behaviour</h2>
<!-- web-performance data-visualization fxa mozilla devops -->
<p class="article-time"><em><time datetime="2017-11-08T07:18:22.332Z">8<sup>th</sup> November 2017</time></em></p>
<p>&quot;Bad performance hurts user engagement&quot;
is a sentiment that feels intuitively true
but can be hard to sell in product conversations.
The best way to persuade a non-believer
is to point them at hard evidence
and for that you need
to do a few things
with your performance data.</p>
<p>Firstly, don't store it in a silo.
That might sound obvious
but silos occur incidentally
for all kinds of organisational reasons
that can be hard to resolve.
<a href="http://www.melconway.com/Home/Conways_Law.html">Conway's law</a>
is real and causes inertia.
If you want to draw meaningful inferences from your data,
it must be possible to link it
to other events that track user behaviour.</p>
<p>Consider the following graph
(numbers redacted):</p>
<p><img src="https://assets.philbooth.me/images/fxa-perf-percentiles-1.e66d28d61ce1bab83577c3ed4a58df57516e5206.png" alt="A chart that shows user engagment on Firefox Accounts is positively correlated with page load performance" /></p>
<p>Here, the x-axis is time
and the y-axis is the count of users
that successfully signed in to Firefox Accounts.
The different coloured lines
represent evenly-sized cohorts of users,
grouped according to how quickly
the initial page loaded.
If web performance
had no effect on user behaviour,
those lines would roughly overlay each other.
Instead, they show that the 10% of users
who experienced the fastest initial page load
were about twice as likely
to sign in to their account,
as the 10% of users who experienced the slowest.
If you're having trouble convincing others
to invest engineering effort in improving performance,
having graphs like this can help.
And they're only possible
if you can directly link performance data
to user events.</p>
<p>Secondly,
be careful if you're using a time-series database
like <a href="https://graphiteapp.org/">Graphite</a>.
These are often configured
to gradually expire data,
which is good for storage costs
but bad for precision.
The expiry is achieved by storing averages
of your metrics in progressively coarser-grained buckets
as they get older,
which reduces precision
and can produce strange jumps
at the bucket boundaries
when rendered in a chart.
People looking at those
might mistake them for genuine
performance changes.</p>
<p>Instead of a time-series database,
consider storing your data
somewhere that can handle it <em>en masse</em>.
We use <a href="https://aws.amazon.com/documentation/redshift/">Redshift</a> for this,
maintaining parallel datasets
that trade history length for sample rate.
We have an unsampled set
that contains 3 months of history,
a 50%-sampled set that goes back 6 months
and a 10%-sampled set that includes 2 years.
After the initial sampling is performed for each set,
we keep all of the data until it expires.
Data is automatically popped from the end of each set
as we push it at the start.
Limiting the size of the sets
keeps query times reasonable.
In practice,
we rarely need to consult the sampled histories
but it's good to have them available
for occasional longer-term analysis
and maintaining them in parallel
means we can always compare like with like.
Medians and percentiles are computed on the fly inside queries,
which can be scheduled to run at regular intervals in the background
so we don't have to wait for them every time we want to view a chart.</p>
<p>Finally,
produce multiple visualisations
of your data.
For instance,
the chart I showed earlier
has a partner that maps the percentile boundaries
to concrete timings:</p>
<p><img src="https://assets.philbooth.me/images/fxa-perf-percentiles-2.346a62c0ba60c5010cc20473b026b2f570612f15.png" alt="A chart that maps performance percentiles to concrete timing values" /></p>
<p>This is useful,
but it's a bit annoying
to look at a separate chart
when you want to know the timing value.
So we have another chart
that tries to combine the work of both
into a single view:</p>
<p><img src="https://assets.philbooth.me/images/fxa-perf-bands-1.3168484c066ee9828da658b2a90c69068d584647.png" alt="A chart that shows the percent success in concrete timing bands" /></p>
<p>Here, each band is between concrete timing values
and the y-axis is the percent of users in each band
that successfully signed in to their account.
It tells the same story as before,
with some extra spikiness
due to the interaction between
weekend behaviour and performance.
Backing up an argument from one chart
with evidence from another
makes our case more compelling.</p>
<p>It also helps make our dashboards
useful for other purposes,
like keeping an eye on performance regressions
and tracking how we're doing over time.
Here are the same 1-second timing bands
rendered to show how many people are in each:</p>
<p><img src="https://assets.philbooth.me/images/fxa-perf-bands-2.93f9adb9d6570637ae035d3de133c5f0ac901a28.png" alt="A chart that shows the distribution of users within concrete timing bands" /></p>
<p>Ideally,
we want to see users moving
from the red bands at the top of the chart
towards the green bands at the bottom.
If the opposite occurs,
we're doing something wrong
and we know how that affects user behaviour.</p>
<aside>
<p class="smallprint jobspam">
Are you hiring?
Because I'm looking for my next role!
See <a href="https://files.philbooth.me/philbooth.cv.pdf" rel="nofollow">my CV</a>.
</p>
</aside>