<h2>Things my dog taught me about software development</h2>
<!-- dogs mentoring personal refactoring teams -->
<p class="article-time"><em><time datetime="2021-10-16T10:54:54.507Z">16<sup>th</sup> October 2021</time></em></p>
<p>I've always wanted a dog,
but could never get one
because I lived in rented accomodation.
Happily that changed last year
so as soon as I could,
I bought a puppy.
This is what I learned
about dogs, people and code.</p>
<p><img src="https://assets.philbooth.me/images/milo-ball-672.jpg" alt="Picture of my dog, Milo, on the day I brought him home" /></p>
<h3>1. Don't be surprised when puppies do puppy stuff 🐶</h3>
<p>As an engineer of somewhat advanced years,
something I've struggled with at times
is being patient with less experienced devs.
I try to be patient
and I know impatience is bad,
but there are times when it feels like
I'm suppressing an urge to be exasperated.</p>
<p>Puppies are tiny, cute bundles of joy.
They are impossible not to love.
They try very hard to be good little boys and girls,
and look to you for guidance.
But also they want to chew everything,
dig holes in your lawn,
jump up on your furniture
and go for a pee or a poo whenever they feel like it.
Obviously it would be stupid to get upset about any of that,
so patiently you guide them to make better choices
and gradually they learn how to behave.</p>
<p>And so it goes for junior engineers.
They're on a journey and trying to improve.
There are things they haven't learned yet
but it's a consequence of their experience,
not lack of ability.
If they're confidently doing the wrong thing,
it's an effect rather than a cause.</p>
<p><img src="https://assets.philbooth.me/images/milo-shoe-672.jpg" alt="Picture of Milo with a stolen shoe in his possession" /></p>
<h3>2. Reward them when they do good things 🍪</h3>
<p>Guidance comes in two forms:
calmly saying no to bad behaviours
and enthusiastically rewarding good ones.
You should do lots more of the second one
than the first.</p>
<p>When saying no,
it doesn't help to get angry about anything
because that won't produce the desired outcome.
If you get angry,
they'll get worked up too
and you enter an arms race
of who can be the most outraged.
It doesn't lead anywhere.</p>
<p>For puppies,
give them a clear and calm &quot;no&quot;
then ignore any further attempts at bad behaviour,
removing them from the situation if you have to.
For junior engineers,
explain the downsides of what they're trying to do
and why alternative solutions don't suffer from them.
Point out better options using broad enough strokes
that they still have some latitude for discovery and creativity
in reaching the right answer.
Don't suck the joy out of programming.</p>
<p>When rewarding,
you want your rewards to be fun
and memorable.</p>
<p>For puppies
that means changing your tone of voice,
giving them tasty treats,
belly rubs,
all the things they enjoy.
And your reward must come right after whatever it's for,
so they know why you're rewarding them.</p>
<p>Most of the above applies to engineers too,
although belly rubs are not advised.
Make sure the reward is something they actually value.
Some people like praise in public,
but some people hate the limelight that brings.
Make the reward commensurate to the size of the task
and make it clear why you're rewarding them.
For code reviews,
call out specific parts of the diff that are good
instead of leaving a blanket 👍 on the PR.
If someone is consistently excellent,
speak to your boss and
help them get a promotion or a pay rise.</p>
<p><img src="https://assets.philbooth.me/images/milo-treat-672.jpg" alt="Picture of Milo eating a treat" /></p>
<h3>3. Don't hold grudges 😠</h3>
<p>After you've said no
to your puppy/engineer,
it's important to let go of the subject
and move on.
Not doing that will cause problems on two fronts.</p>
<p>First,
it will make them think you're an unreasonable old meany.
They'll get scared
and gradually your relationship will become one of fear.
They will not produce their best work
under those conditions.</p>
<p>Second,
it will set the tone inside your own head
for how you interact with them in future.
Negativity begets more negativity,
it's habitual.
Eventually, if you don't keep a lid on it,
the <em>Ghost of Christmas Past</em> will visit
and show you what you used to be like.
Don't turn into Ebenezer Scrooge.</p>
<p><img src="https://assets.philbooth.me/images/milo-grass-672.jpg" alt="Picture of Milo sitting in some long grass" /></p>
<h3>4. Clear up any mess that's left behind 💩</h3>
<p>It's beholden on us all
to create the world we want to live in.
When you walk your puppy,
you take little plastic bags with you
and scoop up their mess.
When they chew through a soft toy
so that all the stuffing spills out
onto the living room floor,
you clean it away
before they can swallow any of it.
Accidents happen,
you clear up afterwards.</p>
<p>The same applies to engineering too,
except we call it refactoring.
Code mess is a natural byproduct of feature development
that can paralyse teams if left unchecked.
The metaphor of technical debt is well understood,
but it can still be hard to get buy-in
from more product-minded parts of the team
when it comes to prioritisation.
The best solution to that
is to be constantly refactoring,
in small steps so that it's hardly noticed.
Bigger refactors will still be needed from time to time of course,
but you can significantly reduce their impact
if you clean up any mess as you go.
Mess that's left behind eventually develops a life of its own
and is that much harder to deal with as a result.</p>
<p><img src="https://assets.philbooth.me/images/milo-seed-672.jpg" alt="Picture of Milo with grass seed all over his face" /></p>
<h3>5. Make time for play 🧸</h3>
<p>For puppies,
play is an important part of their development
and a way to increase the bond between you.
It's also an opportunity to provide more guidance.
If you're playing tug,
have some treats on hand
and use them to gradually teach the puppy
a &quot;drop&quot; or &quot;give&quot; command.
You can utilise your puppy's instinct for chasing
to start teaching them &quot;come&quot;.</p>
<p>Engineering is rarely a solo pursuit
and teams that only communicate around work tasks
are missing an opportunity for bonding too.
It can be difficult when working remotely
but small things like taking five minutes at the start of meetings just to chat,
can develop empathy among the team
and encourage healthier interaction between team-mates.
Deep technical conversations go much better
when you like the people you're having them with.</p>
<p><img src="https://assets.philbooth.me/images/milo-bear-672.jpg" alt="Picture of Milo with his favourite toy bear" /></p>
