<h2>The elegance of Rust</h2>
<!-- rust traits how-to -->
<p class="article-time"><em><time datetime="2018-05-16T10:04:26.740Z">16<sup>th</sup> May 2018</time></em></p>
<p>Here's three little Rust tricks
that I learned in the last week or so.
Each struck me as being an elegant approach
to working cleanly within the confines
of a strongly-typed language.</p>
<ol>
<li>
<p>Say you have a bunch of custom error types
in different places,
each tailored to the specific requirements
of some module or function.
When errors from lower levels
bubble up through higher ones,
they need to be transformed to the correct type.
What's a nice way to do that?</p>
<p>The solution comes in two parts.
Firstly, implement the <a href="https://doc.rust-lang.org/std/convert/trait.From.html"><code>From</code></a> trait
for the higher-level error,
specifying the lower-level error as the type argument:</p>
<pre><code>impl From&lt;DbError&gt; for ApiError {
    fn from(value: DbError) -&gt; ApiError {
        ApiError {
            // ...
        }
    }
}
</code></pre>
<p>Secondly, call <a href="https://doc.rust-lang.org/std/result/enum.Result.html#method.map_err"><code>Result::map_err</code></a>
when you want to transform the lower-level error
into the higher-level one:</p>
<pre><code>pub fn do_something(&amp;self) -&gt; Result&lt;Foo, ApiError&gt; {
    // db.query returns Result&lt;Foo, DbError&gt;
    self.db.query(self.query)
        .map_err(From::from)
}
</code></pre>
</li>
<li>
<p>Sticking with the <code>Result</code> theme,
what should you do
if you want to fold/reduce
over an iterator
using a function that can fail?</p>
<p>Coming from a JS background,
in the past I might have thrown from <code>reduce</code>
and silently cursed myself over using <code>throw</code> for flow control.
But in Rust, <a href="https://doc.rust-lang.org/std/iter/trait.Iterator.html#method.try_fold"><code>Iterator::try_fold</code></a>
comes to the rescue:</p>
<pre><code>let remaining_pizza = pizza_slices
    .iter()
    .try_fold(0, |consumed, slice| {
        if consumed + slice &gt;= 360 {
            Err(())
        } else {
            Ok(consumed + slice)
        }
    })
    .map(|consumed| 360 - consumed);
</code></pre>
</li>
<li>
<p>Maybe there are parts of your code
that can only be tested if you use mocks
to force execution along specific paths.
Or maybe you want to use mocks for other reasons,
like making your tests faster
or just ensuring that failures are properly isolated.
At first this can seem tricky in a strongly-typed language,
but the end result is actually better and more robust
than a dynamically-typed language can achieve.</p>
<p>The key is to promote the type to a trait:</p>
<pre><code>pub trait Emailer {
    fn send_email(&amp;self, message: EmailMessage) -&gt; Result&lt;u64, EmailError&gt;;
    fn get_delivery_status(&amp;self, message_id: u64) -&gt; Result&lt;EmailStatus, EmailError&gt;;
}

pub struct EmailClient {
    // ...
}

impl EmailClient {
    pub fn new() -&gt; EmailClient {
        EmailClient {
            // ...
        }
   }
}

impl Emailer for EmailClient {
    fn send_email(&amp;self, message: EmailMessage) -&gt; Result&lt;u64, EmailError&gt; {
        // ...
    }

    fn get_delivery_status(&amp;self, message_id: u64) -&gt; Result&lt;EmailStatus, EmailError&gt; {
        // ...
    }
}
</code></pre>
<p>Then, if you change the code that uses <code>EmailClient</code>
to expect <code>Box&lt;Emailer&gt;</code> instead,
you can create all kinds of weird and wonderful
mock email clients in your test modules:</p>
<pre><code>pub struct EmailMockFailsOnSend;

impl Emailer for EmailMockFailsOnSend {
    fn send_email(&amp;self, message: EmailMessage) -&gt; Result&lt;u64, EmailError&gt; {
        Err(EmailError::new(&quot;wibble&quot;))
    }

    fn get_delivery_status(&amp;self, message_id: u64) -&gt; Result&lt;EmailStatus, EmailError&gt; {
        // ...
    }
}

#[test]
fn test_some_behaviour_when_email_send_fails() {
    let result = super::do_something(Box::new(EmailMockFailsOnSend));
    // ...
}
</code></pre>
<p>The really nice thing about this approach
is that now your mock objects are all strongly-typed too.
If someone changes the <code>Emailer</code> trait,
they can't forget to update the mock implementations
because the build will fail.
Dynamically-typed languages might make mocking easier,
but they can't offer that guarantee.</p>
</li>
</ol>
<aside>
<p class="smallprint jobspam">
Are you hiring?
Because I'm looking for my next role!
See <a href="https://files.philbooth.me/philbooth.cv.pdf" rel="nofollow">my CV</a>.
</p>
</aside>