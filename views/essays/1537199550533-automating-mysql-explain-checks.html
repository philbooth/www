<h2>Automating MySQL EXPLAIN checks</h2>
<!-- javascript nodejs mysql linting automation tools fxa mozilla databases how-to devops -->
<p class="article-time"><em><time datetime="2018-09-17T15:52:30.533Z">17<sup>th</sup> September 2018</time></em></p>
<p>About a month ago,
we had an outage
caused by a slow-running query in MySQL.
This particular slow query
wasn't spotted when it was deployed
because it depended on data
inserted by client browsers
and the related preference in Firefox
was not enabled at that point.
A few weeks after it shipped,
the client pref was flipped on
and as the table grew,
it slowed down MySQL increasingly
until the whole of Firefox Accounts
became unresponsive.
And of course,
because <a href="https://en.wikipedia.org/wiki/Sod%27s_law">Sod's Law</a>
is one of the fundamental forces of nature,
this happened late on a Friday night.
There were some complicating factors
that slowed down diagnosis,
but it's also fair to say
we could have caught it at source
with an <code>EXPLAIN</code> of the offending query
during code review.
Because of that,
I decided to try and automate
<code>EXPLAIN</code> checks for our MySQL queries.</p>
<p>My broad objective was to write a script
that extracts individual queries from stored procedures,
turns them into <code>EXPLAIN</code> statements
and then feeds those into MySQL.
The results would then be used
to emit warnings and fail pull requests
when bad smells like <code>filesort</code> or a full table scan
are detected.</p>
<h3>Identifying procedure names</h3>
<p>Our stored procedures are versioned
and the old versions
continue to live in the tree
so that reverse migrations
can be applied if necessary.
The first hurdle, then,
is to identify which stored procedure versions
are currently active in the codebase.</p>
<p>This proves to be as simple as
grepping for <code>CALL</code> statements
in our JavaScript code,
then using <code>awk</code> and <code>cut</code>
to pull out the name
of the called procedure:</p>
<pre><code>grep 'CALL ' lib/db/mysql.js | awk -F 'CALL +' '{print $2}' | cut -d '(' -f 1
</code></pre>
<p>Similarly,
the relevant source file for each procedure
can be found by grepping
for <code>CREATE PROCEDURE</code> statements.
Here <code>git grep</code> is used
because it includes the file name
in the output:</p>
<pre><code>git grep &quot;CREATE PROCEDURE `*$PROCEDURE&quot; | cut -d ':' -f 1
</code></pre>
<h3>Extracting SELECTs</h3>
<p>The next job
is to extract queries
from those stored procedures
and turn them into valid <code>EXPLAIN</code> statements.
For the first cut,
I am ignoring
<code>INSERT</code>, <code>UPDATE</code> and <code>DELETE</code>,
and just focusing on <code>SELECT</code> instead.
If that proves valuable,
it should be pretty straightforward
for a future pull request
to transform other query types
to an equivalent <code>SELECT</code>
and build an <code>EXPLAIN</code> from that.</p>
<p>For this step
I wanted to parse the SQL
but none of the off-the-shelf parsers
in npm
are able to process
our stored procedures.
That's okay though
because we don't actually need
a complete parser,
we just need a solution
that's good enough
to extract individual queries
from our codebase.</p>
<p>Looking at the code,
the majority of our stored procedures
conform to some basic assumptions:</p>
<ul>
<li>There are never multiple queries on a single line.</li>
<li><code>CREATE PROCEDURE</code> and its matching <code>END;</code> start at column 1.</li>
<li>Arguments to procedures are named either <code>inXxx</code> or <code>xxxArg</code>.</li>
<li>SQL comment delimiters never appear inside string literals.</li>
</ul>
<p>That means we can process our SQL files line by line
and use simple regular expressions
to pull out individual <code>SELECT</code> queries:</p>
<pre><code>const COMMENT = /--.+$/
const CREATE_PROCEDURE = /^CREATE PROCEDURE `?([A-Z]+_[0-9]+)/i
const END_PROCEDURE = /^END;$/i
const SELECT = /^\s*SELECT/i

function extractSelects (path, procedure) {
  let isProcedure = false, isSelect = false

  const src = fs.readFileSync(path, { encoding: 'utf8' })
  const lines = src.split('\n')

  return lines
    .reduce((selects, line) =&gt; {
      line = line.replace(COMMENT, '')

      if (isProcedure) {
        if (END_PROCEDURE.test(line)) {
          isProcedure = isSelect = false
        } else {
          if (isSelect) {
            selects[selects.length - 1] += ` ${line.trim()}`
          } else if (SELECT.test(line)) {
            selects.push(line.trim())
            isSelect = true
          }

          if (line.indexOf(';') !== -1) {
            isSelect = false
          }
        }
      } else if (procedure) {
        const match = CREATE_PROCEDURE.exec(line)

        if (match &amp;&amp; match.length === 2 &amp;&amp; match[1] === procedure) {
          isProcedure = true
        }
      } else {
        isProcedure = CREATE_PROCEDURE.test(line)
      }

      return selects
    }, [])
    .map(select =&gt; purgeUnbalancedParentheses(select))
}

function purgeUnbalancedParentheses (select) {
  const openingCount = select.split('(').length
  const closingCount = select.split(')').length

  if (openingCount &lt; closingCount) {
    for (let i = 0; i &lt; closingCount - openingCount; ++i) {
      const index = select.lastIndexOf(')')
      select = select.substr(0, index) + select.substr(index + 1)
    }
  } else if (openingCount &gt; closingCount) {
    for (let i = 0; i &lt; openingCount - closingCount; ++i) {
      const index = select.indexOf('(')
      select = select.substr(0, index) + select.substr(index + 1)
    }
  }

  return select
}
</code></pre>
<p>This is good enough to work
for 90% of our queries.
That's fine
because currently we automate <code>EXPLAIN</code> checks
for 0% of our queries,
so it's still a significant improvement
even though it's not perfect.</p>
<h3>Replacing arguments</h3>
<p>In many cases,
the code above returns a string
that looks like this:</p>
<pre><code>SELECT * FROM table WHERE column = columnArg;
</code></pre>
<p>Here <code>columnArg</code> is the name of
an argument to the stored procedure
the query comes from.
Running it through MySQL
outside that context
will fail,
because it doesn't know
what <code>columnArg</code> is.
So for the <code>EXPLAIN</code> to work,
we must replace the argument name
with a literal value.
And the literal value
needs to be of the correct type
in order for MySQL to perform
valid analysis.</p>
<p>To fix this,
we can rewind to the beginning of the script
and add a step that inserts
a bunch of known values
into the database.
Then we can write a function
that replaces arguments
by looking up the argument name
in a dictionary of known values
and making the appropriate replacement:</p>
<pre><code>function replaceArgs (query) {
  return query
    .replace(/([ \(])`?in((?:[A-Z][A-Za-z]+)+)`?/g, replaceArg)
    .replace(/([ \(])`?([a-z]+(?:[A-Z][A-Za-z]+)*)Arg`?/g, replaceArg)
}

function replaceArg (match, delimiter, arg) {
  arg = arg.toLowerCase()

  let value = KNOWN_ARGS.has(arg) ? KNOWN_ARGS.get(arg) : ''

  if (Buffer.isBuffer(value)) {
    value = `UNHEX(&quot;${value.toString('hex')}&quot;)`
  } else if (typeof value === 'string') {
    value = `&quot;${value}&quot;`
  }

  return `${delimiter}${value}`
}
</code></pre>
<h3>Interpreting results</h3>
<p>All that's left now
is to prefix each <code>SELECT</code> with <code>EXPLAIN</code>,
run it through MySQL
and interpret the results.
There are two columns of interest
in the <code>EXPLAIN</code> result set,
<code>type</code> and <code>Extra</code>.
These can be set to many values,
but I'm mostly interested in
four particular bad smells:</p>
<ul>
<li>
<p>If <code>type</code> is <code>ALL</code>,
it means the query is performing
a full table scan.</p>
</li>
<li>
<p>If <code>type</code> is <code>index</code>,
it means the query is doing
a full index scan.</p>
</li>
<li>
<p>If <code>Extra</code> includes <code>using filesort</code>,
it means an expensive sort operation
is involved.</p>
</li>
<li>
<p>If <code>Extra</code> includes <code>using temporary table</code>,
it means a temporary table
is being created.</p>
</li>
</ul>
<p>With those in mind,
the following function
can be used to turn
the <code>EXPLAIN</code> results
into an array of warnings:</p>
<pre><code>const TYPE_FULL_TABLE_SCAN = /^all$/i
const TYPE_FULL_INDEX_SCAN = /^index$/i
const EXTRA_FILESORT = /filesort/i
const EXTRA_TEMPORARY_TABLE = /temporary/i

function warn (explainRows) {
  return explainRows.reduce((warnings, row) =&gt; {
    if (TYPE_FULL_TABLE_SCAN.test(row.type)) {
      warnings.push('full table scan')
    } else if (TYPE_FULL_INDEX_SCAN.test(row.type)) {
      warnings.push('full index scan')
    }

    if (EXTRA_FILESORT.test(row.Extra)) {
      warnings.push('filesort')
    }

    if (EXTRA_TEMPORARY_TABLE.test(row.Extra)) {
      warnings.push('temporary table')
    }

    return warnings
  }, [])
}
</code></pre>
<h3>Ignoring procedures</h3>
<p>The last thing I want to add
is a mechanism for ignoring
specific stored procedures.
This is so we can turn it on in CI
without it failing for queries
that we know can't be parsed yet.
Then, any new query
that causes the script to fail
will get a big red ❌
against its respective pull request.</p>
<p>Assuming the existence of a file called <code>.explain-ignore</code>,
containing one procedure name per line,
we can parse it into a JS <code>Set</code> instance
like so:</p>
<pre><code>function parseIgnoreFile () {
  return new Set(
    fs.readFileSync('.explain-ignore', { encoding: 'utf8' })
      .split('\n')
      .map(procedure =&gt; procedure.trim())
      .filter(procedure =&gt; !! procedure)
  )
}
</code></pre>
<p>Now we can filter our list of procedures
and skip processing
the ones that we want to ignore.</p>
<h3>The final script</h3>
<p>The end result of all this
is a script that I merged
into our database repo today.
You can see the final script
in the <a href="https://github.com/mozilla/fxa-auth-db-mysql/pull/392/files#diff-2c3da3dc0602c0cd50f7e9bdb87cf254">pull request</a>.
It's also running <a href="https://travis-ci.org/mozilla/fxa-auth-db-mysql/builds/429543944#L2169">in CI</a>
and the output looks like this:</p>
<pre><code>$ npm run explain
&gt; fxa-auth-db-mysql@1.120.0 explain /home/travis/build/mozilla/fxa-auth-db-mysql
&gt; node scripts/explain-warn
Found 0 warnings and failed to explain 0 queries.
The command &quot;npm run explain&quot; exited with 0.
</code></pre>
<aside>
<p class="smallprint jobspam">
Are you hiring?
Because I'm looking for my next role!
See <a href="https://files.philbooth.me/philbooth.cv.pdf" rel="nofollow">my CV</a>.
</p>
</aside>