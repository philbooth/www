<h2>How to evaluate dependencies</h2>
<!-- how-to dependencies -->
<p class="article-time"><em><time datetime="2023-02-25T15:20:51.661Z">25<sup>th</sup> February 2023</time></em></p>
<p>One of my stock interview questions goes:
&quot;When picking between dependencies to use in production,
what factors contribute to your decision?&quot;
I'm surprised by how often
I receive an answer along the lines of
&quot;Github stars&quot; and not much else.
I happen to think Github stars is a terrible metric
for selecting production code,
so this post sets out my idea
of a healthier framework to evaluate dependencies.</p>
<h3>0. Read the documentation</h3>
<p>The starting point should always be <em>RTFM</em>.
The docs will be your guide to using this thing,
so make sure you understand them.</p>
<ul>
<li>
<p>Does it solve your problem?</p>
</li>
<li>
<p>Do you know how to use it?</p>
</li>
<li>
<p>Is there anything confusing about the instructions?</p>
</li>
<li>
<p>Is there a changelog or release notes?</p>
</li>
</ul>
<h3>1. Read the code</h3>
<p>After you've read the docs,
the absolute most important thing you can do
is read the code.
This can be an intimidating prospect,
especially if the functionality seems complicated
or is outside your normal area of expertise.
But even if you're not able to fully grok
the entirety of a codebase,
there are still many things it's possible to glean
by looking at it.</p>
<ul>
<li>
<p>How clean is it?
Is it broken down into functions and/or files
in a way that makes sense to you?</p>
</li>
<li>
<p>Does what you see in the code
match what you understood from the docs?
Does it adhere to the principle of least astonishment?</p>
</li>
<li>
<p>Does it contain other dependencies?
Are they up to date?
What do those other dependencies look like?
How deep does the rabbit hole go?</p>
</li>
<li>
<p>Does it do just the thing that you need,
or does it also cater for a bunch of other concerns
that don't match your usage?
Do those other concerns impact performance
or usability?</p>
</li>
<li>
<p>Does it receive untrusted input,
if so does it prevent that input from being abused?</p>
</li>
<li>
<p>Can you estimate a ballpark big-O for the implementation?
Look for nested loops or inappropriate datatypes
that could cause quadratic performance.</p>
</li>
<li>
<p>Are there tests?
How many?
Are the tests clean?
Can you run them?
Do they all pass?
How long do they take?</p>
</li>
<li>
<p>Try to imagine a situation
where you have to fork the code
and implement something yourself.
Does that feel comfortable or scary?</p>
</li>
</ul>
<h3>2. Look at project activity</h3>
<p>After looking at the docs and code,
try to get a sense of activity
on the project.</p>
<ul>
<li>
<p>How many unresolved issues are there?
How long have they been open?
Are any of the unresolved issues concerning?</p>
</li>
<li>
<p>How many resolved issues are there?
How quickly were they closed?
How helpful were the maintainers in addressing any issues?
Were any issues closed in disagreement with the reporters?</p>
</li>
<li>
<p>Look at the commit history.
Are the original maintainers still involved?
If the current maintainers aren't the original ones,
how long have they worked on it?
Are they working on the guts of the code,
or just touching the edges?</p>
</li>
<li>
<p>Look at the release history.
How often do new releases come out?
How often are there breaking API changes?
Is the cadence something you'd be comfortable keeping up with?</p>
</li>
</ul>
<h3>3. Look at project stats</h3>
<p>Next try to get a feel for the community around the project.</p>
<ul>
<li>
<p>How many downstream dependents are there?</p>
</li>
<li>
<p>How many downloads per day/week?</p>
</li>
<li>
<p>Are other people successfully using it in prod?</p>
</li>
</ul>
<p>If you really want to consider Github stars at this point,
do so but understand that starring a project on Github
does not signify any specific intention.
Some people may use it like an upvote
but for others it's just a reminder system.
Mostly Github stars seem to correlate
with how aggressively a project has been promoted
on social media.</p>
<h3>4. Make a decision</h3>
<p>Fundamentally,
using a dependency is not a one-off decision
that you can subsequently forget about.
Dependencies are more like a liability or a tax,
and a prerequisite to your decision
is working out the long-term cost.
You must do the research so that cost can be calculated,
then weigh it up against the alternatives
(and one of those alternatives
might be implementing it yourself).</p>
<aside>
<p class="smallprint jobspam">
Are you hiring?
Because I'm looking for my next role!
See <a href="https://files.philbooth.me/philbooth.cv.pdf" rel="nofollow">my CV</a>.
</p>
</aside>