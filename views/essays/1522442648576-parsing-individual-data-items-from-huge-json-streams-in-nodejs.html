<h2>Parsing individual data items from huge JSON streams in Node.js</h2>
<!-- json javascript nodejs streams bfj how-to -->
<p class="article-time"><em><time datetime="2018-03-30T20:44:08.576Z">30<sup>th</sup> March 2018</time></em></p>
<p>Let's say you have a huge amount of JSON data
and you want to parse values from it in Node.js.
Perhaps it's stored in a file on disk
or, more trickily,
it's on a remote machine
and you don't want to download the entire thing
just to get some data from it.
And even if it is on the local file system,
the thing is so huge that reading it in to memory
and calling <code>JSON.parse</code>
will crash the process
with an out-of-memory exception.
Today I implemented a new method
for my async JSON-parsing lib, <a href="https://gitlab.com/philbooth/bfj">BFJ</a>,
which has exactly this type of scenario in mind.</p>
<p>BFJ already had a bunch of methods
for parsing and serialising
large amounts of JSON <em>en masse</em>,
so I won't go into those here.
The <a href="https://gitlab.com/philbooth/bfj/blob/master/README.md#bfj">readme</a>
is a good place to start
if you want to learn more.
Instead,
this post is going to focus on
the new method, <code>match</code>,
which is concerned with picking individual records
from a larger set.</p>
<p><code>match</code> takes 3 arguments:</p>
<ol>
<li>
<p>A <a href="https://nodejs.org/api/stream.html#stream_readable_streams">readable stream</a>
containing the JSON input.</p>
</li>
<li>
<p>A selector argument,
used to identify matches from the stream.
This can be
a string, a regular expression or a predicate function.
Strings and regular expressions are used
to match against property keys.
Predicate functions are called for each item in the data
and passed two arguments,
<code>key</code> and <code>value</code>.
Whenever the predicate returns <code>true</code>,
that value will be pushed to the stream.</p>
</li>
<li>
<p>An optional options object.</p>
</li>
</ol>
<p>It returns a readable, object-mode stream
that will receive the matched items.</p>
<p>Enough chit-chat, let's see some example code!</p>
<pre><code>const bfj = require('bfj');

// Stream user objects from a file on disk
bfj.match(fs.createReadStream(path), 'user')
  .pipe(createUserStream());

// Stream all the odd-numbered items from an array
bfj.match(fs.createReadStream(path), /[13579]$/)
  .pipe(createOddIndexStream());

// Stream everything that looks like an email address from some remote resource
const request = require('request');
bfj.match(request(url), (key, value) =&gt; emailAddressRegex.test(value))
  .pipe(createEmailAddressStream());
</code></pre>
<p>Those examples do not try
to load all of the data into memory
in one hit.
Instead they parse the data sequentially,
pushing a value to the returned stream
whenever they find a match.
The parse also happens asynchronously,
yielding at regular intervals
so as not to monopolise the event loop.</p>
<p>The approach can be used to parse items
from multiple JSON objects in a single source, too,
by setting the <code>ndjson</code> option to <code>true</code>.
For example,
say you have a log file
containing structured JSON data
logged by <a href="https://github.com/trentm/node-bunyan">Bunyan</a>
or <a href="https://github.com/winstonjs/winston">Winston</a>.
Specifying <code>ndjson</code> will cause BFJ
to treat newline characters as delimiters,
allowing you to pull out interesting values
from each line in the log:</p>
<pre><code>// Stream uids from a logfile
bfj.match(fs.createReadStream(logpath), 'uid', { ndjson: true })
  .pipe(createUidStream());
</code></pre>
<p>If you need to handle errors from the stream,
you can do that by attaching event handlers:</p>
<pre><code>const outstream = bfj.match(instream, selector);
outstream.on('data', value =&gt; {
  // A matching value was found
});
outstream.on('dataError', error =&gt; {
  // A syntax error was found in the JSON data
});
outstream.on('error', error =&gt; {
  // An operational error occurred
});
outstream.on('end', value =&gt; {
  // The end of the stream was reached
});
</code></pre>
<p>There's lots more information
in the <a href="https://gitlab.com/philbooth/bfj/blob/master/README.md#bfj">readme</a> so,
if any of this sounds interesting,
I encourage you to take a look!</p>
<aside>
<p class="smallprint jobspam">
Are you hiring?
Because I'm looking for my next role!
See <a href="https://files.philbooth.me/philbooth.cv.pdf" rel="nofollow">my CV</a>.
</p>
</aside>