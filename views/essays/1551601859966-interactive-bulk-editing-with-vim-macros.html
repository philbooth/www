<h2>Interactive bulk editing with vim macros</h2>
<!-- vim automation tools text-editors macros how-to -->
<p class="article-time"><em><time datetime="2019-03-03T08:30:59.966Z">3<sup>rd</sup> March 2019</time></em></p>
<p>Along with all the fun, creative stuff
it enables you to do,
programming sometimes requires you to carry out
boring and repetitive editing operations.
If those operations are uniformly applicable,
it's straightforward to automate them
using regular expressions
and a tool like <code>sed</code>,
or <code>:%s/foo/bar/g</code> in vim-speak.
But sometimes a regex can't express
the pattern you want to match against
and on those occasions,
vim macros can come to the rescue.</p>
<h3>Case study</h3>
<p>Let's look at a concrete example
to see what I mean.
JavaScript fat arrow functions
have different semantics for <code>this</code>
to regular functions,
binding it to the value of <code>this</code>
from their parent context.
If you want to bulk edit
a bunch of function expressions
to fat arrows,
you must also inspect the body of each function
to see whether it references <code>this</code>.
A simple regular expression match can't do that,
so instead you need
to step through the matches one-by-one
and decide whether to apply the change
after eyeballing each particular block of code.</p>
<p>Consider this code
that uses <a href="https://mochajs.org/">mocha</a>
to run some unit tests:</p>
<pre><code>describe('a unit test suite', function () {
  let result;

  before(function (done) {
    result = foo(done);
  });

  it('returned the correct result', function () {
    assert.equal(result, 'expected result');
  });

  it('flaky test', function () {
    this.retries(3);
    assert.didNotThrow(bar);
  });
});
</code></pre>
<p>Here,
the final test calls <code>this.retries</code>,
so is not suitable
to be replaced by a fat arrow.
Additionally,
we must be careful to preserve
the optional <code>done</code> argument
in the <code>before</code> callback.
In the real world of course,
the example might be hundreds of lines long
and spread over multiple source modules.
Doing it manually
is tedious busywork.</p>
<p>The first step to bulk editing this with macros
is to search for a match pattern.
For our example code,
we can do that by typing the following
in vim's command mode:</p>
<pre><code>/function
</code></pre>
<p>...then hitting <code>Enter</code>.</p>
<p>That will land the cursor
on the first function
in our listing:</p>
<pre><code>describe('a unit test suite', function () {
</code></pre>
<p>It doesn't reference <code>this</code>,
so can be replaced with a fat arrow.
We'll do that while recording a macro,
so the same macro
can be applied to subsequent functions too.</p>
<p>Macros are recorded
by typing <code>q</code> in command mode,
followed by a character
to identify which register
it should be stored in.
As we're replacing functions here,
let's use the letter <code>f</code>:</p>
<pre><code>qf
</code></pre>
<p>We're now in macro-recording mode
and every action we take
will be added to the macro,
including both movement and edits.
The first action we need to perform
is to delete the <code>function</code> keyword
using <code>dw</code>.</p>
<p>Next we want to move the cursor
to the closing parenthesis,
but it's important to do so in a way
that will work for
all subsequent macro invocations.
We can't simply use <code>l</code>
because that wouldn't move past
the optional <code>done</code> argument.
Likewise <code>w</code> would behave inconsistently
for the same reason.
By moving straight to the closing parenthesis
using <code>%</code> instead,
it ensures we'll always skip past
any intervening parameters.</p>
<p>With the cursor positioned
over the closing parenthesis,
we can append text with <code>a</code> and add the fat arrow.
Then we can return to command mode with <code>Escape</code>
and stop recording the macro with <code>q</code>.
The macro is now stored in register <code>f</code>
and the edited line of code
looks like this:</p>
<pre><code>describe('a unit test suite', () =&gt; {
</code></pre>
<p>To recap,
the full sequence of keystrokes
that we went through
to record this macro were:</p>
<pre><code>qfdw%a =&gt;^[q
</code></pre>
<p>(where <code>^[</code> is the <code>Escape</code> key)</p>
<p>At this point we're all set,
and can begin stepping
through the remaining functions
with <code>n</code>.
That takes the cursor
to the next function
in our example listing,
which is the <code>before</code> callback:</p>
<pre><code>before(function (done) {
</code></pre>
<p>This is another valid case for replacement,
so we can apply the macro
that we just recorded
by typing <code>@</code>
followed by the appropriate register letter,
in our case <code>f</code>:</p>
<pre><code>@f
</code></pre>
<p>In one fell swoop,
our macro is applied
and the line is changed
to a fat arrow expression:</p>
<pre><code>before((done) =&gt; {
</code></pre>
<p>Pressing <code>n</code> again
moves the cursor on
to the next function:</p>
<pre><code>it('returned the correct result', function () {
</code></pre>
<p>Once more
we have a valid candidate for replacement,
so can invoke our macro a second time.
We can do that with <code>@f</code> again,
or we can use the &quot;previous macro&quot; shortcut:</p>
<pre><code>@@
</code></pre>
<p>Either way,
the edited line now looks like so:</p>
<pre><code>it('returned the correct result', () =&gt; {
</code></pre>
<p>Pressing <code>n</code>
a further time
brings us to the function
that we <em>don't</em> want to to update
because it uses <code>this</code>:</p>
<pre><code>it('flaky test', function () {
  this.retries(3);
</code></pre>
<p>We can skip past this one with <code>n</code>
and continue working our way
through the rest of the codebase
in a similar fashion.</p>
<h3>Persisting macros</h3>
<p>Often
the macro you create
will be for a particular task
and you can forget about it
after that task is completed.
But occasionally
there will be macros
that you want to re-use
in the future
and it makes sense
to add those
to your <code>.vimrc</code> file,
to save the effort
of recording them again
every time
you need to apply them.
Doing that is easy.</p>
<p>Continuing with our previous example,
let's assume
the macro we want to save
is stored in register <code>f</code>.
We don't want to lose it
by terminating the session,
so we open the <code>.vimrc</code> file
in the same session instead:</p>
<pre><code>:e ~/.vimrc
</code></pre>
<p>Next,
we add a line to the <code>.vimrc</code> file
where we're going to save
the macro definition:</p>
<pre><code>let @f = ''
</code></pre>
<p>Now we want to
print the contents of register <code>f</code>
between the two quote characters.
We can do that
with the cursor over
the opening quote
by typing the following
in command mode:</p>
<pre><code>&quot;fp
</code></pre>
<p>Or if the cursor
is over the closing quote
of course:</p>
<pre><code>&quot;fP
</code></pre>
<p>Here <code>f</code> is just the register name,
so if our macro had been saved
in a different register
we'd replace <code>f</code>
with another letter instead.
Regardless,
the line will now look like this:</p>
<pre><code>let @f = 'dw%a =&gt;^['
</code></pre>
<p>(except the <code>^[</code>
will be a real
Unicode <code>Escape</code> character,
decimal <code>27</code> /
hex <code>1B</code>)</p>
<p>In a fresh editing session
you can now check
the macro has been saved
by finding a function
and typing <code>@f</code>
in command mode.
All being well,
you should see it change
to a fat arrow expression.</p>
<h3>Conclusion</h3>
<p>So, in summary:</p>
<ul>
<li>
<p>Record macros with <code>q</code>
followed by a register letter.</p>
</li>
<li>
<p>Both movement and edits will be included in the macro.</p>
</li>
<li>
<p>Finish recording by pressing <code>q</code> again.</p>
</li>
<li>
<p>Invoke a macro by pressing <code>@</code>
followed by a register letter.</p>
</li>
<li>
<p>Invoke the last-used macro
by pressing <code>@@</code>.</p>
</li>
<li>
<p>Macros are stored in registers
so can be printed using <code>&quot;xp</code>.</p>
</li>
<li>
<p>Save any commonly-used macros
to your <code>.vimrc</code> file
using the syntax <code>let @x = '...'</code>.</p>
</li>
</ul>
<aside>
<p class="smallprint jobspam">
Are you hiring?
Because I'm looking for my next role!
See <a href="https://files.philbooth.me/philbooth.cv.pdf" rel="nofollow">my CV</a>.
</p>
</aside>