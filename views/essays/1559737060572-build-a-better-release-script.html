<h2>Build a better release script</h2>
<!-- mozilla fxa automation tools javascript nodejs rust shell how-to devops -->
<p class="article-time"><em><time datetime="2019-06-05T12:17:40.572Z">5<sup>th</sup> June 2019</time></em></p>
<p>Automating your release process
saves time,
eliminates tedious busywork
and reduces the likelihood of mistakes
when cutting a new release.
There are plenty of off-the-shelf solutions available,
but this post will show
how easy it is to build your own release script
and why the end result can be better
than using a generic, third-party option.
Throughout the post
I'll use the case study
of some recent work we did
to <a href="https://github.com/mozilla/fxa/pull/680">automate the release process</a>
for <a href="https://github.com/mozilla/fxa">FxA</a>,
to provide concrete examples
of what I'm talking about.</p>
<h3>Why</h3>
<p>There's nothing wrong
with off-the-shelf solutions <em>per se</em>,
and if they work for you
that's great.
But many projects have
local idiosyncracies,
which a generic script
can't cater to by definition.
Perhaps the generic option
doesn't do some things your project needs,
or does them differently,
or it does other things
you don't want a release script to do.</p>
<p>On FxA,
after <a href="/blog/fxa-monorepo-migration">migrating all of our services to a monorepo</a>,
we wanted a single script
that works equally well
for our JavaScript and Rust codebases,
now they all live in one place.
We also need to co-ordinate version numbers
across public and private forks of our repository,
because some security-sensitive commits
only exist in the private fork.
And we have an FxA-specific step
of opening a deployment ticket in Bugzilla
for our Ops team,
copying notes into it
from various sources.</p>
<h3>Where to begin</h3>
<p>You should start by making a list
of everything that you and/or your team
have to do when it's time to cut
a new release.
Include every detail,
even if there are things
you don't think can be automated.
Once you have the complete list,
try to arrange it
into a rough chronological order.
Some things can happen in parallel
and the order won't matter for those,
but try to organise items
so they hang together coherently
in a single thread.</p>
<p>For FxA,
our list looked roughly like this:</p>
<ol>
<li>
<p>Make sure all the pull requests
labelled for the release
have been merged.
Check that no-one
has anything else
they want to land.</p>
</li>
<li>
<p>Create a branch
to cut the release from.</p>
</li>
<li>
<p>Bump the version strings
for all services that
have changed since the last release.
For JavaScript projects
the versions are stored in JSON files,
but for Rust
they're stored in TOML files.</p>
</li>
<li>
<p>Update each change log
with details of all the commits
made since the last release.</p>
</li>
<li>
<p>Commit those changes.</p>
</li>
<li>
<p>Create a git tag.</p>
</li>
<li>
<p>Create another release branch
from the private fork.</p>
</li>
<li>
<p>Merge the public release branch
into the private release branch.</p>
</li>
<li>
<p>Push the public branch and tag
to the public remote.</p>
</li>
<li>
<p>Push the private branch and tag
to the private remote.</p>
</li>
<li>
<p>Check the respective <a href="https://circleci.com/gh/mozilla/fxa">builds have passed in CI</a>
and images have been
<a href="https://hub.docker.com/search?q=mozilla%2Ffxa&amp;type=image">uploaded to DockerHub</a>.</p>
</li>
<li>
<p>Open a deployment ticket,
with links to the tags,
the builds,
the change logs,
deployment notes
and any requests for QA.</p>
</li>
<li>
<p>Open pull requests to merge
the release branches back
to their respective trunks.</p>
</li>
</ol>
<p>We also had
a slight variation to that list
for our patch releases,
where a pre-existing release branch is used
and there's no need to open
a new deployment ticket.
We wanted a script to handle
both workflows.</p>
<h3>Decide on scope</h3>
<p>When you have your complete list,
you can go through each item
and decide whether
it's in scope for automation,
or out of scope.
The in-scope items
are going to form the body of your script,
so the main qualification for an item being in scope
is deciding whether it can be automated.
The out-of-scope items
won't be ignored completely though.
The script can emit reminders
and even commands to copy/paste,
so people are less likely
to forget them.</p>
<p>For FxA,
items 2 through 10
were deemed to be in scope
for automation,
leaving items 1, 11, 12 and 13
as manual steps
requiring a human to help out.</p>
<h3>Pick a language</h3>
<p>Next,
you should decide
what language you're going to work in.
This decision should take into account
your own or your team's competencies,
but also the nature of the tasks
being automated.</p>
<p>If you're not considering
POSIX-compatible shell syntax
at this point,
I'd like to make a case for it.
The Bourne shell is virtually ubiquitous
in modern development environments,
with interpreters widely available
across a variety of environments
on Linux, MacOS and Windows.
Writing shell code that works with <code>git</code>
is immediately intuitive
because it uses the exact same interface
you're already familiar with
from the command line.
And other UNIX commands
such as <a href="http://pubs.opengroup.org/onlinepubs/9699919799/utilities/cut.html"><code>cut</code></a>,
<a href="http://pubs.opengroup.org/onlinepubs/9699919799/utilities/grep.html"><code>grep</code></a>,
<a href="http://pubs.opengroup.org/onlinepubs/9699919799/utilities/awk.html"><code>awk</code></a>
and <a href="http://pubs.opengroup.org/onlinepubs/9699919799/utilities/sed.html"><code>sed</code></a>
make working with git's output
straightforward too.</p>
<p>For FxA,
<code>/bin/sh</code> was an obvious choice
for all of the reasons above.
The only downside was that
not everyone felt fully comfortable
maintaining a shell script.
To mitigate that
we commented the script heavily.</p>
<p>All of the code examples that follow
are taken from the script
we wrote for FxA.
If you want to read the whole thing
in its entirety
before diving any deeper,
you can find it <a href="https://github.com/mozilla/fxa/blob/master/release.sh">here</a>.</p>
<h3>Enforce pre-requisites</h3>
<p>The first thing your script should do
is check pre-requisite conditions
and abort if any of them aren't met.
Aborting early
means your script is less likely
to fail part-way through,
leaving someone's local tree
in a bad state.</p>
<p>For FxA,
these pre-requisites include:</p>
<ul>
<li>
<p>Parsing a command-line argument
that indicates whether the release
is a major version
or a patch-level bump.</p>
</li>
<li>
<p>Checking the local tree contains no uncommitted changes.</p>
</li>
<li>
<p>Checking some commits have been made since the last tag.</p>
</li>
</ul>
<p>For the command-line argument,
we use the value <code>patch</code>
to indicate a patch-level bump
and the absence of any argument
indicates a major release,
or &quot;train&quot; in FxA-speak.
The code for checking it
looks like this:</p>
<pre><code>case &quot;$1&quot; in
  &quot;&quot;)
    BUILD_TYPE=&quot;Train&quot;
    ;;
  &quot;patch&quot;)
    BUILD_TYPE=&quot;Patch&quot;
    ;;
  *)
    echo &quot;Release aborted: Invalid argument \&quot;$1\&quot;&quot;
    exit 1
    ;;
esac
</code></pre>
<p>Keep an eye out for that <code>BUILD_TYPE</code> variable
as it will make a number of appearances
in later steps.</p>
<p>To check there are no uncommitted changes locally,
we use <code>git status</code>.
Passing it the <code>--porcelain</code> argument
makes it return parseable output
and if that result is not the empty string,
we error out:</p>
<pre><code>STATUS=`git status --porcelain`
if [ &quot;$STATUS&quot; != &quot;&quot; ]; then
  echo &quot;Release aborted: You have uncommited changes&quot;
  exit 1
fi
</code></pre>
<p>There's a small refactoring
we can make here,
extracting a function to abort the script
instead of repeating the pattern
of <code>echo</code> then <code>exit</code>
every time we want to fail:</p>
<pre><code>abort() {
  echo &quot;Release aborted: $1.&quot;
  exit 1
}
</code></pre>
<p>So the previous
status check
now looks like this:</p>
<pre><code>STATUS=`git status --porcelain`
if [ &quot;$STATUS&quot; != &quot;&quot; ]; then
  abort &quot;You have uncommited changes&quot;
fi
</code></pre>
<p>To check that some commits have been made
since the last tag,
there are a couple of different options.</p>
<p>Firstly,
you could just get
the most recent tag
that is reachable
from the current <code>HEAD</code>:</p>
<pre><code>LAST_TAG=`git describe --tags --first-parent --abbrev=0`
</code></pre>
<p>The <code>--first-parent</code> argument
prevents tags from merged branches
being selected,
which you may or may not want
depending on how branching is done
in your repository.</p>
<p>The alternative approach
is to sort all tags alphanumerically
and then pick the last one.
This works if your tags
aren't necessarily arranged chronologically
in your history:</p>
<pre><code>LAST_TAG=`git tag -l --sort=version:refname | tail -1`
</code></pre>
<p>For FxA,
we actually use one or the other,
depending on what type
of release it is:</p>
<pre><code>if [ &quot;$BUILD_TYPE&quot; = &quot;Train&quot; ]; then
  LAST_TAG=`git tag -l --sort=version:refname | tail -1`
else
  LAST_TAG=`git describe --tags --first-parent --abbrev=0`
fi
</code></pre>
<p>This allows us to tag patches
for older releases,
without more recent tags
causing a problem.</p>
<p>Regardless of how you identify the last tag,
you can check for  intervening commits
with <code>git log</code>:</p>
<pre><code>COMMITS=`git log $LAST_TAG..HEAD`
if [ &quot;$COMMITS&quot; = &quot;&quot; ]; then
  abort &quot;I see no work&quot;
fi
</code></pre>
<h3>Pull from remote branches</h3>
<p>It's easy to forget
to pull from remote branches
before cutting a release,
so it makes sense
to have the script
do that job for you too.
But some care has to be taken when doing so,
because the local repository
might be on a different branch
when the script is executed.
And at least in FxA's case,
the remote branch may or may not exist yet,
depending on what type of release it is.
If we're creating a new release branch from scratch,
we want to pull from <code>master</code>,
but if we're bumping an existing release,
we want to pull from the release branch.
And before we can do any of that,
we need to work out what the
name of the release branch actually <em>is</em>.</p>
<p>In FxA,
release branches are named like <code>train-$TRAIN</code>,
where <code>$TRAIN</code> is the train number
from the version string.
The version string is encoded in the tags,
which are of the form <code>v1.$TRAIN.$PATCH</code>,
e.g. at the time of writing
the current tag is <code>v1.138.4</code>.
The tags in our private fork
also have a <code>-private</code> suffix,
e.g. <code>v1.138.4-private</code>.
So we can get the name of the release branch
from the <code>LAST_TAG</code> variable
that we set earlier,
by splitting it into its constituent parts
using <code>cut</code>:</p>
<pre><code>MAJOR=`echo &quot;$LAST_TAG&quot; | cut -d '.' -f 1 | cut -d 'v' -f 2`
TRAIN=`echo &quot;$LAST_TAG&quot; | cut -d '.' -f 2`
PATCH=`echo &quot;$LAST_TAG&quot; | cut -d '.' -f 3 | cut -d '-' -f 1`
</code></pre>
<p>Here we've set
<code>MAJOR</code> to the substring
between the <code>v</code> and the first period,
<code>TRAIN</code> to the substring
between the first and second periods,
and <code>PATCH</code> to the substring
after the second period
(and before the hyphen,
if one exists).</p>
<p>Now that we've broken the version string
into its constituent parts,
we can determine the name of the release branch,
depending on what type of build it is:</p>
<pre><code>case &quot;$BUILD_TYPE&quot; in
  &quot;Train&quot;)
    NEW_TRAIN=`expr $TRAIN + 1`
    ;;
  &quot;Patch&quot;)
    NEW_TRAIN=&quot;$TRAIN&quot;
    ;;
esac
RELEASE_BRANCH=&quot;train-$NEW_TRAIN&quot;
</code></pre>
<p>So for major releases,
we increment the train number
using <a href="http://pubs.opengroup.org/onlinepubs/9699919799/utilities/expr.html"><code>expr</code></a>
and for patches,
we just re-use the current train number.</p>
<p>Now we know the branch name,
we can check which branch we're on locally.
If we're already on the release branch,
we just need to pull from <code>origin</code>
to get the latest changes.
If we're not,
we can look for
a remote release branch instead.
If one exists use that,
otherwise create a new branch
from <code>master</code>:</p>
<pre><code>CURRENT_BRANCH=`git branch --no-color | grep '^\*' | cut -d ' ' -f 2`

if [ &quot;$CURRENT_BRANCH&quot; = &quot;$RELEASE_BRANCH&quot; ]; then
  git pull origin &quot;$RELEASE_BRANCH&quot; &gt; /dev/null 2&gt;&amp;1 || true
else
  RELEASE_BRANCH_EXISTS=`git branch --no-color | awk '{$1=$1};1' | grep &quot;^$RELEASE_BRANCH\$&quot;` || true

  if [ &quot;$RELEASE_BRANCH_EXISTS&quot; = &quot;&quot; ]; then
    git fetch origin $RELEASE_BRANCH &gt; /dev/null 2&gt;&amp;1 || true

    REMOTE_BRANCH=&quot;origin/$RELEASE_BRANCH&quot;
    REMOTE_BRANCH_EXISTS=`git branch --no-color -r | awk '{$1=$1};1' | grep &quot;^$REMOTE_BRANCH\$&quot;` || true

    if [ &quot;$REMOTE_BRANCH_EXISTS&quot; = &quot;&quot; ]; then
      echo &quot;Warning: $RELEASE_BRANCH branch not found on local or remote, creating one from master.&quot;
      git checkout master &gt; /dev/null 2&gt;&amp;1
      git pull origin master &gt; /dev/null 2&gt;&amp;1
      git checkout -b &quot;$RELEASE_BRANCH&quot; &gt; /dev/null 2&gt;&amp;1
    else
      git checkout --track -b &quot;$RELEASE_BRANCH&quot; &quot;$REMOTE_BRANCH&quot; &gt; /dev/null 2&gt;&amp;1
    fi
  else
    git checkout &quot;$RELEASE_BRANCH&quot; &gt; /dev/null 2&gt;&amp;1
    git pull origin &quot;$RELEASE_BRANCH&quot; &gt; /dev/null 2&gt;&amp;1 || true
  fi
fi
</code></pre>
<p>There's a few things
worth calling out here:</p>
<ul>
<li>
<p>Some of these commands can legitimately fail
and we don't want them to abort the script
if we're running it with <a href="https://stackoverflow.com/questions/2870992/automatic-exit-from-bash-shell-script-on-error"><code>set -e</code></a>.
Appending <code>|| true</code> takes care of this.</p>
</li>
<li>
<p>They're quite noisy on the console
and we don't want the output from our script
getting too cluttered.
To keep them quiet,
we redirect <code>stdout</code> to the null device
using <code>&gt; /dev/null</code>.
In the cases where commands can fail,
we also redirect <code>stderr</code>
using <code>2&gt;&amp;1</code>.</p>
</li>
<li>
<p>When running <code>git branch</code>
we specify the <code>--no-color</code> option
to prevent any control codes
from leaking into the output.
Otherwise they'd break our assumptions with <code>grep</code>,
where we use <code>^</code> and <code>$</code> to specify
strict start/end-of-string matches.</p>
</li>
<li>
<p><code>awk '{$1=$1};1'</code> is a pattern
you'll see repeated a lot,
used to trim any space
from the start or end of a string.
The single quotes are important
because they prevent shell-expansion of <code>$1</code>,
passing it through to <code>awk</code> unadulterated.</p>
</li>
</ul>
<p>There is a problem introduced by this code
in that it changes the local branch,
so if the script aborts
the user will find themselves
in a different state
to when they ran the script.
Clearly that's unacceptable,
so we can fix it by moving
the assignment to <code>CURRENT_BRANCH</code>
to the very beginning of the script,
then changing our earlier definition
of the <code>abort</code> function
like so:</p>
<pre><code>abort() {
  git checkout &quot;$CURRENT_BRANCH&quot; &gt; /dev/null 2&gt;&amp;1
  echo &quot;Release aborted: $1.&quot;
  exit 1
}
</code></pre>
<h3>Bump version strings</h3>
<p>Now we're on the right branch
and have pulled latest changes,
we can get on with updating the version strings.
We already broke the current version string
down into its constituent parts
and bumped the train number,
so it's a minor tweak
to revisit that code
and bump the patch level too:</p>
<pre><code>case &quot;$BUILD_TYPE&quot; in
  &quot;Train&quot;)
    NEW_TRAIN=`expr $TRAIN + 1`
    NEW_PATCH=0
    ;;
  &quot;Patch&quot;)
    NEW_TRAIN=&quot;$TRAIN&quot;
    NEW_PATCH=`expr $PATCH + 1`
    ;;
esac
</code></pre>
<p>Then we can recombine the parts
and generate our new version string:</p>
<pre><code>NEW_VERSION=&quot;$MAJOR.$NEW_TRAIN.$NEW_PATCH&quot;
</code></pre>
<p>We're going to use <code>sed</code>
to update the version in a bunch of places,
so we need to turn the old version string
into a regular expression:</p>
<pre><code>LAST_VERSION_REGEX=&quot;$MAJOR\\.$TRAIN\\.$PATCH&quot;
</code></pre>
<p>We have to deal with two levels
of character escaping here,
because we want to escape the period
in the regex using a backslash
but to do that in a shell script,
we must first escape the backslash itself
using another backslash.</p>
<p>Updating the version strings
then happens in two parts.</p>
<p>First we have to loop through
each of the directories in our repo.
For this we're going to assume
the existence of a function called <code>update</code>
that we'll define in a moment:</p>
<pre><code>DIRECTORIES=&quot;packages/fxa-auth-db-mysql
packages/fxa-auth-server
packages/fxa-content-server
packages/fxa-customs-server
packages/fxa-email-event-proxy
packages/fxa-email-service
packages/fxa-event-broker
packages/fxa-profile-server&quot;

for DIRECTORY in $DIRECTORIES; do
  update &quot;$DIRECTORY&quot;
done
</code></pre>
<p>Then for each directory
we're going to look for files
where version strings might be stored
and use <code>sed</code> to update them.
We do that
in the implementation of <code>update</code>
like this:</p>
<pre><code>update() {
  if [ -f &quot;$1/package.json&quot; ]; then
    sed -i.release -e &quot;s/$LAST_VERSION_REGEX/$NEW_VERSION/g&quot; &quot;$1/package.json&quot;
    rm &quot;$1/package.json.release&quot;
  fi

  if [ -f &quot;$1/Cargo.toml&quot; ]; then
    sed -i.release -e &quot;s/$LAST_VERSION_REGEX/$NEW_VERSION/g&quot; &quot;$1/Cargo.toml&quot;
    rm &quot;$1/Cargo.toml.release&quot;
  fi
}
</code></pre>
<p>Note the <code>-i</code> option to <code>sed</code>
is optional in some environments
but we choose to include it
for maximum portability.</p>
<p>The nice thing with this pattern
is its ambivalence about the target language.
If the repo grows
to include projects
from other languages
like Ruby or Python,
it's simple to add
more invocations of <code>sed</code>
as needed.</p>
<h3>Update change logs</h3>
<p>For the change logs,
we'll extend our implementation
of the <code>update</code> function
to loosely parse
the first line of each commit message
for the target directory,
then write a sorted list of commits
to each package's <code>CHANGELOG.md</code>.</p>
<p>FxA follows the <a href="https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#commits">Angular.js conventions</a>
for formatting commit messages.
This means the first line of the commit
should be of the form:</p>
<pre><code>type(scope): summary message
</code></pre>
<p><code>type</code> is limited
to a strict set of values,
the most interesting of which
are <code>feat</code>, <code>fix</code>, <code>perf</code>, <code>refactor</code> and <code>revert</code>.
In our change logs,
we want to group commit messages
under each of those headings
to make it easier for readers
to find particular changes
they might be looking for.</p>
<p>We'll also include
the commit hash with the summary message,
to help anyone
who wants to link a change from the log
to the specific point in git's history.</p>
<p>To do this,
we'll add the following code
to the body of <code>update</code>:</p>
<pre><code>LOCAL_COMMITS=`git log $LAST_TAG..HEAD --no-color --pretty=oneline --abbrev-commit -- &quot;$1&quot;`

for COMMIT in $LOCAL_COMMITS; do
  HASH=`echo &quot;$COMMIT&quot; | cut -d ' ' -f 1`
  MESSAGE=`echo &quot;$COMMIT&quot; | cut -d ':' -f 2- | awk '{$1=$1};1'`
  TYPE=`echo &quot;$COMMIT&quot; | cut -d ' ' -f 2 | awk '{$1=$1};1' | cut -d ':' -f 1 | cut -d '(' -f 1 | awk '{$1=$1};1'`
  SCOPE=`echo &quot;$COMMIT&quot; | cut -d '(' -f 2 | cut -d ')' -f 1 | awk '{$1=$1};1'`

  if [ &quot;$SCOPE&quot; = &quot;$COMMIT&quot; ]; then
    SCOPE=&quot;&quot;
  fi

  if [ &quot;$SCOPE&quot; != &quot;&quot; ]; then
    SCOPE=&quot;$SCOPE: &quot;
  fi

  case &quot;$TYPE&quot; in
    &quot;&quot;)
      # Ignore blank lines
      ;;
    &quot;Merge&quot;)
      # Ignore merge commits
      ;;
    &quot;Release&quot;)
      # Ignore release commits
      ;;
    &quot;feat&quot;)
      if [ &quot;$FEAT_SUMMARY&quot; = &quot;&quot; ]; then
        FEAT_SUMMARY=&quot;### New features\n&quot;
      fi
      FEAT_SUMMARY=&quot;$FEAT_SUMMARY\n* $SCOPE$MESSAGE ($HASH)&quot;
      ;;
    &quot;fix&quot;)
      if [ &quot;$FIX_SUMMARY&quot; = &quot;&quot; ]; then
        FIX_SUMMARY=&quot;### Bug fixes\n&quot;
      fi
      FIX_SUMMARY=&quot;$FIX_SUMMARY\n* $SCOPE$MESSAGE ($HASH)&quot;
      ;;
    &quot;perf&quot;)
      if [ &quot;$PERF_SUMMARY&quot; = &quot;&quot; ]; then
        PERF_SUMMARY=&quot;### Performance improvements\n&quot;
      fi
      PERF_SUMMARY=&quot;$PERF_SUMMARY\n* $SCOPE$MESSAGE ($HASH)&quot;
      ;;
    &quot;refactor&quot;)
      if [ &quot;$REFACTOR_SUMMARY&quot; = &quot;&quot; ]; then
        REFACTOR_SUMMARY=&quot;### Refactorings\n&quot;
      fi
      REFACTOR_SUMMARY=&quot;$REFACTOR_SUMMARY\n* $SCOPE$MESSAGE ($HASH)&quot;
      ;;
    &quot;revert&quot;)
      if [ &quot;$REFACTOR_SUMMARY&quot; = &quot;&quot; ]; then
        REVERT_SUMMARY=&quot;### Reverted changes\n&quot;
      fi
      REVERT_SUMMARY=&quot;$REVERT_SUMMARY\n* $SCOPE$MESSAGE ($HASH)&quot;
      ;;
    *)
      if [ &quot;$OTHER_SUMMARY&quot; = &quot;&quot; ]; then
        OTHER_SUMMARY=&quot;### Other changes\n&quot;
      fi
      OTHER_SUMMARY=&quot;$OTHER_SUMMARY\n* $SCOPE$MESSAGE ($HASH)&quot;
      ;;
  esac

  if [ &quot;$FEAT_SUMMARY&quot; != &quot;&quot; ]; then
    FEAT_SUMMARY=&quot;$FEAT_SUMMARY\n\n&quot;
  fi

  if [ &quot;$FIX_SUMMARY&quot; != &quot;&quot; ]; then
    FIX_SUMMARY=&quot;$FIX_SUMMARY\n\n&quot;
  fi

  if [ &quot;$PERF_SUMMARY&quot; != &quot;&quot; ]; then
    PERF_SUMMARY=&quot;$PERF_SUMMARY\n\n&quot;
  fi

  if [ &quot;$REFACTOR_SUMMARY&quot; != &quot;&quot; ]; then
    REFACTOR_SUMMARY=&quot;$REFACTOR_SUMMARY\n\n&quot;
  fi

  if [ &quot;$REVERT_SUMMARY&quot; != &quot;&quot; ]; then
    REVERT_SUMMARY=&quot;$REVERT_SUMMARY\n\n&quot;
  fi

  if [ &quot;$OTHER_SUMMARY&quot; != &quot;&quot; ]; then
    OTHER_SUMMARY=&quot;$OTHER_SUMMARY\n\n&quot;
  fi

  SUMMARY=&quot;$FEAT_SUMMARY$FIX_SUMMARY$PERF_SUMMARY$REFACTOR_SUMMARY$OTHER_SUMMARY&quot;
  if [ &quot;$SUMMARY&quot; = &quot;&quot; ]; then
    SUMMARY=&quot;No changes.\n\n&quot;
  fi

  awk &quot;{ gsub(/^## $LAST_VERSION/, \&quot;## $NEW_VERSION\n\n$SUMMARY## $LAST_VERSION\&quot;) }; { print }&quot; &quot;CHANGELOG.md&quot; &gt; &quot;CHANGELOG.md.release&quot;
  mv &quot;CHANGELOG.md.release&quot; &quot;CHANGELOG.md&quot;
done
</code></pre>
<p>We use <code>cut</code> a number of times
to pull out the different components
of the commit message
and <code>awk</code> is used like before
to trim spaces from the resulting strings.
We then collect
the formatted messages
into different variables
and write them to the correct point
in the change log
using <code>awk</code>.</p>
<p>This code also uses a variable <code>LAST_VERSION</code>
that we haven't defined yet,
so we should scoot back
to where we defined <code>LAST_VERSION_REGEX</code>
and add this line
alongside it:</p>
<pre><code>LAST_VERSION=&quot;$MAJOR.$TRAIN.$PATCH&quot;
</code></pre>
<h3>Tag the release</h3>
<p>At this point,
the local tree will contain uncommitted changes
so we need to commit them:</p>
<pre><code>git commit -a -m &quot;Release $NEW_VERSION&quot;
</code></pre>
<p>Then we can create the tag:</p>
<pre><code>NEW_TAG=&quot;v$NEW_VERSION&quot;
git tag -a &quot;$NEW_TAG&quot; -m &quot;$BUILD_TYPE release $NEW_VERSION&quot;
</code></pre>
<p>Lastly,
we mustn't forget to return the user
to their original branch:</p>
<pre><code>git checkout &quot;$CURRENT_BRANCH&quot; &gt; /dev/null 2&gt;&amp;1
</code></pre>
<h3>Talk to the user</h3>
<p>In many of the commands above,
we sent the output to <code>/dev/null</code>
and the user doesn't have any idea
what has happened.
So we should tell them
that the script finished successfully
and how they can check
what's changed:</p>
<pre><code>echo
echo &quot;Success! The release has been tagged locally but it hasn't been pushed.&quot;
echo &quot;Before pushing, you should check that the changes appear to be sane.&quot;
echo
echo &quot;Branch:&quot;
echo
echo &quot;  $RELEASE_BRANCH&quot;
echo
echo &quot;Tag:&quot;
echo
echo &quot;  $NEW_TAG&quot;
</code></pre>
<p>We can also tell them
what their next steps are:</p>
<pre><code>echo
echo &quot;When you're ready to push, paste the following lines into your terminal:&quot;
echo
echo &quot;git push origin $RELEASE_BRANCH&quot;
echo &quot;git push origin $NEW_TAG&quot;
echo
echo &quot;After that, you must open a pull request to merge the changes back to master:&quot;
echo
echo &quot;  https://github.com/mozilla/fxa/compare/$RELEASE_BRANCH?expand=1&quot;
</code></pre>
<h3>Putting it all together</h3>
<p>This is the full output we see
when we run the script
we built for FxA:</p>
<pre><code>~/c/fxa (train-138) $ ./release.sh patch
[train-138 1d8914565] Release 1.138.5
 23 files changed, 44 insertions(+), 16 deletions(-)

Success! The release has been tagged locally but it hasn't been pushed.
Before pushing, you should check that the changes appear to be sane.
At the very least, eyeball the diffs and git log.
If you're feeling particularly vigilant, you may want to run some of the tests and linters too.

Branches:

  train-138
  train-138-private

Tags:

  v1.138.5
  v1.138.5-private

When you're ready to push, paste the following lines into your terminal:

git push origin train-138
git push origin v1.138.5
git push private train-138-private
git push private v1.138.5-private

After that, you must open pull requests in both the public and private repos to merge the changes back to master:

  https://github.com/mozilla/fxa/compare/train-138?expand=1
  https://github.com/mozilla/fxa-private/compare/train-138-private?expand=1

Don't forget to leave a comment in the deploy bug.

Include links to the tags:

### Tags
* https://github.com/mozilla/fxa/releases/tag/v1.138.5
* https://github.com/mozilla/fxa-private/releases/tag/v1.138.5-private
</code></pre>
<h3>There will be bugs</h3>
<p>You can see the full release script
for FxA <a href="https://github.com/mozilla/fxa/blob/master/release.sh">here</a>.
We fixed a number of issues
to reach that point,
so it might be instructive
to link to some examples
of things we got wrong
along on the way:</p>
<ul>
<li><a href="https://github.com/mozilla/fxa/pull/730">Forgetting to pull remote changes</a>.</li>
<li><a href="https://github.com/mozilla/fxa/pull/920">Using the return code instead of the result string</a>.</li>
<li><a href="https://github.com/mozilla/fxa/pull/1271">Identifying the last tag incorrectly</a>.</li>
<li><a href="https://github.com/mozilla/fxa/pull/1311">Tripping over control codes when parsing branch names</a>.</li>
<li><a href="https://github.com/mozilla/fxa/pull/1601">Using bash loop syntax</a></li>
</ul>
<aside>
<p class="smallprint jobspam">
Are you hiring?
Because I'm looking for my next role!
See <a href="https://files.philbooth.me/philbooth.cv.pdf" rel="nofollow">my CV</a>.
</p>
</aside>