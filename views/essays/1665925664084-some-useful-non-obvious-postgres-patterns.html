<h2>Some useful, non-obvious Postgres patterns</h2>
<!-- sql postgres linting databases devops -->
<p class="article-time"><em><time datetime="2022-10-16T13:07:44.084Z">16<sup>th</sup> October 2022</time></em></p>
<p>There's nothing earth-shattering here,
but I can recall that each of these principles
were not obvious to me at some point in time.
Perhaps some of them are not obvious to you right now.</p>
<ul>
<li><a href="#explicit-on-delete">Always define explicit <code>ON DELETE</code> semantics</a></li>
<li><a href="#on-delete-set-null">If in doubt, use <code>ON DELETE SET NULL</code></a></li>
<li><a href="#mutex-columns">Mutually exclusive columns</a></li>
<li><a href="#prohibit-jsonb-nulls">Prohibit hidden nulls in <code>jsonb</code> columns</a></li>
<li><a href="#updated-at-not-null">Declare your <code>updated_at</code> columns <code>NOT NULL</code> to make sorting easier</a></li>
<li><a href="#citext-email-addresses">Use the <code>citext</code> extension for email addresses</a></li>
</ul>
<div id="explicit-on-delete"></div>
<h3>Always define explicit <code>ON DELETE</code> semantics</h3>
<p>When you create a foreign key constraint using <code>REFERENCES</code>,
the default behaviour for <code>ON DELETE</code> is <code>NO ACTION</code>.
That means deletes on the foreign table
will fail if they break any referring rows.
Sometimes that's okay and what you want to happen.
But plenty of times it won't be
and in those cases it tends to bite you
when it's least convenient.</p>
<p>For that reason,
it's prudent to always make your <code>ON DELETE</code> clauses explicit,
even if the action is <code>NO ACTION</code> or <code>RESTRICT</code>.
Doing that as a rule
means there will never be occasions
when you forget to declare them by accident,
leading to unexpected <code>DELETE</code> failures
down the line.</p>
<p>You can enforce the rule
with a crude grep-based lint check
in your CI pipeline:</p>
<pre><code>#!/bin/sh

MISSING_ON_DELETE=$(git grep -i references -- '*.sql' | grep -iv 'on delete')

if [ &quot;$MISSING_ON_DELETE&quot; != &quot;&quot; ]; then
  echo &quot;ON DELETE not declared on foreign keys:&quot;
  echo &quot;$MISSING_ON_DELETE&quot;
  exit 1
fi
</code></pre>
<p>Of course,
any failure here might just be a newline
that was inserted before the <code>ON DELETE</code> clause.
But it's not too much hardship to fixup your pull requests
so that <code>ON DELETE</code> is always on the same line as <code>REFERENCES</code>.
The payoff is that you'll never end up in situations
where deleting data accidentally fails
due to the default semantics.</p>
<div id="on-delete-set-null"></div>
<h3>If in doubt, use <code>ON DELETE SET NULL</code></h3>
<p>Following from the previous point,
any time there's a shred of doubt
about which <code>ON DELETE</code> behaviour is correct,
consider opting for <code>ON DELETE SET NULL</code>.</p>
<p>The case for <code>ON DELETE CASCADE</code> is often obvious,
if the foreign row has clear ownership of referring data.
Things become more uncertain when the decision is between
<code>ON DELETE SET NULL</code> and <code>ON DELETE RESTRICT</code> or <code>ON DELETE NO ACTION</code>.
The fallout from erring towards <code>ON DELETE SET NULL</code>
is usually easier to deal with
in those cases.</p>
<p>For instance,
consider the extreme case of a GDPR request
from a user to delete all their data.
You have a legal obligation
to ensure everything gets deleted,
even if it means leaving some nulls behind
in other tables.
Any left-behind nulls can be handled
according to your business logic.
Sometimes it's fine to leave them in place
and just code round them.
Other times you might want an automated process
that surfaces them for human intervention,
or perhaps there are other users in your system
that ownership can be transferred to.
Failing deletes are typically less preferable to those alternatives.</p>
<div id="mutex-columns"></div>
<h3>Mutually exclusive columns</h3>
<p>It can happen in a data model
that you want an either/or relationship
between two (or more) columns.
When that scenario arises,
you should enforce it with a constraint
so it's physically impossible for clients to create conflicting data.</p>
<p>To give a concrete example,
imagine a customisable navigation menu
where each item can link to
either a URL or some entity from the database.
The simplified table might look like this:</p>
<pre><code>CREATE TABLE menu_items (
  id uuid NOT NULL DEFAULT gen_random_uuid() PRIMARY KEY,
  position integer NOT NULL,
  entity_id uuid REFERENCES entities(id) ON DELETE SET NULL,
  url text
);
</code></pre>
<p>Here <code>entity_id</code> and <code>url</code> are mutually exclusive,
and you want to guarantee
they can't both be set on a single row.
A <code>CHECK</code> constraint that counts non-null values
can do that:</p>
<pre><code>CREATE TABLE menu_items (
  id uuid NOT NULL DEFAULT gen_random_uuid() PRIMARY KEY,
  position integer NOT NULL,
  entity_id uuid REFERENCES entities(id) ON DELETE SET NULL,
  url text,

  CONSTRAINT ck_menu_items_mutex CHECK (
    (entity_id IS NOT NULL)::integer +
    (url IS NOT NULL)::integer &lt;= 1
  )
);
</code></pre>
<p>Perhaps you want to add a third type of menu item later,
linking to records from a <code>views</code> table:</p>
<pre><code>ALTER TABLE menu_items
ADD view_id uuid REFERENCES views(id) ON DELETE SET NULL;
</code></pre>
<p>If so,
you should amend the constraint at the same time:</p>
<pre><code>ALTER TABLE menu_items
ADD view_id uuid REFERENCES views(id) ON DELETE SET NULL,
DROP CONSTRAINT ck_menu_items_mutex,
ADD CONSTRAINT ck_menu_items_mutex CHECK (
  (entity_id IS NOT NULL)::integer +
  (view_id IS NOT NULL)::integer +
  (url IS NOT NULL)::integer &lt;= 1
);
</code></pre>
<p>In this way you've guaranteed
that mutual exclusivity is upheld
between the relevant columns.</p>
<div id="prohibit-jsonb-nulls"></div>
<h3>Prohibit hidden nulls in <code>jsonb</code> columns</h3>
<p>When storing data as <code>jsonb</code>,
it's common to declare the column as <code>NOT NULL</code>
so clients aren't forced to perform a null check
before dereferencing:</p>
<pre><code>  data jsonb NOT NULL DEFAULT '{}',
</code></pre>
<p>However there's a lurking footgun here
because the string <code>'null'</code> is valid JSON
that will evaluate to null when parsed.
You can preclude this from happening
with a constraint:</p>
<pre><code>  data jsonb NOT NULL DEFAULT '{}',

  CONSTRAINT ck_data_not_null CHECK (
    data &lt;&gt; 'null'
  )
</code></pre>
<p>Now clients are freed from the burden
of null-checking <code>data</code>
before dereferencing it.</p>
<div id="updated-at-not-null"></div>
<h3>Declare your <code>updated_at</code> columns <code>NOT NULL</code> to make sorting easier</h3>
<p>It's common practice
to add <code>created_at</code> and <code>updated_at</code> columns to tables
if you want to track when rows were last changed.</p>
<p>The definition of <code>created_at</code> is clear:</p>
<pre><code>  created_at timestamptz NOT NULL DEFAULT now(),
</code></pre>
<p>It's tempting to define <code>updated_at</code> as nullable
so that clients can easily determine
whether a given row has been updated since creation:</p>
<pre><code>  updated_at timestamptz,
</code></pre>
<p>However doing that has implications on sort order,
if you ever want to sort rows
in order of last-updated on the frontend.
For example,
if you <code>SELECT</code> with <code>ORDER BY updated_at DESC</code>,
the rows with a null <code>updated_at</code>
will be sorted to the beginning of the result set.
Typically that's not what your users want to see.
Adding <code>NULLS FIRST</code> to your <code>SELECT</code> doesn't help either,
because what users really want
is an interleaved sort order of last-updated-or-created.</p>
<p>For that, you can define <code>updated_at</code> as <code>NOT NULL</code> too:</p>
<pre><code>  updated_at timestamptz NOT NULL DEFAULT now(),
</code></pre>
<p>Now when you <code>ORDER BY updated_at DESC</code>,
rows will be returned in an order that's helpful to users.</p>
<p>But what if you also wanted to use the <code>updated_at</code> column
to determine whether or not a row has been updated?
That's best handled with a separate, generated column:</p>
<pre><code>  is_updated boolean GENERATED ALWAYS AS (
    updated_at &gt; created_at
  ) STORED,
</code></pre>
<p>This leans on the fact that <code>now()</code> returns the time
from the start of the current transaction.
So when new rows are created,
it's guaranteed that the default values
for <code>created_at</code> and <code>updated_at</code>
will always be the same.
Now clients can use the <code>is_updated</code> column
to check whether rows have been updated
and still have a user-friendly natural sort order
for <code>updated_at</code>.</p>
<div id="citext-email-addresses"></div>
<h3>Use the <code>citext</code> extension for email addresses</h3>
<p>Occasionally users have their <code>CAPS LOCK</code> key pressed
without realising.
So if you need to identify them
using an email address that they typed,
you should ignore case
everywhere you make a comparison.</p>
<p>The traditional way to do case-insensitive string comparison in SQL
is by calling <code>LOWER()</code> on both operands.
That's fine,
but it's easy to forget
and if you do forget,
it isn't always spotted right away.</p>
<p>Fortunately there's a Postgres extension
that means you don't have to remember
to do explicit case-insensitive comparisons;</p>
<pre><code>CREATE EXTENSION citext;
</code></pre>
<p>With that in place,
you can declare your email address columns as type <code>citext</code>:</p>
<pre><code>  email_address citext NOT NULL,
</code></pre>
<p>There's no need to call <code>LOWER()</code>
when working with this data now,
the extension will automatically ignore casing
on your behalf.</p>
<aside>
  <p class="smallprint jobspam">
    Discuss this post
    <a href="https://www.reddit.com/r/PostgreSQL/comments/y6474g/some_useful_nonobvious_postgres_patterns/">on Reddit</a>.
  </p>
</aside>
<aside>
<p class="smallprint jobspam">
Are you hiring?
Because I'm looking for my next role!
See <a href="https://files.philbooth.me/philbooth.cv.pdf" rel="nofollow">my CV</a>.
</p>
</aside>