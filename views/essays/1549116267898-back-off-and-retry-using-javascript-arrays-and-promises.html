<h2>Back-off and retry using JavaScript arrays and promises</h2>
<!-- javascript promises arrays recursion how-to -->
<p class="article-time"><em><time datetime="2019-02-02T14:04:27.898Z">2<sup>nd</sup> February 2019</time></em></p>
<p>JavaScript's <code>Array</code> and <code>Promise</code> types
compose nicely
with functional programming idioms
to control concurrency
without recourse to 3rd-party libraries.
This post contrasts two such patterns
that enable you to process data
either concurrently or serially,
with back-off and retry logic
in the event of errors occurring.</p>
<h3>Concurrent execution</h3>
<p>Concurrent execution is the simple case,
used when you don't have to worry about
factors such as rate-limits or ordering.</p>
<p>Let's say
you have an array of data
that you want to send
to a back-end metrics system.
This system is under your control,
and you know you won't be rate-limited:</p>
<pre><code>const request = require('request-promise');

const { METRICS_API_KEY } = process.env;
const METRICS_ENDPOINT = 'https://example.com/metrics';

function send (data) {
  return Promise.all(
    data.map(batch =&gt; request(METRICS_ENDPOINT, {
      method: 'POST',
      formData: {
        api_key: METRICS_API_KEY,
        data: JSON.stringify(batch),
      },
    }).promise())
  );
}
</code></pre>
<p>In this case,
you can just bang through the data
as quickly as possible
using <code>Array.map</code>
and pay no heed
to how that translates
into network usage.</p>
<p>If you want to add retry logic,
it's straightforward to insert some
by pulling the map operation out
to a named function
and calling it recursively
in the error case:</p>
<pre><code>const request = require('request-promise');

const { METRICS_API_KEY } = process.env;
const METRICS_ENDPOINT = 'https://example.com/metrics';
const RETRY_LIMIT = 3;

function send (data) {
  return Promise.all(data.map(batch =&gt; sendBatch(batch)));
}

async function sendBatch (batch, iteration = 0) {
  try {
    return await request(METRICS_ENDPOINT, {
      simple: true,
      method: 'POST',
      formData: {
        api_key: METRICS_API_KEY,
        data: JSON.stringify(batch),
      },
    }).promise();
  } catch (error) {
    if (iteration === RETRY_LIMIT) {
      return error;
    }

    return sendBatch(batch, iteration + 1);
  }
}
</code></pre>
<p>It's worth calling out the termination condition here.
All recursive functions need
some kind of termination condition
to prevent them spinning off
into infinity.
<a href="https://mitpress.mit.edu/books/little-schemer-fourth-edition">The Little Schemer</a>,
which is possibly the best book there is
about recursion,
suggests the termination condition
should usually be the first item
in any recursive function.
I've broken that convention here
because the condition returns the error response,
which it wouldn't have access to
in the other position.
But at least it's the first item
in the <code>catch</code> block
and hopefully stands out clearly enough
to anyone scanning the code.</p>
<p>It's also worth calling out
that <a href="https://alexn.org/blog/2017/10/11/javascript-promise-leaks-memory.html">JavaScript's native promises
leak memory in the presence of recursion</a>.
That won't be an issue
if your recursion is shallow,
but if any of your code
is deeply recursive,
you should consider switching to
a more competent implementation.</p>
<h3>Serial execution</h3>
<p>When you want to call a 3rd-party service,
things are less simple
because you probably need
to adhere to rate-limits.
These will dictate your behaviour when sending
and also when backing-off
in the face of <code>429</code> responses.</p>
<p>Staying with the previous example,
it can be modified
to send data serially
and take a break
if the rate-limit is violated,
like so:</p>
<pre><code>const request = require('request-promise');

const { METRICS_API_KEY } = process.env;
const METRICS_ENDPOINT = 'https://example.com/metrics';
const RETRY_LIMIT = 3;
const BACKOFF_INTERVAL = 30000;

function send (data) {
  return data.reduce(async (promise, batch) =&gt; {
    let responses = await promise;

    responses.push(await sendBatch(batch));

    return responses;
  }, Promise.resolve([]));
}

async function sendBatch (batch, iteration = 0) {
  try {
    return await request(METRICS_ENDPOINT, {
      simple: true,
      method: 'POST',
      formData: {
        api_key: METRICS_API_KEY,
        data: JSON.stringify(batch),
      },
    }).promise();
  } catch (error) {
    if (iteration === RETRY_LIMIT) {
      return error;
    }

    if (error.statusCode === 429) {
      return new Promise(resolve =&gt; {
        setTimeout(() =&gt; {
          sendBatch(batch, iteration + 1)
            .then(resolve);
        }, BACKOFF_INTERVAL);
      });
    }

    return sendBatch(batch, iteration + 1);
  }
}
</code></pre>
<p>Here the <code>Array.map</code>
is changed to an <code>Array.reduce</code>,
where the accumulator argument
is a promise
and it waits for that promise to resolve
at the start of each iteration.
This forces the loop to execute serially,
waiting for each batch to finish
before beginning the next one.</p>
<p>Then further down,
in the error-handling logic,
a condition is added
to check whether the error response
has a <code>429</code> status code.
If it does,
the recursive call is delayed
for 30 seconds
and the whole loop is paused
waiting for that back-off period
to expire.</p>
<p>If the documented rate limit was, say,
10 batches per second,
you could take this approach
a step further
and pre-emptively seek
to honour the rate-limit
without triggering the prohibitive
30-second back-off:</p>
<pre><code>const request = require('request-promise');

const { METRICS_API_KEY } = process.env;
const METRICS_ENDPOINT = 'https://example.com/metrics';
const BATCH_INTERVAL = 100;
const RETRY_LIMIT = 3;
const BACKOFF_INTERVAL = 30000;

function send (data) {
  return data.reduce(async (promise, batch) =&gt; {
    let responses = await promise;

    responses.push(await sendBatch(batch));

    await new Promise(resolve =&gt; {
      setTimeout(resolve, BATCH_INTERVAL);
    });

    return responses;
  }, Promise.resolve([]));
}

async function sendBatch (batch, iteration = 0) {
  try {
    return await request(METRICS_ENDPOINT, {
      simple: true,
      method: 'POST',
      formData: {
        api_key: METRICS_API_KEY,
        data: JSON.stringify(batch),
      },
    }).promise();
  } catch (error) {
    if (iteration === RETRY_LIMIT) {
      return error;
    }

    if (error.statusCode === 429) {
      return new Promise(resolve =&gt; {
        setTimeout(() =&gt; {
          sendBatch(batch, iteration + 1)
            .then(resolve);
        }, BACKOFF_INTERVAL);
      });
    }

    return sendBatch(batch, iteration + 1);
  }
}
</code></pre>
<p>Here the <code>sendBatch</code> function
remains unchanged
but the reducer was tweaked
to add a short delay
in between batches.</p>
<h3>Conclusion</h3>
<p>So, in summary:</p>
<ul>
<li>
<p>Use <code>Array.map</code> when you want concurrency.</p>
</li>
<li>
<p>Use <code>Array.reduce</code> when you want serial execution.</p>
</li>
<li>
<p>Wrap <code>setTimeout</code> inside a promise
for back-off or delay logic.</p>
</li>
<li>
<p>Use recursion for retry logic.</p>
</li>
<li>
<p>Consider alternative promise implementations
if your code is deeply recursive.</p>
</li>
</ul>
<p>If you want to see examples
of this approach in production code,
in <a href="https://github.com/mozilla/fxa-amplitude-send/pull/81/files">mozilla/fxa-amplitude-send#81</a>
I applied it
to the metrics pipeline
for Firefox Accounts
and in <a href="https://github.com/mozilla/fxa-shared/pull/56/files#diff-b71a4b24e51e084510f9912b8f21a664R54">mozilla/fxa-shared#56</a>
I used it in our feature-flagging abstraction.</p>
<aside>
  <p class="smallprint jobspam">
    Discuss this post
    <a href="https://www.reddit.com/r/javascript/comments/amemi2/backoff_and_retry_using_javascript_arrays_and/">on Reddit</a>.
  </p>
</aside>
<aside>
<p class="smallprint jobspam">
Are you hiring?
Because I'm looking for my next role!
See <a href="https://files.philbooth.me/philbooth.cv.pdf" rel="nofollow">my CV</a>.
</p>
</aside>