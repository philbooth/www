<h2>The art of good code review</h2>
<!-- code-review how-to -->
<p class="article-time"><em><time datetime="2024-01-29T08:42:48.051Z">29<sup>th</sup> January 2024</time></em></p>
<p>This post is an extension
to a talk I gave recently at work.
It was arranged at short notice
and the audience were experienced engineers,
so I kept it brief and tried not to be patronising.
But the feedback afterwards was quite positive
and there were some questions too,
so here's the extended version
for anyone interested.
Note that our team conducts pre-merge reviews,
and some of the suggestions are specific to that context.
I actually prefer post-merge reviews,
but am yet to persuade everyone I work with
that they're better.
Oh, and trigger warning:
this post contains opinions.</p>
<h3>Why do we review code?</h3>
<p>First,
let's establish what the point
of code review is
and also what it isn't.</p>
<p>The number one,
most important reason to review code
is shared ownership.
&quot;Ownership&quot; can be tricky to define in code terms,
but mostly it's a feeling.
It means you understand the code,
that you feel empowered to change it
and the responsibility to maintain it.
It doesn't necessarily mean that you wrote it.</p>
<p>Shared ownership is important
because individuals fail in lots of ways.
They get sick
or move jobs
or get promoted.
And code fails too of course,
so whenever important code is owned by an individual,
that's a timebomb counting down
to sabotage your organisation
(usually late on a Friday night).
Reviewing code
(along with other techniques, like pair programming)
is a way to dismantle knowledge siloes
and reduce the likelihood of emergency situations occurring.</p>
<p>The second most important reason we review code
is &quot;the big picture&quot;.
Engineers are naturally detail-oriented people.
The job literally forces you to focus on details
in order to do it well.
When you spend most of your day
thinking about and working on small details,
it's very easy to lose sight of broader concerns.
A code review is an opportunity to address that,
to step back and consider the broader view.
Does that small component you've been building in isolation
make sense in the context of the wider system?
Does it compromise security or observability or performance?
Is it robust?
Are there simpler approaches?</p>
<p>Trailing far behind those two
are a host of less important reasons
for reviewing code:
improving code quality,
upholding conventions,
correct formatting,
catching bugs,
spotting gaps in test coverage,
eliminating dead code,
etc.
Those things are all fine,
but they're less important
because code review is an unreliable way to catch them.
Some things will get spotted but many won't.
If code review is your only mechanism for fixing them,
you are absolutely guaranteed to fail.
That's why we have tools like linters, compilers, type checkers, formatters
and various flavours of automated testing.
If no tool exists for a thing you want to check,
write one that does.
And when machines are doing that work for you,
your humans will be happier too.</p>
<h3>What does a bad code review look like?</h3>
<blockquote>
<p>lgtm 👍</p>
</blockquote>
<p>Not all &quot;lgtm&quot; code reviews are bad,
of course.
Some changes are trivial
or just obviously correct,
and &quot;lgtm&quot; is fine on those.
But for more complex changes,
if the only comment is &quot;lgtm&quot;
it can be a sign the reviewer
hasn't properly tried to understand it.
And understanding is the primary aim,
because of shared ownership.
Code review is not a rubber stamping exercise.</p>
<p>Another sign of bad code review
is when reviews show up too quickly.
There's a lower bound on how fast it's possible to read code
and any faster than that should be an alarm bell.
Although again,
there can be exceptions.
Perhaps a change was worked on in partnership
and the reviewer is already familiar with it.</p>
<p>Bad code reviews can also happen when
lots of discussion is generated on trivial changes,
but the complicated parts sail through
without any comment at all.
This is <a href="https://bikeshed.org/">bikeshed</a> territory
and it can escalate to poisonous levels
if not corrected.</p>
<h3>Good code review requires a good PR</h3>
<p>The burden for a good code review
does not rest solely on the reviewer.
Initial responsibility falls on the reviewee
to make their changes easy to understand.</p>
<p>That starts with the description,
which should be clear, detailed and precise.
It should introduce the change
and include all of the context required to understand it.
If the context is too long to describe in detail,
summarise it and link to the original sources of information.
The description should cover what the change is,
why it's needed
and how you've approached it.
If there are approaches you opted against,
mention those too
and why you decided against them.</p>
<p>Then the change itself
should only contain the minimal diff
necessary to implement the description.
If there are tangential refactorings,
extract them to a fresh branch
and get them reviewed separately,
in advance of the main change.
Avoid frivolous changes
that don't serve to improve the thing you're working on.
Every single part of the diff
should have a purpose that you can justify
when asked to do so.</p>
<p>You should make sure all automated checks are green
before asking for review.
A reviewer's attention is a finite resource,
don't waste it on things that aren't finished yet.</p>
<p>All behavioural changes should come with equivalent modifications to the tests.
New features should have new tests,
behaviour modifications should modify existing tests.
If there are no existing tests to modify,
take the opportunity to write some new ones
so that nobody can accidentally break your change in future.</p>
<p>If the change itself was automated,
include sufficient information
for a reviewer to review the invocation
instead of thousands of lines of generated output.
They'll want to eyeball the output too of course,
but the command you used or script you wrote
is the real thing being reviewed here.</p>
<p>Lastly,
before asking others to review,
conduct a self-review yourself first.
It doesn't take long
and you might be surprised how many mistakes it catches.
Typos, leftover <code>TODO</code> comments, debug logging, skipped tests, commented-out code etc.
You'll appear far more competent to your peers
if that stuff doesn't show up in your pull requests.</p>
<h3>What does a good code review look like?</h3>
<p>There are five things you want to get right
as a reviewer.</p>
<ol start="0">
<li>
<p>The description.</p>
<p>Before you even get to looking at the code,
read the description.
Is there one?
Does it make sense?
Does it include all of the information
required to understand the code?</p>
<p>This is not pedantry.
You need all of that sorted out first,
to be certain you understand
what you're meant to be reviewing.
If you're consistent about it,
engineers will quickly adapt to your expectations
and writing good descriptions will become a habit.</p>
</li>
<li>
<p>The code.</p>
<p>Reading the code means more than just glancing at its outline.
You need to actually read every token on every line.
It's supposed to take time.
You can't read a 500-line diff in 5 minutes,
so carve out enough time in advance
that you won't feel rushed to finish
and move on to the next thing.</p>
<p>You don't have to understand everything straight away.
Ask questions about parts that aren't clear.
Don't worry about sounding stupid,
nobody is going to think that
and the fastest way to be right about something
is to be wrong about it in public.
For parts you think you understand,
check your understanding by writing it down
and asking the reviewee to confirm it
(or correct you if it's wrong).
If you see something
that looks funny or weird or surprising or complicated,
mention it.</p>
<p>Gradually build a mental model of the code,
then ask yourself the key question
that underpins the concept of shared ownership:</p>
<p><em>&quot;Am I happy to maintain this?&quot;</em></p>
</li>
<li>
<p>The tests.</p>
<p>Tests are just another part of the code,
but I'm calling them out separately
because sometimes people completely skip over them
when reviewing.
Understanding the tests is as important
as understanding the code.
Tests that are hard to understand
are less likely to be maintained in future.
And good tests can double as helpful documentation
for what a system can and can't do.</p>
<p>Make sure the things that should be tested are actually being tested.
This is not just about coverage,
you need to look at the assertions too.
100% coverage is meaningless
if assertions don't enforce
the right invariants.</p>
<p>While reviewing the tests
it can also be helpful to go back
and review the relevant code again.
Compare the two side-by-side
and look for things that seem missing
or out of place.
Ask questions about anything
that doesn't match your expectations.</p>
</li>
<li>
<p>Commenting.</p>
<p>There are three rules for discussion in code review:
be considerate, be honest and be open-minded.</p>
<p><em>Be considerate</em>
because there are humans with feelings
at the other end of the review.
Teams work much better
when there's mutual respect
and everyone gets along.
So any criticism should be of the code,
not the person.
It should be made dispassionately
and come with reasoning attached.
If possible,
alternative suggestions should be mentioned too,
although avoid being over-prescriptive
and try to leave the reviewee some space
to figure out their own solution.
Don't suck the joy out of programming.</p>
<p><em>Be honest</em>
because code review is a way
to improve practices across the team.
It fails at that objective if you withhold opinion
in a misguided attempt to be &quot;nice&quot;.
Honesty will also lead to your own mistakes being corrected occasionally,
which means you benefit from it personally too.</p>
<p><em>Be open-minded</em>
because mistakes are inevitable on all sides,
including your own.
Don't cleave to your opinions too tightly
and take time to consider other points of view.
If you find yourself stuck in a disagreement
where neither side wants to budge,
yield.
It's okay to disagree and move on,
code review is not a competitive sport.
If you really feel strongly about something,
you can present your ideas as follow-up suggestions.
Codebases are rarely finished,
there's usually an opportunity to improve things
further down the line.</p>
</li>
<li>
<p>Approval.</p>
<p>Given that the main point of code review is shared ownership,
it follows that you can't approve a change before you understand it.
Everything else runs secondary to that,
so withhold approval until you're confident
that you fully understand the change.</p>
<p>The flipside of this principle
is that it's okay to approve changes you disagree with,
provided you understand them.
Code review is not meant to be a dictatorship.</p>
<p>If you've left comments about minor corrections,
pre-approve the change anyway before they land.
Your peers should be trusted to handle them appropriately,
or even if they get it wrong that can be fixed later.
Pre-approval ensures nobody gets blocked
from merging the revised changes when they're ready.</p>
</li>
</ol>
<h3>Code review checklist</h3>
<p>What kinds of thing
should you be looking for in code review?
People sometimes pay close attention
to a few pet areas
and let other stuff slide.
If that sounds like you,
you can use this (incomplete) checklist as inspiration:</p>
<ul>
<li>Security</li>
<li>Observability</li>
<li>Performance</li>
<li>Robustness</li>
<li>Complexity</li>
<li>Readability</li>
<li>Maintainability</li>
<li>Over-engineering (YAGNI)</li>
<li>Using existing abstractions correctly</li>
<li>Naming stuff</li>
<li>Comments</li>
<li>Tests</li>
<li>Doing one thing at a time (separate PRs)</li>
<li>&quot;Do I want to maintain this?&quot;</li>
</ul>
<p>If anything above is something you're not sure about,
spend a bit of time learning around the topic
then start by asking questions about it during code review.
You'll quickly level up in that area.</p>
<aside>
  <p class="smallprint jobspam">
    Discuss this post
    <a href="https://www.reddit.com/r/programming/comments/1adr51z/the_art_of_good_code_review/">on Reddit</a>.
  </p>
</aside>
<aside>
<p class="smallprint jobspam">
Are you hiring?
Because I'm looking for my next role!
See <a href="https://files.philbooth.me/philbooth.cv.pdf" rel="nofollow">my CV</a>.
</p>
</aside>