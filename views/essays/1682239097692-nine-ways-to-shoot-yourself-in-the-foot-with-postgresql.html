<h2>Nine ways to shoot yourself in the foot with PostgreSQL</h2>
<!-- devops databases postgres how-to sql extreme-learning -->
<p class="article-time"><em><time datetime="2023-04-23T08:38:17.692Z">23<sup>rd</sup> April 2023</time></em></p>
<p>Previously for <a href="/blog?topic=extreme-learning"><em>Extreme Learning</em></a>,
I discussed
<a href="/blog/six-ways-to-shoot-yourself-in-the-foot-with-healthchecks">all the ways I've broken production using healthchecks</a>.
In this post
I'll do the same for PostgreSQL.</p>
<p>The common thread linking most of these gotchas is scalability.
They're things that won't affect you while your database is small.
But if one day you want your database not to be small,
it pays to think about them in advance.
Otherwise they'll came back and bite you later,
potentially when it's least convenient.
Plus in many cases
it's less work to do the right thing from the start,
than it is to change a working system
to do the right thing later on.</p>
<h3>1. Keep the default value for <code>work_mem</code></h3>
<p>The biggest mistake I made
the first time I deployed Postgres in prod
was not updating the default value for <a href="https://www.postgresql.org/docs/current/runtime-config-resource.html#GUC-WORK-MEM"><code>work_mem</code></a>.
This setting governs how much memory is available
to each query operation
before it must start writing data
to temporary files on disk,
and can have a huge impact on performance.</p>
<p>It's an easy trap to fall into if you're not aware of it,
because all your queries in local development
will typically run perfectly.
And probably in production too at first,
you'll have no issues.
But as your application grows,
the volume of data and complexity of your queries both increase.
It's only then that you'll start to encounter problems,
a textbook <em>&quot;but it worked on my machine&quot;</em> scenario.</p>
<p>When <code>work_mem</code> becomes over-utilised,
you'll see latency spikes
as data is paged in and out,
causing hash table and sorting operations
to run much slower.
The performance degradation is extreme and,
depending on the composition of your application infrastructure,
can even turn into full-blown outages.</p>
<p>A good value depends on multiple factors:
the size of your Postgres instance,
the frequency and complexity of your queries,
the number of concurrent connections.
So it's really something
you should always keep an eye on.</p>
<p>Running your logs through <a href="https://pgbadger.darold.net/">pgbadger</a>
is one way to look for warning signs.
Another way is to use an automated 3rd-party system
that alerts you before it becomes an issue,
such as <a href="https://pganalyze.com/">pganalyze</a>
(disclosure: I have no affiliation to pganalyze, but am a very happy customer).</p>
<p>At this point,
you might be asking if there's a magic formula
to help you pick the correct value for <code>work_mem</code>.
It's not my invention
but this one was handed down to me
by the greybeards:</p>
<pre><code>work_mem = ($YOUR_INSTANCE_MEMORY * 0.8 - shared_buffers) / $YOUR_ACTIVE_CONNECTION_COUNT
</code></pre>
<aside>
  <p class="aside">
    <em>
      <strong>EDIT:</strong>
      Thanks to <a href="https://news.ycombinator.com/item?id=35697986">afhammad</a>
      for pointing out you can also override <code>work_mem</code> on a per-transaction basis
      using <a href="https://www.postgresql.org/docs/current/sql-set.html"><code>SET LOCAL work_mem`</code></a>.
    </em>
  </p>
</aside>
<h3>2. Push all your application logic into Postgres functions and procedures</h3>
<p>Postgres has some nice abstractions for procedural code
and it can be tempting
to push lots or even all
of your application logic
down into the db layer.
After all,
doing that eliminates latency
between your code and the data,
which should mean lower latency for your users, right?
Well, nope.</p>
<p>Functions and procedures in Postgres are not zero-cost abstractions,
they're deducted from your performance budget.
When you spend memory and CPU to manage a call stack,
less of it is available to actually run queries.
In severe cases that can manifest in some surprising ways,
like unexplained latency spikes and replication lag.</p>
<p>Simple functions are okay,
especially if you can mark them <code>IMMUTABLE</code> or <code>STABLE</code>.
But any time you're assembling data structures in memory
or you have nested functions or recursion,
you should think carefully about
whether that logic can be moved back to your application layer.
There's no TCO in Postgres!</p>
<p>And of course,
it's far easier to scale application nodes
than it is to scale your database.
You probably want to postpone thinking about
database scaling for as long as possible,
which means being conservative about resource usage.</p>
<h3>3. Use lots of triggers</h3>
<p>Triggers are another feature
that can be misused.</p>
<p>Firstly,
they're less efficient than some of the alternatives.
Requirements that can be implemented
using <a href="https://www.postgresql.org/docs/current/ddl-generated-columns.html">generated columns</a>
or <a href="https://www.postgresql.org/docs/current/rules-materializedviews.html">materialized views</a>
should use those abstractions,
as they're better optimised by Postgres internally.</p>
<p>Secondly,
there's a hidden gotcha lurking in how triggers
tend to encourage event-oriented thinking.
As you know,
it's good practice in SQL
to batch related <code>INSERT</code> or <code>UPDATE</code> queries together,
so that you lock a table once
and write all the data in one shot.
You probably do this in your application code automatically,
without even needing to think about it.
But triggers can be a blindspot.</p>
<p>The temptation is to view each trigger function
as a discrete, composable unit.
As programmers we value separation of concerns
and there's an attractive elegance
to the idea of independent updates
cascading through your model.
If you feel yourself pulled in that direction,
remember to view the graph in its entirety
and look for parts that can be optimised
by batching queries together.</p>
<p>A useful discipline here is to restrict yourself
to a single <code>BEFORE</code> trigger
and a single <code>AFTER</code> trigger
on each table.
Give your trigger functions generic names
like <code>before_foo</code> and <code>after_foo</code>,
then keep all the logic inline
inside one function.
Use <a href="https://www.postgresql.org/docs/current/plpgsql-trigger.html#PLPGSQL-DML-TRIGGER"><code>TG_OP</code></a>
to distinguish the trigger operation.
If the function gets long,
break it up with some comments
but don't be tempted to refactor to smaller functions.
This way it's easier
to ensure writes are implemented efficiently,
plus it also limits the overhead
of managing an extended call stack
in Postgres.</p>
<h3>4. Use <code>NOTIFY</code> heavily</h3>
<p>Using <a href="https://www.postgresql.org/docs/current/sql-notify.html"><code>NOTIFY</code></a>,
you can extend the reach of triggers into your application layer.
That's handy if you don't have the time or the inclination
to manage a dedicated message queue,
but once again it's not a cost-free abstraction.</p>
<p>If you're generating lots of events,
the resources spent on notifying listeners
will not be available elsewhere.
This problem can be exacerbated
if your listeners need to read further data
to handle event payloads.
Then you're paying for every <code>NOTIFY</code> event
plus every consequential read in the handler logic.
Just as with triggers,
this can be a blindspot that hides opportunities
to batch those reads together
and reduce load on your database.</p>
<p>Instead of <code>NOTIFY</code>,
consider writing events to a FIFO table
and then consume them in batches at regular cadence.
The right cadence depends on your application,
maybe it's a few seconds
or perhaps you can get away with a few minutes.
Either way it will reduce the load,
leaving more CPU and memory available for other things.</p>
<p>A possible schema for your event queue table
might look like this:</p>
<pre><code>CREATE TABLE event_queue (
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  user_id uuid NOT NULL REFERENCES users(id) ON DELETE CASCADE,
  type text NOT NULL,
  data jsonb NOT NULL,
  created_at timestamptz NOT NULL DEFAULT now(),
  occurred_at timestamptz NOT NULL,
  acquired_at timestamptz,
  failed_at timestamptz
);
</code></pre>
<p>With that in place you could acquire events
from the queue like so:</p>
<pre><code>UPDATE event_queue
SET acquired_at = now()
WHERE id IN (
  SELECT id
  FROM event_queue
  WHERE acquired_at IS NULL
  ORDER BY occurred_at
  FOR UPDATE SKIP LOCKED
  LIMIT 1000 -- Set this limit according to your usage
)
RETURNING *;
</code></pre>
<p>Setting <code>acquired_at</code> on read
and using <code>FOR UPDATE SKIP LOCKED</code>
guarantees each event is handled only once.
After they've been handled,
you can then delete the acquired events in batches too
(there are better options than Postgres
for permanently storing historical event data
of unbounded size).</p>
<aside>
  <p class="aside">
    <em>
      <strong>EDIT:</strong>
      Thanks to
      <a href="https://www.reddit.com/r/PostgreSQL/comments/12w574s/comment/jhfc455/">labatteg</a>,
      <a href="https://www.reddit.com/r/programming/comments/12ye352/comment/jhn31v2/">notfancy</a>,
      <a href="https://news.ycombinator.com/item?id=35699614">reese_john</a>,
      <a href="https://news.ycombinator.com/item?id=35700422">xnickb</a>
      and <a href="https://news.ycombinator.com/item?id=35701210">Mavvie</a>
      for pointing out the missing <code>FOR UPDATE SKIP LOCKED</code> in this section.
    </em>
  </p>
</aside>
<h3>5. Don't use <code>EXPLAIN ANALYZE</code> on real data</h3>
<p><a href="https://www.postgresql.org/docs/current/using-explain.html"><code>EXPLAIN</code></a>
is a core tool in every backend engineer's kit.
I'm sure you diligently check your query plans
for the dreaded <code>Seq Scan</code> already.
But Postgres can return more accurate plan data
if you use <a href="https://www.postgresql.org/docs/current/using-explain.html#USING-EXPLAIN-ANALYZE"><code>EXPLAIN ANALYZE</code></a>,
because that actually executes the query.
Of course,
you don't want to do that in production.
So to use <code>EXPLAIN ANALYZE</code> well,
there are a few steps you should take first.</p>
<p>Any query plan is only as good
as the data you run it against.
There's no point running <code>EXPLAIN</code>
against a local database
that has a few rows in each table.
Maybe you're fortunate enough
to have a comprehensive seed script
that populates your local instance
with realistic data,
but even then there's a better option.</p>
<p>It's really helpful
to set up a dedicated sandbox instance
alongside your production infrastructure,
regularly restored with a recent backup from prod,
specifically for the purpose of running <code>EXPLAIN ANALYZE</code>
on any new queries that are in development.
Make the sandbox instance smaller
than your production one,
so it's more constrained than prod.
Now <code>EXPLAIN ANALYZE</code> can give you confidence
about how your queries are expected to perform
after they've been deployed.
If they look good on the sandbox,
there should be no surprises waiting for you
when they reach production.</p>
<h3>6. Prefer CTEs over subqueries</h3>
<p>If you're regularly using <code>EXPLAIN</code>
this one probably won't catch you out,
but it's caught me out before
so I want to mention it explicitly.</p>
<p>Many engineers are bottom-up thinkers
and CTEs (i.e. <a href="https://www.postgresql.org/docs/current/queries-with.html"><code>WITH</code> queries</a>)
are a natural way to express bottom-up thinking.
But they may not be the most performant way.</p>
<p>Instead I've found that <a href="https://www.postgresql.org/docs/current/functions-subquery.html">subqueries</a>
will often execute much faster.
Of course it depends entirely
on the specific query,
so I make no sweeping generalisations
other than to suggest you should <code>EXPLAIN</code> both approaches
for your own complex queries.</p>
<p>There's a discussion of the underlying reasons
in the &quot;<a href="https://www.postgresql.org/docs/current/queries-with.html#id-1.5.6.12.7">CTE Materialization</a>&quot;
section of the docs,
which describes the performance tradeoffs more definitively.
It's a good summary,
so I won't waste your time
trying to paraphrase it here.
Go and read that
if you want to know more.</p>
<aside>
  <p class="aside">
    <em>
      <strong>EDIT:</strong>
      Thanks to <a href="https://www.reddit.com/r/PostgreSQL/comments/12w574s/comment/jhdrd2n/">Randommaggy and Ecksters</a>
      for pointing out the subquery suggestion in this section is outdated.
      Since version 12,
      Postgres has been much better at optimising CTEs
      and will often just replace the CTE with a subquery anyway.
      I've left the section in place
      as the broader point about comparing approaches with <code>EXPLAIN</code> still stands
      and the "CTE Materialization" docs remain a worthwhile read.
      But bear in mind the comment thread linked above!
    </em>
  </p>
</aside>
<h3>7. Use recursive CTEs for time-critical queries</h3>
<p>If your data model is a graph,
your first instinct will naturally be
to traverse it recursively.
Postgres provides <a href="https://www.postgresql.org/docs/current/queries-with.html#QUERIES-WITH-RECURSIVE">recursive CTEs</a> for this
and they work nicely,
even allowing you to handle self-referential/infinitely-recursive loops gracefully.
But as elegant as they are,
they're not fast.
And as your graph grows,
performance will decline.</p>
<p>A useful trick here is to think about
how your application traffic stacks up
in terms of reads versus writes.
It's common for there to be many more reads than writes
and in that case,
you should consider denormalising your graph
to a materialized view or table
that's better optimised for reading.
If you can store each queryable subgraph on its own row,
including all the pertinent columns
needed by your queries,
reading becomes a simple (and fast) <code>SELECT</code>.
The cost of that is write performance of course,
but it's often worth it for the payoff.</p>
<h3>8. Don't add indexes to your foreign keys</h3>
<p>Postgres doesn't automatically create indexes for foreign keys.
This may come as a surprise if you're more familiar with MySQL,
so pay attention to the implications
as it can hurt you in a few ways.</p>
<p>The most obvious fallout from it
is the performance of joins
that use a foreign key.
But those are easily spotted using <code>EXPLAIN</code>,
so are unlikely to catch you out.</p>
<p>Less obvious perhaps
is the performance of <code>ON DELETE</code> and <code>ON UPDATE</code> behaviours.
If your schema relies on cascading deletes,
you might find some big performance gains
by adding indexes on foreign keys.</p>
<h3>9. Compare indexed columns with <code>IS NOT DISTINCT FROM</code></h3>
<p>When you use regular <a href="https://www.postgresql.org/docs/9.0/functions-comparison.html">comparison operators</a>
with <code>NULL</code>,
the result is also <code>NULL</code>
instead of the boolean value you might expect.
One way round this
is to replace <code>&lt;&gt;</code> with <code>IS DISTINCT FROM</code>
and replace <code>=</code> with <code>IS NOT DISTINCT FROM</code>.
These operators treat <code>NULL</code> as a regular value
and will always return booleans.</p>
<p>However,
whereas <code>=</code> will typically cause the query planner
to use an index if one is available,
<code>IS NOT DISTINCT FROM</code> bypasses the index
and will likely do a <code>Seq Scan</code> instead.
This can be confusing
the first time you notice it
in the output from <code>EXPLAIN</code>.</p>
<p>If that happens
and you want to force a query to use the index instead,
you can make the null check explicit
and then use <code>=</code> for the not-null case.</p>
<p>In other words,
if you have a query that looks like this:</p>
<pre><code>SELECT * FROM foo
WHERE bar IS NOT DISTINCT FROM baz;
</code></pre>
<p>You can do this instead:</p>
<pre><code>SELECT * FROM foo
WHERE (bar IS NULL AND baz IS NULL)
OR bar = baz;
</code></pre>
<aside>
  <p class="smallprint jobspam">
    Discuss this post
    <a href="https://www.reddit.com/r/PostgreSQL/comments/12w574s/nine_ways_to_shoot_yourself_in_the_foot_with/">on Reddit</a>
    and <a href="https://news.ycombinator.com/item?id=35684220">on Hacker News</a>.
  </p>
</aside>
<aside>
<p class="smallprint jobspam">
Are you hiring?
Because I'm looking for my next role!
See <a href="https://files.philbooth.me/philbooth.cv.pdf" rel="nofollow">my CV</a>.
</p>
</aside>