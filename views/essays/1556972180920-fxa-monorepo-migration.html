<h2>FxA monorepo migration</h2>
<!-- mozilla fxa monorepo devops -->
<p class="article-time"><em><time datetime="2019-05-04T12:16:20.920Z">4<sup>th</sup> May 2019</time></em></p>
<p>Historically,
the <a href="https://accounts.firefox.com/">Firefox Accounts</a> (FxA) codebase
was organised as separate repositories
because it's deployed as separate microservices
in production.
However,
that arrangement caused us
some nagging issues
as developers,
so we decided to migrate the code
to a <a href="https://github.com/mozilla/fxa">monorepo</a> instead.
This post discusses
why and how we did that,
and how things turned out
in the end.</p>
<h3>Before</h3>
<p>There were three main problems
with the many-repo approach.</p>
<ul>
<li>
<p>Firstly,
it was hard to share common code
between services.
Generally we addressed that
by extracting the re-usable logic
to yet another repository
and publishing it from there with <code>npm</code>.
But each additional repository
increased the mental load
of working on FxA
and updating the common code
became a tiresome process
that involved opening pull requests
in multiple locations
to grab the latest dependency.</p>
</li>
<li>
<p>Because different subsets of the team
worked in different repositories,
coding conventions gradually diverged between them.
We could have averted this situation
with stricter lint rules
shared between repos,
but by the time we realised there was a problem,
there was a problem.
The respective owners of each codebase
were not enthusiastic about making
wide-ranging formatting changes
to their code,
so anyone working horizontally
was forced to chop and change
between local customs instead.</p>
</li>
<li>
<p>The most serious issues were in CI.
It was possible for services
to break downstream consumers
of their interfaces,
without causing any builds to fail.
This would sometimes lead to
downstream developers discovering failures
while working on unrelated tasks
and lacking any context
to help them understand
what was wrong.</p>
</li>
</ul>
<p>With the imminent arrival of
<a href="/blog/firefox-accounts-is-hiring">new recruits on the FxA team</a>,
we decided to take the hit
and pull all of the code
in to one place.</p>
<h3>During</h3>
<p>Our hard requirements for the move
were that all of the commit histories must be preserved,
the most recent tag for each service should be copied too
and that CI should run tests
for all downstream consumers
when their upstream dependencies were changed.
Danny Coates took the bull by the horns
and wrote a <a href="https://gist.github.com/dannycoates/682d039544b58fdca124e12bccd1529a#file-mono-sh">script to do the actual work</a>.
We played with that until
we <a href="https://github.com/mozilla/fxa/issues/354">persuaded ourselves we'd thought things through properly</a>,
then we fixed a date
to migrate the code for real.</p>
<p>There could be no going back
after that point,
as issues were to be moved,
pull requests re-opened,
code landed and
old repositories archived.
We opted to do the move
immediately after cutting the release
for FxA train 134,
to give ourselves some breathing space
to work through any problems
before cutting <a href="https://github.com/mozilla/fxa/releases/tag/v1.135.0">train 135</a>.
Or at least,
that was the theory.
In practice,
we had to cut
<a href="https://github.com/mozilla/fxa/blob/v1.134.5/packages/fxa-auth-server/CHANGELOG.md#11345">point releases for train 134</a>
so a lot of that breathing space
was lost.</p>
<p>After the code was moved,
the next priority
was to ensure
we had a release script
that worked from the new monorepo.
This meant working for
both JavaScript and Rust services,
so probably warrants
a <a href="/blog/build-a-better-release-script">separate blog post in its own right</a>,
but you can find
the finished script
<a href="https://github.com/mozilla/fxa/blob/master/release.sh">here</a>.</p>
<h3>After</h3>
<p>It's fair to say
we underestimated
how many problems
might occur
as a result of this migration.
For example,
some of the unexpected issues were:</p>
<ul>
<li>
<p>The git hooks we had in our old repos
were forced to compete against each other
in the new monorepo.
Lacking the time to make them co-exist happily,
we just <a href="https://github.com/mozilla/fxa/pull/758">deleted them</a>.</p>
</li>
<li>
<p>The version endpoint for one of our services
broke because the relative path
to the <code>.git</code> directory
changed.
<a href="https://github.com/mozilla/fxa/pull/789">Fixing it was straightforward</a>.</p>
</li>
<li>
<p>Some of our tests
cloned the database repo
in their setup.
That repo is now archived
and becoming ever staler
with each passing commit,
so we quickly applied a
<a href="https://github.com/mozilla/fxa/pull/796">sticking-plaster fix to copy the local db directory</a> instead
but are yet to land a long-term fix
that we're really happy with.</p>
</li>
</ul>
<p>Additionally,
we haven't yet done
some of the things
the monorepo was designed to enable,
such as <a href="https://github.com/mozilla/fxa/issues/941">standardise a mechanism for cross-package dependencies</a>
or <a href="https://github.com/mozilla/fxa/issues/1025">unify the lint rules for all our packages</a>.</p>
<p>Ultimately,
it's become clear
the migration will be a gradual process
carried out over a number of months
rather than an instant cutover.
The long-term benefits
are all still available
but the journey to reach them
is ongoing!</p>
<aside>
<p class="smallprint jobspam">
Are you hiring?
Because I'm looking for my next role!
See <a href="https://files.philbooth.me/philbooth.cv.pdf" rel="nofollow">my CV</a>.
</p>
</aside>