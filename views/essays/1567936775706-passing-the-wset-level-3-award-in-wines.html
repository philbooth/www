<h2>Passing the WSET Level 3 Award in Wines</h2>
<!-- wine wset personal -->
<p class="article-time"><em><time datetime="2019-09-08T09:59:35.706Z">8<sup>th</sup> September 2019</time></em></p>
<p>Last week I received the result
of my WSET level 3.
I got a pass with distinction
for both parts of the exam,
the tasting
and the theory.
So, keeping up the tradition
I started with <a href="/blog/passing-the-wset-level-2-award-in-wines-and-spirits">level 2</a>,
this post summarises my advice
for anyone out there
thinking about level 3.</p>
<p><img src="https://assets.philbooth.me/images/wset-level3-672.jpg" alt="Result letter for the WSET level 3, showing a pass with distinction" /></p>
<h3>Theory papers</h3>
<p>As you'd expect,
the size of
the level 3 subject matter
is a significant step up
from level 2.
There are more
grapes and regions to learn about,
all in much more detail.
There are also significant sections covering
the life-cycle of a vine,
vine management,
and production techniques in the winery.
So of course,
many more hours of home study
are needed at this level.</p>
<p>Fortunately it's possible
to pick apart the detail
into sections that will only be
covered by the multiple-choice paper
and others that will be included
in the written answer paper too.
This is all clarified
by the course specification booklet,
which is included
in the bundle you will receive
before starting the course.
Especially when you come to revising
for the exams,
it pays to use the specification
to allocate your time
between topics appropriately.</p>
<p><img src="https://assets.philbooth.me/images/wset-level3-specification-672.jpg" alt="Specification booklet for the WSET level 3" /></p>
<p>The multiple-choice paper
is pretty much the same
as the one from level 2,
just covering a wider set of topics.
The written answer paper
is a new challenge though,
and it's worth spending time
practising answering these
types of question
as much as possible
during the course itself
and in whatever revision time
you have before the exam.</p>
<p>The questions often fall into
one of three categories:</p>
<ul>
<li>
<p><em>Describe the style of...</em></p>
<p>These questions expect you to answer
with a structured note,
as if you were tasting
an example of the wine
under exam conditions.
You can gauge how much detail is called for
by the number of marks next to the question but,
bearing in mind you won't lose points
for providing too much information,
if you answer with a full tasting note,
you will always maximise your chance
to earn all the marks.
So include the appearance,
nose,
palate,
quality
and readiness for drinking.
Since you'll be well practised
at writing these tasting notes
by the time you get to the exam,
in most cases
it shouldn't take you very long
to fill out those details.</p>
</li>
<li>
<p><em>Describe 5</em> (or 3, or 7, or whatever) <em>factors in the vineyard affecting the style of...</em></p>
<p>There are two parts to this.
Obviously you must name
however many factors
in the vineyard.
Think climate,
weather,
temperature,
aspect,
altitude,
latitude,
grape variety,
training,
pruning,
canopy management,
frost,
hail,
pests,
disease,
noble rot,
harvesting
and so on.
Sometimes a question will specify human factors
and other times it will specify natural factors
but regardless,
there should be no shortage
of things to talk about.</p>
<p>But it's not sufficient
simply to name them,
you must also discuss
how each one affects
development of the grapes and,
by consequence,
the wine itself.</p>
<p>Occasionally you'll get one of these questions
where it only says &quot;name the factors...&quot;.
In that case,
you don't have to do the extra discussion
and simply naming them is sufficient.
So it pays to read the question carefully
and make sure you've understood precisely
what is being asked.</p>
</li>
<li>
<p><em>Describe factors in the winery...</em></p>
<p>Same as before,
but now concerning what happens
after the grapes have been harvested.
Think sorting,
destemming,
crushing,
draining,
pressing,
maceration (including carbonic and semi-carbonic),
fermentation (including whole bunch),
punching down,
pumping over,
rack and return,
maturation,
oak or inert vessels,
autolysis,
malolactic fermentation,
blending,
clarification,
stabilisation,
bottling
and more.</p>
<p>And again,
if the question says
&quot;describe&quot; or &quot;discuss&quot;
it's not enough to just name the factors.
You have to talk about
how they affect the wine
if you want to get all the marks.</p>
</li>
</ul>
<p>There'll be other questions
that don't fall into these three types
of course,
but many of them will
(even if sometimes they're
worded differently).</p>
<h3>Tasting paper</h3>
<p>Perhaps the most daunting part of the exam
is the tasting paper.
Fortunately,
there a number of tricks
you can use to make it easier.</p>
<p>The first thing to really understand is
that the classroom sessions
are your opportunity to calibrate your reactions
against your instructor's.
They will be marking your tasting paper so
if you find yourself consistently making
the same mistakes,
it's important to recognise that early
and try different techniques
to explicitly correct yourself
as the course progresses.
For instance,
one of the things I struggled with was acidity.
I generally found myself
over-stating the acidity of wines compared
to my instructor,
calling things high acidity
when she said they were only medium or medium(+).
Realising it was a problem,
I focused on it a lot
in the last few sessions
and managed to change my perception
by spitting earlier.
It turned out that I was giving the wine
too much time in the acid-sensitive parts of my mouth,
experiencing sensory overload as a result.</p>
<p>Something else I worried about
in the classroom sessions
was that other students often identified
many more aromas and flavours
than I was able to.
There are tricks you can use
to your advantage here too,
however.
For red wines,
you really want to focus on reliably detecting
whether a wine is in
the &quot;black fruits&quot; or &quot;red fruits&quot; category.
For whites,
is it herbaceous, citrus, stone fruits or tropical fruits?
Some wines cover multiple categories of course,
but the point is that
once you've identified the broad themes
it becomes much easier to focus on individual sub-flavours
and ask yourself if you can detect them.
And in the absolute worst case,
where you're really struggling with a particular wine,
there are often flavours that you can gamble on
as being common across the board.
Bramble is a good one
for red wines,
as is blossom for whites.
And don't forget &quot;simple&quot; is a valid descriptor
that will get you a point
on wines that really aren't offering very much.</p>
<p><img src="https://assets.philbooth.me/images/wset-level3-lexicon-672.jpg" alt="Detail from the WSET level tasting lexicon" /></p>
<p>When thinking about whether
a wine exhibits secondary or tertiary flavours,
make sure you get at least a couple of examples
from each that applies,
to make it clear
you've identified that aspect.
Some flavours exist in multiple categories,
so if you only list one example
you risk it counting against the wrong category
and not picking up
all of the available marks.</p>
<p>Also realise that you won't be penalised
for putting too many aromas or flavours down
within reason,
so if you have something in mind
but aren't sure about it,
don't overthink it and
just write it down anyway.
The worst thing to do
is chase a particular elusive flavour
or over-analyse,
because it can get you into a state of
self-doubt and paralysis.
If that happens,
it's best to clear your palate
with some water,
mentally reset yourself
and then start afresh.</p>
<h3>Have fun</h3>
<p>The biggest difference for me at level 3
was how much fun it was
compared to level 2.</p>
<p>Level 2 is all about
remembering individual facts
for the multiple choice paper,
which makes it as much a test of memory
as it is of true understanding.
It's been a while since
I sat my level 2,
but my abiding impression
is of stressing over small details
and not really taking in the bigger picture.</p>
<p>At level 3
there's time to really dive into why things are the way they are,
which makes it feel like you're starting to truly understand
the underlying subject matter.
Everything hangs together more coherently
and you'll get a lot of <em>ah-ha!</em> moments,
where something you learn about a particular region
links back to other things you already learned about elsewhere.
It's a more satisfying and enjoyable syllabus
because of that.</p>
<p>So if you've taken level 2
and are unsure about level 3,
I'd recommend it
on that basis alone!</p>
