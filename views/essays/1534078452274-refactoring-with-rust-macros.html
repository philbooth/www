<h2>Refactoring with Rust macros</h2>
<!-- rust refactoring macros how-to -->
<p class="article-time"><em><time datetime="2018-08-12T12:54:12.274Z">12<sup>th</sup> August 2018</time></em></p>
<p>Refactoring boilerplate code is always easy
in dynamically-typed languages,
but sometimes takes a bit more effort
when constrained by strong typing.
This is something I was puzzling over recently,
when the penny dropped for me
about how <a href="https://doc.rust-lang.org/book/second-edition/appendix-04-macros.html">Rust's macros</a>
can be used to bridge the gap.</p>
<p>If you have control
over all of the code in question,
macros probably aren't needed of course.
Some combination of generics, traits and enums
would typically provide a better (and more readable) solution.
But there are times
when the types involved
are out of your control
and that is a niche
which macros can thrive in.</p>
<p>Here is a basic example
I encountered last week.
<a href="https://actix.rs/api/actix-web/stable/actix_web">Actix-web</a>
exports a <code>TestServer</code> struct
that helps you test your endpoints.
<code>TestServer::new</code> expects a configuration function
that takes one argument
of type <code>TestApp</code>:</p>
<pre><code>TestServer::new(|app| {
    // Set up routes, resources and middleware
    // by calling methods on `app`...
})
</code></pre>
<p>For the production code
there is a similar <code>App</code> type,
which has methods
with the same signatures
as the ones on <code>TestApp</code>.
Since the routes you want to test
are likely the same
as the routes on your production server,
a natural next step
might be to try and write
a common route-setup function
that works in both contexts.
Unfortunately though,
<code>TestApp</code> and <code>App</code> are not related.
Those &quot;common&quot; methods aren't inherited
from some shared trait,
they're defined independently
on each structure.</p>
<p>So in order to write a function
that sets routes on either <code>App</code> or <code>TestApp</code>,
you'd have to wrap them in an enum
and write code to manually forward
all of the method calls
to the inner structs.
Looking up the associated type information
to get those definitions right
is tedious busywork,
but a simple macro
allows you to skip it instead:</p>
<pre><code>macro_rules! init_routes {
    ($app:expr) =&gt; {
        // All the initialisation code stays the same,
        // methods are just called on `$app` instead...
    }
}
</code></pre>
<p>Now you can invoke the macro
from both your test setup
and the production code,
without it needing to know anything about
the type information for <code>$app</code>:</p>
<pre><code>TestServer::new(|app| {
    init_routes!(app);
})
</code></pre>
<p>The compiler will concern itself
with type-checking later,
after the macro has been expanded.
All the macro cares about
is whether its match arm is satisfied.</p>
<h3>Refactoring types</h3>
<p>Macros can also be used
to eliminate duplication
where there is no executable code
involved at all,
only type information.</p>
<p>For instance,
staying with actix,
let's say you have an actor
that handles some messages:</p>
<pre><code>pub struct MyActor {
    // Whatever state your actor needs...
}

impl Handler&lt;Foo&gt; for MyActor {
    type Result = Result&lt;HashMap&lt;String, String&gt;, Error&gt;;

    fn handle(&amp;mut self, msg: Foo, context: &amp;mut self::Context) -&gt; Self::Result {
        let mut result = HashMap::new();

        // Populate `result` somehow...

        Ok(result)
    }
}

impl Handler&lt;Bar&gt; for MyActor {
    type Result = Result&lt;bool, Error&gt;;

    fn handle(&amp;mut self, msg: Foo, context: &amp;mut self::Context) -&gt; Self::Result {
        let mut result = false;

        // ...

        Ok(result)
    }
}
</code></pre>
<p>The code to define
the <code>Foo</code> and <code>Bar</code> messages
might look something like this:</p>
<pre><code>pub struct Foo {
    pub id: String,
    pub wibble: String,
}

impl Message for Foo {
    type Result = &lt;MyActor as Handler&lt;Foo&gt;&gt;::Result;
}

pub struct Bar {
    pub id: String,
    pub blee: String,
}

impl Message for Bar {
    type Result = &lt;MyActor as Handler&lt;Bar&gt;&gt;::Result;
}
</code></pre>
<p>And that pattern might be repeated many more times
for other message types too.
Conventional refactoring is immediately off the agenda here
because we only have types, properties
and the <code>Message</code> trait
to work with.
But that's all meat and drink
for a macro:</p>
<pre><code>macro_rules! message {
    ($message:ident {$($property:ident: $type:ty),*}) =&gt; {
        pub struct $message {
            $(pub $property: $type,)*
        }

        impl Message for $message {
            type Result = &lt;MyActor as Handler&lt;$message&gt;&gt;::Result;
        }
    }
}
</code></pre>
<p>With that in place
your message boilerplate
now looks like this:</p>
<pre><code>message!(Foo {
    id: String,
    foo: String
});

message!(Bar {
    id: String,
    bar: String
});
</code></pre>
<p>An important introduction here
was the pair of <code>$(</code> and <code>),*</code>
wrapping the declarations
of <code>$property</code> and <code>$type</code>
in the match arm of the macro.
That denotes repetition
and says the enclosed portion of the match
can be repeated zero or more times,
with a <code>,</code> separating each item.
Replacing the <code>*</code> with a <code>+</code>
would change that to one or more times
and the <code>,</code> could be replaced
by anything you like
(including nothing at all).
But <code>,</code> is a good choice here
because it makes the macro more intuitive.</p>
<h3>Nested macros</h3>
<p>That's all well and good
for straightforward cases,
but what about
when a refactoring has many levels?
Perhaps there is a core pattern
to be extracted
in addition to higher levels
that depend on the core?
This can also be achieved,
although there are a couple of gotchas
to be careful of.</p>
<p>Returning to the previous example,
you'll have noticed that <code>Foo</code> and <code>Bar</code>
share a common <code>id</code> property.
If there are no other message types,
we could just move <code>id</code>
into the macro body inline.
But what if there are
<code>Baz</code> and <code>Qux</code> messages,
which don't have an <code>id</code> property?
Nested macros can help you with that:</p>
<pre><code>macro_rules! id_message {
    ($message:ident {$($property:ident: $type:ty),*}) =&gt; {
        message!($message {
            id: String
            $(, $property: $type)*
        });
    }
}

id_message!(Foo {
    foo: String
});

id_message!(Bar {
    bar: String
});

message!(Baz {
    baz: String
});

message!(Qux {
    qux: String
});
</code></pre>
<p>Another way of expressing the same thing
might have been to try and write
a higher-order macro,
nesting one <code>macro_rules!</code>
directly inside another.
But in this case that wouldn't work
because nested repetition is ambiguous syntax:</p>
<pre><code>macro_rules! message_macro {
    ($macro:ident {$($common_property:ident: $common_type:ty),*}) =&gt; {
        macro_rules! $macro {
            ($message:ident {$($property:ident: $type:ty),*}) =&gt; {
                pub struct $message {
                    $(pub $common_property: $common_type,)*
                    $(pub $property: $type,)*
                }

                impl Message for $message {
                    type Result = &lt;MyActor as Handler&lt;$message&gt;&gt;::Result;
                }
            }
        }
    }
}

message_macro!(message {});

message_macro!(id_message {
    id: String
});
</code></pre>
<p>Here,
it would require some magical thinking
to infer that we meant for
<code>$($property:ident: $type:ty),*</code>
to be interpreted as part of
the child macro's match arm
rather than the parent macro's body.
The compiler very reasonably points this out to us
with the following error message:</p>
<pre><code>error: attempted to repeat an expression containing no syntax variables matched as repeating at this depth
 --&gt; src/main.rs:4:31
  |
4 |             ($message:ident {$($property:ident: $type:ty),*}) =&gt; {
  |
</code></pre>
<p>The second obstacle
to be wary of with nesting
is macro hygiene.
Rust's macros are hygienic,
which means they each have their own context for expansion.
An identifier introduced by an inner context
may not be referenced by outer layers,
instead you must pass the identifier in from outside.
Even if the expansion looks like
it would make sense when rolled out manually,
the compiler will still complain.</p>
<p>More concretely,
consider the following
macros for defining route handlers
with actix-web:</p>
<pre><code>macro_rules! endpoint {
    ($handler:ident: $dispatcher:ident ($path_type:ty) {$($property:ident: $value:expr),*}) =&gt; {
        pub fn $handler(
            (path, state): (Path&lt;$path_type&gt;, State&lt;ServerState&gt;),
        ) -&gt; FutureResponse&lt;HttpResponse&gt; {
            state
                .actor
                .send($dispatcher {
                    $($property: $value),*
                })
                .from_err()
                .and_then(|res| match res {
                    Ok(body) =&gt; Ok(HttpResponse::Ok().json(body)),
                    Err(_) =&gt; Ok(HttpResponse::InternalServerError().into()),
                })
                .responder()
        }
    }
}

macro_rules! uid_endpoint {
    ($handler:ident: $dispatcher:ident) =&gt; {
        endpoint! {
            $handler: $dispatcher (UidParam) {
                uid: path.uid.clone()
            }
        }
    }
}
</code></pre>
<p><code>uid_endpoint!</code> would fail compilation here with:</p>
<pre><code>error[E0425]: cannot find value `path` in this scope
</code></pre>
<p>Even though we know <code>path</code> exists
because <code>endpoint!</code> always declares it,
we still have to pass the identifier in
so that <code>uid_endpoint!</code> is allowed to reference it:</p>
<pre><code>macro_rules! endpoint {
    ($handler:ident: $dispatcher:ident ($path:ident: $path_type:ty) {$($property:ident: $value:expr),*}) =&gt; {
        pub fn $handler(
            ($path, state): (Path&lt;$path_type&gt;, State&lt;ServerState&gt;),
        ) -&gt; FutureResponse&lt;HttpResponse&gt; {
            state
                .actor
                .send($dispatcher {
                    $($property: $value),*
                })
                .from_err()
                .and_then(|res| match res {
                    Ok(body) =&gt; Ok(HttpResponse::Ok().json(body)),
                    Err(_) =&gt; Ok(HttpResponse::InternalServerError().into()),
                })
                .responder()
        }
    }
}

macro_rules! uid_endpoint {
    ($handler:ident: $dispatcher:ident) =&gt; {
        endpoint! {
            $handler: $dispatcher (path: UidParam) {
                uid: path.uid.clone()
            }
        }
    }
}
</code></pre>
<h3>Macro debugging</h3>
<p>Something that's obvious
from the examples in this post
is that macros can get pretty grawlixy
and hard to read at times.
If the compiler is complaining
about some code in one of your macros
and you're struggling to identify the problem,
it can be helpful to look at
the rolled-out macro expansions.
There is a compiler flag
that lets you print them
from the command line:</p>
<pre><code>rustc -Z unstable-options --pretty expanded &lt;MODULE PATH&gt;
</code></pre>
<p>Where <code>&lt;MODULE PATH&gt;</code> is the path
to the source module
containing the macro you want to print.</p>
<p>A general rule of thumb
I've found helpful is to try
and keep lower-level macros
as simple as you can,
limiting all repetition to just the outermost macros
where possible.</p>
<aside>
<p class="smallprint jobspam">
Are you hiring?
Because I'm looking for my next role!
See <a href="https://files.philbooth.me/philbooth.cv.pdf" rel="nofollow">my CV</a>.
</p>
</aside>